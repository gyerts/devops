"""WEB_AUTOMATIZATION URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
# from django.contrib import admin

import Engine.views.main
import Engine.views.settings
import Engine.views.interfaces
import Engine.views.tasks
import Engine.views.artifacts
import Engine.classes.login.login
import Engine.views.test
from modules.git.views import list_of_submodules


urlpatterns = [
    url('^register', Engine.classes.login.login.register,                       name='register'),
    url('^run_process', Engine.views.main.run_process,                          name='run_process'),
    url('^get_status', Engine.views.main.get_output_of_running_process,         name='get_output_of_running_process'),
    url('^get_active_process', Engine.views.main.get_active_process,            name='get_active_process'),
    url('^settings', Engine.views.settings.settings,                            name='settings'),
    url('^settings/global_settings', Engine.views.settings.settings,            name='global_settings'),
    url('^find', Engine.views.interfaces.find,                                  name='find'),
    url('^show_all_interfaces', Engine.views.interfaces.show_all_interfaces,    name='show_all_interfaces'),
    url('^show_all_errors', Engine.views.interfaces.show_all_errors,            name='show_all_errors'),
    url('^tasks', Engine.views.tasks.show_all_tasks,                            name='tasks'),
    url('^change_status_of_task', Engine.views.tasks.change_status_of_task),
    url('^change_time_pattern', Engine.views.tasks.change_time_pattern),
    url('^get_text_of_task', Engine.views.tasks.get_text_of_task),
    url('^edit_python_file', Engine.views.tasks.edit_python_file,               name='edit_python_file'),
    url('^artifacts', Engine.views.artifacts.artifacts,                         name='artifacts'),
    url('^git', include("modules.git.urls")),

    url('^test', Engine.views.test.test,                                        name='test'),
    url('^run_function', Engine.views.main.run_function,                        name='run_function'),

    url('^', Engine.views.main.main,                                            name='home'),
]
