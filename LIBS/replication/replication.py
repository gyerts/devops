
import os, subprocess, sys

class Replication_server:
    def __init__(self, server_ip, share):
        self.server_ip = server_ip
        self.share = share
        self.local_path_of_connected_folder = \
            os.path.join(os.environ.get("XDG_RUNTIME_DIR"), 'gvfs', 'smb-share:server=%s,share=%s'%(self.server_ip, self.share)).lower()

        self.sh = '''#!/usr/bin/expect
                        set timeout 1
                        spawn gvfs-mount smb://<<ip_address>>/
                        <<login>>
                        <<domain>>
                        <<password>>
                        interact
                    '''.replace('<<ip_address>>', server_ip + '/' + share)

        print('cifs tools')

    def __replace(self, tag, value, sh):
        if value:
            return sh.replace('<<%s>>'%tag, 'expect ": "\nsend "%s\\r"'%value)
        else:
            return sh.replace('<<%s>>'%tag, '')

    def connect(self, login=None, domain=None, password=None):
        list_of_connections = os.listdir(os.path.join(os.environ.get("XDG_RUNTIME_DIR"), 'gvfs'))

        connected = False
        connected_folder_name = None
        for connection in list_of_connections:
            if self.server_ip in connection:
                connected = True
                connected_folder_name = connection

        if not connected:
            new_sh_content = self.__replace('login', login, self.sh)
            new_sh_content = self.__replace('domain', domain, new_sh_content)
            new_sh_content = self.__replace('password', password, new_sh_content)

            new_sh_content = new_sh_content.replace('                        ', '')
            new_sh_content = new_sh_content.replace('                    ', '')

            name_of_sh_file = 'connect_to_replication_server_' + self.server_ip.replace('/', '_').replace('\\', '_') + '.sh'

            f = open(name_of_sh_file, 'w')
            f.write(new_sh_content)
            f.close()

            DEVNULL = open(os.devnull, 'wb')

            os.system('chmod +x ./%s'%name_of_sh_file)
            os.system('./%s'%name_of_sh_file)
            #os.system('rm ./%s'%name_of_sh_file)

rs = Replication_server('172.30.136.211', 'Toyota_CY17_MEU')
