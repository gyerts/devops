import sys, json
from multiprocessing.pool import ThreadPool

from report_generator_lib.stash_report.Library.Stash import Stash
from report_generator_lib.stash_report.Library.Start import Start
from report_generator_lib.report_views.sca_report_view import sca_report_view

from report_generator_lib.stash_report.Runner.do_login import *
from report_generator_lib.stash_report.Runner.check_merged_pull_request import *
from report_generator_lib.stash_report.Runner.get_changes_of_pull_request import *
from report_generator_lib.stash_report.Runner.add_involved_domains_into_the_all_changes import *
from report_generator_lib.stash_report.Runner.get_all_pull_requests import *
from report_generator_lib.stash_report.Runner.totaly_change_STATE import *
from report_generator_lib.stash_report.Runner.change_names import change_names
from report_generator_lib.stash_report.Runner.read_PathRespMapping import read_PathRespMapping

ola_gang_num_style = "asdljgiod;fug09eirtelrmgmnknv;gheriljg"
start = Start()
c = 9


def do(commit, pull_requests_merged, changes, threads_count):
    change = Change(start)
    change.change = commit.id
    change.date = commit.authorTimestamp
    change.author = commit.author.name

    belongs_to_merged_pull_requests = False
    comments_counter = 0

    # the commit could be submitted via pull-request or could be submitted directly to the branch,
    # next block of code make definition, is for-loop commit belongs to pull-request
    # if commit belongs to Merged Pull-request - add as merged commit
    """commit belongs to the MERGED PULL-REQUEST"""
    change, belongs_to_merged_pull_requests, comments_counter = \
        check_commit_in_merged_pull_requests(
            change,
            belongs_to_merged_pull_requests,
            comments_counter,
            pull_requests_merged,
            commit
        )

    # if NOT belongs to Merged Pull-requests - add as commit submitted to branch direct without review
    """commit was submitted direct to BRANCH"""
    if not belongs_to_merged_pull_requests:
        change.state = "Direct commit"

        # checking IS IT a PULL-REQUEST ?
        # BREAK CHECKING IF IT TRUE !!! pull-request is an commit object but without files, so just skip it
        commit_files = commit.files()
        if len(commit_files) == 0:
            # print(" !!! skipped !!! because it's not a commit, it's a pull-request")
            return
        # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        # get comments for every file within commit_files
        change, comments_counter = get_comments(change, comments_counter, commit_files)
        change.reviewed = 0



    change.comments = comments_counter
    changes.append(change)


def generate_stash_review_report(
        stash_url,
        project_name,
        repo_slug,
        responsibilities_ini,
        count_of_commits,
        names_ini,
        branch,
        stash_single_file,
        from_date,
        to_date,
        threads_count,
        stash_login=None,
        stash_password=None,
        json_output=False
    ):
    try:
        from_date = datetime.strptime(from_date, '%d/%m/%Y')
        to_date = datetime.strptime(to_date, '%d/%m/%Y')

        if (stash_login is None) and (stash_password is None):
            # read/ask login
            stash_login, stash_password = ask_or_read_login(ola_gang_num_style)

        # create stash/project/repository object
        stash = Stash(start, stash_url, stash_login, stash_password)
        project = stash.get_project_by_name(project_name)
        repository = project.get_repository_by_slug(repo_slug)
        print(project.name, ":", repository.name)

        # changes which one will be in report
        changes = list()

        # pull-requests which one merged
        print("\n\n[1/%s] get all merged pull-requests" % str(c))
        pull_requests_merged = get_all_pull_requests(start, stash, repository, "merged", branch, threads_count)

        # pull-requests which one opened
        print("\n\n[2/%s] get all opened pull-requests" % str(c))
        pull_requests_open = get_all_pull_requests(start, stash, repository, "open", branch, threads_count, from_date, to_date)

        # fetch all commits from repository
        print("\n\n[3/%s] get all repository commits (merged one should be duplicated in this list)" % str(c))
        commits = repository.commits(count_of_commits, branch, from_date, to_date)

        # -------------------------------------------------------------------------------------------
        # before we should fulfill all existing commits
        print("\n\n[4/%s] read all comments of merged/unmerged requests" % str(c))
        # create Change() objects Merged/Other
        # -------------------------------------------------------------------------------------------
        """
_________          _______  _______  _______  ______   _______  _______  _______  _          _
\__   __/|\     /|(  ____ )(  ____ \(  ___  )(  __  \ (  ____ )(  ___  )(  ___  )( \        (#\
   ) (   | )   ( || (    )|| (    \/| (   ) || (  \  )| (    )|| (   ) || (   ) || (         \#\
   | |   | (___) || (____)|| (__    | (___) || |   ) || (____)|| |   | || |   | || |          \#\
   | |   |  ___  ||     __)|  __)   |  ___  || |   | ||  _____)| |   | || |   | || |           )#)
   | |   | (   ) || (\ (   | (      | (   ) || |   ) || (      | |   | || |   | || |          /#/
   | |   | )   ( || ) \ \__| (____/\| )   ( || (__/  )| )      | (___) || (___) || (____/\   /#/
   )_(   |/     \||/   \__/(_______/|/     \|(______/ |/       (_______)(_______)(_______/  (#/


        """
        commit_pool = ThreadPool(processes=threads_count)
        for commit in commits:
            commit_pool.apply_async(func=do, args=(commit, pull_requests_merged, changes, threads_count))

        commit_pool.close()
        commit_pool.join()
        """
    _   _________          _______  _______  _______  ______   _______  _______  _______  _
   /#)  \__   __/|\     /|(  ____ )(  ____ \(  ___  )(  __  \ (  ____ )(  ___  )(  ___  )( \
  /#/      ) (   | )   ( || (    )|| (    \/| (   ) || (  \  )| (    )|| (   ) || (   ) || (
 /#/       | |   | (___) || (____)|| (__    | (___) || |   ) || (____)|| |   | || |   | || |
(#(        | |   |  ___  ||     __)|  __)   |  ___  || |   | ||  _____)| |   | || |   | || |
 \#\       | |   | (   ) || (\ (   | (      | (   ) || |   ) || (      | |   | || |   | || |
  \#\      | |   | )   ( || ) \ \__| (____/\| )   ( || (__/  )| )      | (___) || (___) || (____/\
   \#)     )_(   |/     \||/   \__/(_______/|/     \|(______/ |/       (_______)(_______)(_______/

        """
        # -------------------------------------------------------------------------------------------

        # -------------------------------------------------------------------------------------------
        # after we can read all opened but not merged pull-requests
        print("\n\n[5/%s] read all comments of opened requests" % str(c))
        # -------------------------------------------------------------------------------------------
        for pull_request in pull_requests_open:
            changes = get_and_convert_Commit_s_to_Change_s__of_pull_request(start, pull_request, "Under review", threads_count) + changes

        # -------------------------------------------------------------------------------------------
        # after we can do replacement of founded changes
        print("\n\n[6/%s] replace domain entry by %s" % (str(c), responsibilities_ini))
        # -------------------------------------------------------------------------------------------
        changes = add_involved_domains_into_the_all_changes_by_mapping(responsibilities_ini, changes)
        changes = add_sense_to_all_state_of_changes(changes)
        changes = change_names(changes, read_PathRespMapping(names_ini, "Workers"))

        # -------------------------------------------------------------------------------------------
        # now we can serialize all changes to output-list()
        print("[7/%s] serialize all changes to output-list" % str(c))
        # -------------------------------------------------------------------------------------------
        output = list()
        for change in changes:
            if len(change.files) > 0:
                output.append(change.to_dict())

        # -------------------------------------------------------------------------------------------
        # write xlsx file if need
        # -------------------------------------------------------------------------------------------
        print("[8/%s] MAKE XLSX" % str(c))
        sca_report_view(output, stash_single_file + ".xlsx", responsibilities_ini)

        # -------------------------------------------------------------------------------------------
        # write it to json file if need
        # -------------------------------------------------------------------------------------------
        if json_output:
            print("[9/%s] MAKE JSON" % str(c))
            with open(stash_single_file + ".json", 'w') as outfile:
                json.dump(output, outfile, sort_keys=True, indent=4)

        return output

    # -------------------------------------------------------------------------------------------
    # if stash not reachable
    # -------------------------------------------------------------------------------------------
    except Exception as ex:
        if "urlopen error" in str(ex):
            print("Can't connect to %s" % stash_url, file=sys.stderr)
            return 404
        else:
            raise ex
