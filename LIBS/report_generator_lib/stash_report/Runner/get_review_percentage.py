def get_review_percentage(pull_request, commit_id):
    reviewed = 0
    for reviewer in pull_request.reviewers:
        if reviewer.is_reviewed(commit_id):
            reviewed += 1

    if reviewed > 0:
        return reviewed/len(pull_request.reviewers)
    else:
        return 0
