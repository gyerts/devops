from report_generator_lib.stash_report.Runner.read_PathRespMapping import *


def add_involved_domains_into_the_all_changes_by_mapping(name_of_file, changes):
    mappings = read_PathRespMapping(name_of_file, "PathRespMapping")
    for change in changes:
        domains = list()
        for file in change.files:
            for mapping in mappings:
                if mapping.lower() in file.lower():
                    if mappings[mapping] not in domains:
                        domains.append(mappings[mapping])
                    break
        if len(domains) == 0:
            domains.append("Unknown domain")

        change.domain = domains
    return changes
