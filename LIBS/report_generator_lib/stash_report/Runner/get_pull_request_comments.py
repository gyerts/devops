def get_pull_request_comments(pull_request, change, comments_counter):
    if len(pull_request.comments) > 0:
        change.comments_text += "\n[#{}] pull-request-comments:\n".format(pull_request.id)
        for filename, comment in pull_request.comments:
            change.comments_text += "   {}\n".format(filename)
            change.comments_text += comment.to_string(tab="      ")
            comments_counter += comment.count()

    return comments_counter
