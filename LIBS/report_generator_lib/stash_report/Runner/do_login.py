import os
import getpass
import base64


def ask_or_read_login(hash_code):
    if os.path.exists('credentials'):
        f = open("credentials")

        login = f.readline().strip()
        password = f.readline().strip()

        login = base64.b64decode(login).decode().replace(hash_code, '')
        password = base64.b64decode(password).decode().replace(hash_code, '')

    else:
        login = input("Login: ")
        password = getpass.getpass(prompt="Password: ")

        f = open("credentials", "wb")
        f.write(base64.b64encode(bytes(login + hash_code, 'utf-8')))
        f.write(b'\n')
        f.write(base64.b64encode(bytes(password + hash_code, 'utf-8')))
        f.close()

    return [login, password]
