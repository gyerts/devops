from report_generator_lib.stash_report.Runner.get_comments import *
from report_generator_lib.stash_report.Runner.get_review_percentage import *
from report_generator_lib.stash_report.Runner.add_reviewer import *
from report_generator_lib.stash_report.Runner.get_pull_request_comments import *


def check_commit_in_merged_pull_requests(change, belongs_to_merged_pull_requests, comments_counter, pull_requests, commit):
    for pull_request in pull_requests:
        if pull_request.contains(commit):
            change.state = "Merged"
            change.reviewed = get_review_percentage(pull_request, commit.id)
            change.review += commit.url.replace("/rest/api/1.0", "")

            belongs_to_merged_pull_requests = True

            change, comments_counter = get_comments(
                change, comments_counter, pull_request.get_commit_by_id(commit.id).files()
            )

            comments_counter = get_pull_request_comments(pull_request, change, comments_counter)

            for reviewer in pull_request.reviewers:
                add_reviewer(reviewer, change)

    return [change, belongs_to_merged_pull_requests, comments_counter]
