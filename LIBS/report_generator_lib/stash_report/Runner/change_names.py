import re


def change_names(changes, workers):
    for change in changes:
        for mapping in workers:
            pattern = re.compile(mapping, re.IGNORECASE)
            if mapping.lower() in change.author.lower():
                change.author = pattern.sub(workers[mapping], change.author)
            for counter, reviewer in enumerate(change.reviewers):
                if mapping.lower() in reviewer.lower():
                    change.reviewers[counter] = pattern.sub(workers[mapping], reviewer)

            change.comments_text = pattern.sub(workers[mapping], change.comments_text)
    return changes


# test of this function
# from report_generator_lib.crusible_report.Change import Change
#
# ch = Change()
# ch.author = "ygyerts"
# ch.reviewers = ["ygyerts", "ygyerts"]
# ch.comments_text = "sdhfskd fjkshd fkjshd fkhsk df ygyerts sdfsjdfjhsldf sldj flsdf \n[ygyerts]: sshdfh sdjfh skdh f"
#
# changes = change_names(r"D:\STASH\integration-tools\scripts\names_map.ini", [ch])
# changes[0].show()
