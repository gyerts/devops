def add_reviewer(reviewer, change):
    review_state = reviewer.get_state_of_commit(change.change)
    if review_state == "APPROVED":
        approved = "(v) "

    elif review_state == "UNAPPROVED":
        approved = "(x) "

    else:
        approved = "(  ) "

    change.reviewers.append(approved + reviewer.displayName)
