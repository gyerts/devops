from report_generator_lib.stash_report.Change import Change
from report_generator_lib.stash_report.Runner.get_comments import *
from report_generator_lib.stash_report.Runner.get_review_percentage import *
from report_generator_lib.stash_report.Runner.add_reviewer import *
from report_generator_lib.stash_report.Runner.get_pull_request_comments import *

from multiprocessing.pool import ThreadPool


def do(start, commit, state, pull_request, changes):
    change = Change(start)
    comments_counter = 0
    change.change = commit.id
    change.date = commit.authorTimestamp
    change.author = commit.author.name
    change.state = state
    change.reviewed = get_review_percentage(pull_request, commit.id)
    change.review += commit.url.replace("/rest/api/1.0", "")

    for reviewer in pull_request.reviewers:
        add_reviewer(reviewer, change)

    change, comments_counter = get_comments(
        change, comments_counter, pull_request.get_commit_by_id(commit.id).files()
    )

    comments_counter = get_pull_request_comments(pull_request, change, comments_counter)

    change.comments = comments_counter
    changes.append(change)


def get_and_convert_Commit_s_to_Change_s__of_pull_request(start, pull_request, state, threads_count):
    changes = list()

    """
_________          _______  _______  _______  ______   _______  _______  _______  _          _
\__   __/|\     /|(  ____ )(  ____ \(  ___  )(  __  \ (  ____ )(  ___  )(  ___  )( \        (#\
   ) (   | )   ( || (    )|| (    \/| (   ) || (  \  )| (    )|| (   ) || (   ) || (         \#\
   | |   | (___) || (____)|| (__    | (___) || |   ) || (____)|| |   | || |   | || |          \#\
   | |   |  ___  ||     __)|  __)   |  ___  || |   | ||  _____)| |   | || |   | || |           )#)
   | |   | (   ) || (\ (   | (      | (   ) || |   ) || (      | |   | || |   | || |          /#/
   | |   | )   ( || ) \ \__| (____/\| )   ( || (__/  )| )      | (___) || (___) || (____/\   /#/
   )_(   |/     \||/   \__/(_______/|/     \|(______/ |/       (_______)(_______)(_______/  (#/

    """
    pull_requests_pool = ThreadPool(processes=threads_count)
    for commit in pull_request.commits:
        pull_requests_pool.apply_async(func=do, args=(start, commit, state, pull_request, changes))

    pull_requests_pool.close()
    pull_requests_pool.join()
    """
    _   _________          _______  _______  _______  ______   _______  _______  _______  _
   /#)  \__   __/|\     /|(  ____ )(  ____ \(  ___  )(  __  \ (  ____ )(  ___  )(  ___  )( \
  /#/      ) (   | )   ( || (    )|| (    \/| (   ) || (  \  )| (    )|| (   ) || (   ) || (
 /#/       | |   | (___) || (____)|| (__    | (___) || |   ) || (____)|| |   | || |   | || |
(#(        | |   |  ___  ||     __)|  __)   |  ___  || |   | ||  _____)| |   | || |   | || |
 \#\       | |   | (   ) || (\ (   | (      | (   ) || |   ) || (      | |   | || |   | || |
  \#\      | |   | )   ( || ) \ \__| (____/\| )   ( || (__/  )| )      | (___) || (___) || (____/\
   \#)     )_(   |/     \||/   \__/(_______/|/     \|(______/ |/       (_______)(_______)(_______/

    """

    return changes
