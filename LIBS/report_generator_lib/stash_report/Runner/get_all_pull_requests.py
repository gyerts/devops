from report_generator_lib.stash_report.Library.PullRequest import PullRequest
from multiprocessing.pool import ThreadPool
from datetime import datetime


def do(start, stash, repository, pull_request, branch, pull_requests, from_date, to_date):
    pr = PullRequest(start, stash, repository.url, pull_request)
    if pr.toRef.displayId == branch:
        if from_date is not None:
            # append this commit if it between from - to dates
            if (pr.updatedDate >= from_date) and (pr.updatedDate <= to_date):
                pull_requests.append(pr)
            else:
                return
        else:
            pull_requests.append(pr)
        pr.fulfill()


def get_all_pull_requests(start, stash, repository, state, branch, threads_count, from_date=None, to_date=None):
    pull_requests = list()

    """
_________          _______  _______  _______  ______   _______  _______  _______  _          _
\__   __/|\     /|(  ____ )(  ____ \(  ___  )(  __  \ (  ____ )(  ___  )(  ___  )( \        (#\
   ) (   | )   ( || (    )|| (    \/| (   ) || (  \  )| (    )|| (   ) || (   ) || (         \#\
   | |   | (___) || (____)|| (__    | (___) || |   ) || (____)|| |   | || |   | || |          \#\
   | |   |  ___  ||     __)|  __)   |  ___  || |   | ||  _____)| |   | || |   | || |           )#)
   | |   | (   ) || (\ (   | (      | (   ) || |   ) || (      | |   | || |   | || |          /#/
   | |   | )   ( || ) \ \__| (____/\| )   ( || (__/  )| )      | (___) || (___) || (____/\   /#/
   )_(   |/     \||/   \__/(_______/|/     \|(______/ |/       (_______)(_______)(_______/  (#/

    """
    pull_requests_pool = ThreadPool(processes=threads_count)

    for pull_request in repository.get_all_pull_requests(state=state):
        pull_requests_pool.apply_async(
            func=do,
            args=(start, stash, repository, pull_request, branch, pull_requests, from_date, to_date
        ))

    pull_requests_pool.close()
    pull_requests_pool.join()

    """
    _   _________          _______  _______  _______  ______   _______  _______  _______  _
   /#)  \__   __/|\     /|(  ____ )(  ____ \(  ___  )(  __  \ (  ____ )(  ___  )(  ___  )( \
  /#/      ) (   | )   ( || (    )|| (    \/| (   ) || (  \  )| (    )|| (   ) || (   ) || (
 /#/       | |   | (___) || (____)|| (__    | (___) || |   ) || (____)|| |   | || |   | || |
(#(        | |   |  ___  ||     __)|  __)   |  ___  || |   | ||  _____)| |   | || |   | || |
 \#\       | |   | (   ) || (\ (   | (      | (   ) || |   ) || (      | |   | || |   | || |
  \#\      | |   | )   ( || ) \ \__| (____/\| )   ( || (__/  )| )      | (___) || (___) || (____/\
   \#)     )_(   |/     \||/   \__/(_______/|/     \|(______/ |/       (_______)(_______)(_______/

    """
    return pull_requests
