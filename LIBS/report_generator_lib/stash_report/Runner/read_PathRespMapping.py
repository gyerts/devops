import configparser


def read_PathRespMapping(name_of_file, setting):
    output = {}
    config = configparser.ConfigParser()
    config.sections()
    config.read(name_of_file)

    for key in config[setting]:
        output[key] = config[setting][key]
    return output
