def get_comments(change, comments_counter, files):
    for file in files:
        #
        change.files.append(file.path.toString)
        change.formats.add(file.path.extension)
        #
        list_of_comments = file.get_comments()

        if len(list_of_comments) > 0:
            change.comments_text += file.path.name + "\n"
            for comment in list_of_comments:
                change.comments_text += comment.to_string(tab="   ")
                comments_counter += comment.count()

    return [change, comments_counter]
