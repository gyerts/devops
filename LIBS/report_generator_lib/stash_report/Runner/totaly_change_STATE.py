
def add_sense_to_all_state_of_changes(changes):
    """
    Before:

    All change.state(s) should be fulfilled by the pattern:
        "Direct commit" - no pull-request for this commit was found
        "Merged"        - this commit within the pull-request
        "Under review"  - means that the commit belong to opened pull request
    All change.reviewed should be fulfilled with number from 0.00 to 1.00

    After you will get changed information to:
        "Direct commit"
            change.state = "No review found"
        ("Merged" or "Under review") and change.reviewed < 1.00
            change.state = "Review in progress"
        ("Merged" or "Under review") and change.reviewed == 1.00
            change.state = "Review done"
    """

    for change in changes:
        if "Direct commit" in change.state:
            change.state = "No review found"
        elif ("Merged" in change.state) or ("Under review" in change.state):
            if change.reviewed < 1:
                change.state = "Review in progress"
            else:
                change.state = "Review done"
        else:
            raise ValueError("['Direct commit', 'Merged', 'Under review'] not found in {}".format(change.state))

    return changes
