from report_generator_lib.stash_report.Library.User import User
from report_generator_lib.stash_report.Library.Commit import Commit


class Action():
    def __init__(self, owner, json_obj_action):
        self.owner = owner

        self.dictionary = json_obj_action

        self.id               = json_obj_action["id"]
        self.createdDate      = json_obj_action["createdDate"]
        self.action           = json_obj_action["action"]
        self.user             = User(self, json_obj_action["user"])

        self.commits = list()

        try:
            self.fromHash         = json_obj_action["fromHash"]
            self.toHash           = json_obj_action["toHash"]
            self.previousFromHash = json_obj_action["previousFromHash"]
            self.previousToHash   = json_obj_action["previousToHash"]
        except:
            pass

        if "RESCOPED" == self.action:
            for changeset in json_obj_action["added"]["changesets"]:
                self.commits.append(Commit(self, "", "", changeset))

            for changeset in json_obj_action["removed"]["changesets"]:
                self.commits.append(Commit(self, "", "", changeset))

        if "MERGED" == self.action:
            self.commits.append(Commit(self, "", "", json_obj_action["changeset"]))


    def get_owner(self, info=False):
        if info:
            ans = self.owner.get_owner() + " -> Action: action=" + self.action
        else:
            ans = self.owner.get_owner() + " -> Action"
        return ans

    def to_dict(self):
        return self.dictionary
