from report_generator_lib.stash_report.Library.User import User


class Comment:
    def __init__(self, owner, dict_activity):
        self.owner = owner

        if "comment" in dict_activity:
            dict_activity = dict_activity["comment"]

        self.id = dict_activity["id"]
        self.createdDate = dict_activity["createdDate"]
        self.author = User(self, dict_activity["author"])
        self.text = dict_activity["text"]
        self.comments = self.get_replies(dict_activity)

    def to_string(self, *, tab="", cut=50):

        output = "{}{}: {}\n".format(tab, self.author.name, self.text.strip())

        output = output.replace("\n", "; ")

        if len(output) > cut:
            output = output[:cut - 3]
            output += "...\n"
        else:
            output += "\n"

        for comment in self.comments:
            output += comment.to_string(tab=tab+"   ")
        return output

    def get_owner(self):
        return self.owner.get_owner() + " -> Comment: id=" + self.id

    def get_replies(self, dict_activity):
        comments = list()
        if "comments" in dict_activity:
            for comment_dict in dict_activity["comments"]:
                comment = Comment(self, comment_dict)
                comments.append(comment)
        return comments

    def count(self):
        counter = 1
        for reply in self.comments:
            counter += reply.count()
        return counter
