from report_generator_lib.stash_report.Library.User import User


class Reviewer(User):
    def __init__(self, owner, dict_reviewer):
        self.owner = owner

        super().__init__(self, dict_reviewer["user"])
        self.role = dict_reviewer["role"]
        # self.approved = dict_reviewer["approved"]

        self.commits_to_review = dict()  # commiit_id = APPROVED/UNAPPROVED/NOT_REVIEWED

    def show(self, tab="   "):
        print(tab + "/* ----------- User -----------")
        super().show(tab)
        print(tab + "role:", self.role)
        # print(tab + "approved:", self.approved)
        print(tab + "----------- User ----------- */")

    def get_owner(self):
        return self.owner.get_owner() + " -> Reviewer: name=" + self.name

    def add_commit(self, commit_id):
        self.commits_to_review[commit_id] = "NOT_REVIEWED"

    def add_status_of_review(self, status):
        for commit in self.commits_to_review:
            self.commits_to_review[commit] = status

    def get_state_of_commit(self, commit_id):
        return self.commits_to_review[commit_id]

    def is_reviewed(self, commit_id):
        reviewed = False
        if commit_id in self.commits_to_review:
            state = self.commits_to_review[commit_id]
            if "NOT_REVIEWED" != state:
                reviewed = True
        else:
            raise ValueError("!!! Error !!! Reviewer.is_reviewed: commit_id %s is not under Reviewer %s \
            responsibility, POSSIBLE commit_id variable is a commit-object, not a commit-id" %
                             (commit_id, self.displayName))
        return reviewed
