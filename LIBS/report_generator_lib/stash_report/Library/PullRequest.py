from report_generator_lib.stash_report.Library.Link import Link
from report_generator_lib.stash_report.Library.Ref import Ref
from report_generator_lib.stash_report.Library.Reviewer import Reviewer
from report_generator_lib.stash_report.Library.Commit import Commit
from report_generator_lib.stash_report.Library.File import File
from report_generator_lib.stash_report.Library.Comment import Comment
from report_generator_lib.stash_report.Library.Action import Action

import os, time
from datetime import datetime


class PullRequest:
    def __init__(self, owner, stash, url, dict_pull_request):
        self.owner = owner

        self.parent_url = url
        self.stash = stash

        self.id          = dict_pull_request["id"]
        self.version     = dict_pull_request["version"]
        self.title       = dict_pull_request["title"]
        self.description = dict_pull_request["description"]
        self.state       = dict_pull_request["state"]
        self.open        = dict_pull_request["open"]
        self.closed      = dict_pull_request["closed"]

        cdate = time.ctime(dict_pull_request["createdDate"]/1000.0)
        self.createdDate = datetime.strptime(cdate, "%a %b %d %X %Y")

        cdate = time.ctime(dict_pull_request["updatedDate"]/1000.0)
        self.updatedDate = datetime.strptime(cdate, "%a %b %d %X %Y")

        self.fromRef     = Ref(self, stash, "fromRef", dict_pull_request["fromRef"])
        self.toRef       = Ref(self, stash, "toRef", dict_pull_request["toRef"])

        self.url = url + "/pull-requests/" + str(self.id)

        self.author       = Reviewer(self, dict_pull_request["author"])
        self.reviewers    = self.__get_reviewers(dict_pull_request["reviewers"])

        self.participants = dict_pull_request["participants"]
        self.link         = Link(self, dict_pull_request["link"])
        self.links        = dict_pull_request["links"]

        try:
            self.attributes = dict_pull_request["attributes"]
        except:
            self.attributes = None

        self.commits = list()
        self.changed_files = list()
        self.comments = list()
        self.activities = list()


# public --------------------------------------------------------------------------------------------------------
# public --------------------------------------------------------------------------------------------------------
# public ---------------------------------         PUBLIC METHODS       -----------------------------------------
# public --------------------------------------------------------------------------------------------------------
# public --------------------------------------------------------------------------------------------------------

    def get_owner(self, info=False):
        if info:
            ans = self.owner.get_owner() + " -> PullRequest: id=" + str(self.id)
        else:
            ans = self.owner.get_owner() + " -> PullRequest"
        return ans

    def get_commit_by_id(self, id):
        for commit in self.commits:
            if commit.id == id:
                return commit
        return None

    def contains(self, obj_commit):
        for commit in self.commits:
            if commit.id == obj_commit.id:
                return True
        return False

# fulfill --------------------------------------------------------------------------------------------------------
# fulfill --------------------------------------------------------------------------------------------------------
# fulfill ------------------------------       LAZY INITIALIZATION      ------------------------------------------
# fulfill --------------------------------------------------------------------------------------------------------
# fulfill --------------------------------------------------------------------------------------------------------

    def fulfill(self):
        self.__fulfill_commits()
        self.__fulfill_files()
        self.__fulfill_activities()

    def __fulfill_files(self):
        if not self.changed_files:
            if self.commits:
                for commit in self.commits:
                    for file in commit.files():
                        self.changed_files.append(file.path.toString)
            else:
                raise ValueError("First you should run function PullRequest.get_commits()")

    def __fulfill_commits(self):
        if not self.commits:
            url = self.url + "/commits?limit=500"
            # --------------------------------------------------------------------------------------------------
            example = "{server}/rest/api/1.0/projects/{project_id}/repos/{repo_slug}/pull-requests/{id_of_pull_request}/commits?limit=500"
            # --------------------------------------------------------------------------------------------------

            # print("*****************************", url)
            ans = self.stash.rest_request(example, url)
            for commit in ans['values']:
                commit_to_add = Commit(self, self.parent_url, self.stash, commit, self.url)
                self.commits.append(commit_to_add)

                for reviewer in self.reviewers:
                    reviewer.add_commit(commit_to_add.id)

    def __fulfill_activities(self):
        if not self.activities:
            url = self.url + "/activities?limit=500"
            # --------------------------------------------------------------------------------------------------
            example = "{server}/rest/api/1.0/projects/{project_id}/repos/{repo_slug}/pull-requests/{id_of_pull_request}/activities?limit=500"
            # --------------------------------------------------------------------------------------------------
            json_object = self.stash.rest_request(example, url)["values"]

            for action in json_object:
                if "COMMENTED" == action["action"]:
                    comment = Comment(self, action)
                    try:
                        # if this comment attached to a file
                        self.comments.append((os.path.basename(action["commentAnchor"]["path"]), comment))
                    except:
                        # else
                        self.comments.append(("pull-request 'GLOBAL' comment", comment))
                else:
                    self.activities.insert(0, Action(self, action))

            for action in self.activities:
                for reviewer in self.reviewers:
                    if "APPROVED" == action.action:
                        if reviewer.name == action.user.name:
                            reviewer.add_status_of_review("APPROVED")
                            break

                    elif "UNAPPROVED" == action.action:
                        if reviewer.name == action.user.name:
                            reviewer.add_status_of_review("UNAPPROVED")
                            break

                    elif "RESCOPED" == action.action:
                        for commit in action.commits:
                            reviewer.add_commit(commit.id)

                    elif "MERGED" == action.action:
                        for commit in action.commits:
                            reviewer.add_commit(commit.id)

                    elif "OPENED" == action.action:
                        reviewer.add_status_of_review("NOT_REVIEWED")

# private --------------------------------------------------------------------------------------------------------
# private --------------------------------------------------------------------------------------------------------
# private ---------------------------------         INSIDE METHODS       -----------------------------------------
# private --------------------------------------------------------------------------------------------------------
# private --------------------------------------------------------------------------------------------------------

    def __get_changed_files(self):
        response = self.stash.rest_request(self.url + "/changes")["values"]
        files = list()
        for file in response:
            files.append(File(self.stash, self.url, file))
        return files

    def __get_reviewers(self, dict_users):
        reviewers = list()
        for user in dict_users:
            reviewers.append(Reviewer(self, user))
        return reviewers




















    def show(self, tab="   "):
        print(tab + "id: ", self.id)
        print(tab + "version: ", self.version)
        print(tab + "title: ", self.title)
        print(tab + "description: ", self.description)
        print(tab + "state: ", self.state)
        print(tab + "open: ", self.open)
        print(tab + "closed: ", self.closed)
        print(tab + "createdDate: ", self.createdDate)
        print(tab + "updatedDate: ", self.updatedDate)
        self.fromRef.show(tab)
        self.toRef.show(tab)

        self.author.show(tab)

        print(tab + "reviewers: ")
        for reviewer in self.reviewers:
            reviewer.show(tab + tab)

        print(tab + "participants: ", self.participants)
        # print(tab + "attributes: ", self.attributes)
        self.link.show(tab)
        print(tab + "links: ", self.links)

        print(tab + "commits: ")
        for commit in self.commits:
            commit.show(tab + "   ")

        print(tab + "files: ")
        for file in self.changed_files:
            print(tab + "   " + file)

        print(tab + "comments: ")
        for comment in self.comments:
            print(tab + "   " + str(comment))
