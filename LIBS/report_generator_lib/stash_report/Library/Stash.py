import urllib.request, base64, json
import time
from report_generator_lib.stash_report.Library.Project import Project


class Stash:
    def __init__(self, owner, path_to_stash, login, password):
        self.owner = owner

        self.url = self.__correct_path(path_to_stash) + "/rest/api/1.0"
        self.basic = (b"Basic " + base64.b64encode(login.encode()+b":"+password.encode())).decode()

        self.requests = {
            "unique": list(),
            "duplicated": list()
        }

    def get_owner(self, info=False):
        if info:
            ans = self.owner.get_owner() + " -> Stash: url=" + self.url
        else:
            ans = self.owner.get_owner() + " -> Stash"
        return ans

    def rest_request(self, example, url, method="GET"):
        # print("/|======")
        # print("||", example)
        # print("   [REQUEST] -> ", url)
        # print("\|======")

        if url in self.requests["unique"]:
            self.requests["duplicated"].append(url)
            raise ValueError("WARNING: should be no same requests to Stash:", url)
        else:
            self.requests["unique"].append(url)

        req = urllib.request.Request(url)
        req.add_header("Authorization", self.basic)
        req.add_header("Content-Type", "application/json")
        req.method = method

        num = 5
        for _try in range(num):
            try:
                response = urllib.request.urlopen(req).read().decode()
                if _try > 0:
                    pass
                    # print(" - SUCCESS :)")
                break
            except urllib.request.HTTPError as error:
                if _try == num - 1:
                    # print(" - RAISE :(")
                    raise error
                else:
                    # print("some error occurred: {}, I WILL MAKE ANOTHER TRY IN 5 SEC".format(error.msg), end="")
                    time.sleep(5)
                    print("   ...  REQUEST  -> ", url)

        print("        REQUEST  -> ", url)

        return json.loads(response)

    def get_all_projects(self):
        projects = list()
        # to get all projects -------------------
        example = "{server}/rest/api/1.0/projects"
        # ---------------------------------------
        for project in self.rest_request(example, self.url + "/projects")['values']:
            projects.append(Project(self, self, project))
        return projects

    def get_project_by_name(self, name):
        for project in self.get_all_projects():
            if project.name == name:
                return project
        return None

    def get_project_by_key(self, key):
        for project in self.get_all_projects():
            if project.key == key:
                return project
        return None

    def get_project_by_id(self, id):
        for project in self.get_all_projects():
            if project.id == id:
                return project
        return None
        
    def __correct_path(self, path):
        if '/' == path[-1]:
            path = path[0:-1]
        return path
