from report_generator_lib.stash_report.Library.Link import Link
from report_generator_lib.stash_report.Library.Commit import Commit
from datetime import datetime

class Repository:
    def __init__(self, owner, stash, project, dict_repository):
        self.owner = owner

        self.stash = stash
        self.project = project

        self.statusMessage = dict_repository["statusMessage"]
        self.name          = dict_repository["name"]
        self.slug          = dict_repository["slug"]
        self.id            = dict_repository["id"]
        self.cloneUrl      = dict_repository["cloneUrl"]
        self.state         = dict_repository["state"]
        self.scmId         = dict_repository["scmId"]
        self.links         = dict_repository["links"]
        self.public        = dict_repository["public"]
        self.link          = Link(self, dict_repository["link"])

        try:
            self.forkable = dict_repository["forkable"]
        except:
           self.forkable = None

        self.url = "%s/repos/%s" % (project.url, self.slug)

    def get_owner(self, info=False):
        if info:
            ans = self.owner.get_owner() + " -> Repository: slug=" + self.slug
        else:
            ans = self.owner.get_owner() + " -> Repository"
        return ans

    def commits(self, limit, branch, from_date, to_date):
        """
        limit - show N commits
        branch - master, etc
        """
        self.branch = branch
        # --------------------------------------------------------------------------------------------------
        example = "{server}/rest/api/1.0/projects/{project_id}/repos/{repo_slug}/" \
                  "commits?limit={commits_to_show}&until={branch_name}"
        # --------------------------------------------------------------------------------------------------
        url = "{}/commits?limit={}&until={}".format(self.url, limit, branch)

        commits = list()
        commits_json = self.stash.rest_request(example, url)["values"]

        for commit in commits_json:
            commit = Commit(
                    owner=self,
                    url=self.url,
                    stash=self.stash,
                    dict_commit=commit
                )

            # append this commit if it between from - to dates
            date_object = datetime.strptime(commit.authorTimestamp, '%a %b %d %X %Y')
            if (date_object >= from_date) and (date_object <= to_date):
                commits.append(commit)

        return commits

    def get_all_pull_requests(self, state="open"):
        """
        state could be:
            open
            merged
            declined
        """

        # --------------------------------------------------------------------------------------------------
        example = "{server}/rest/api/1.0/projects/{project_id}/repos/{repo_slug}/pull-requests/{id_of_pull_request}/[?state={state}]"
        # --------------------------------------------------------------------------------------------------
        url = self.url + "/pull-requests?state={}&limit=20000".format(state)
        return self.stash.rest_request(example, url)['values']

    def show(self):
        print("statusMessage =", self.statusMessage)
        print("name =", self.name)
        print("slug =", self.slug)
        print("id =", self.id)
        print("cloneUrl =", self.cloneUrl)
        print("state =", self.state)
        print("scmId =", self.scmId)
        print("links =", self.links)
        print("public =", self.public)
        print("forkable =", self.forkable)
        print("link:")
        self.link.show()
        print('\n\n')
