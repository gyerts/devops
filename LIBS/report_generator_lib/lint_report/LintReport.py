class LintReport:
    def __init__(self):
        self.sourceFile = str()
        self.rawArr = list()
        self.finalArr = list()


    def getOccurrenceLine(self, text):
        result = None

        for line in self.rawArr:
            if text in line:
                domaninName = line.pop().replace(">", " / ")
                line.insert(0, domaninName)
                result = line
                break

        return result


    def getLintStatistics(self):
        with open(self.sourceFile, "r") as file:
            ctx = file.read()
            start = ctx.find('HB_PRIO_1  HB_PRIO_2  HB_PRIO_3  HB_PRIO_4  HB_PRIO_5  HB_PRIO_6  Path')
            stringList = ctx[start:].split("\n")
            self.rawArr = list()

            for x in stringList:
                if len(x) and not "<unknown_file>" in x:
                    self.rawArr.append(x.split())

            #[print(x) for x in self.rawArr]

            self.finalArr = [
                self.getOccurrenceLine('Path'),
                self.getOccurrenceLine('Applications>Engine'),
                self.getOccurrenceLine('Applications>HTML'),
                self.getOccurrenceLine('Diagnosis'),
                self.getOccurrenceLine('HMI'),
                self.getOccurrenceLine('OnlineServices'),
                self.getOccurrenceLine('Speech'),
                self.getOccurrenceLine('SWUpdate'),
                self.getOccurrenceLine('SWSystem>AvcLan'),
                self.getOccurrenceLine('SWSystem>Connectivity'),
                self.getOccurrenceLine('SWSystem>IODevices'),
                self.getOccurrenceLine('SWSystem>SystemCore')
            ]

            #[print(x) for x in self.finalArr]

        return self.finalArr