import traceback
import xlrd
import xlsxwriter
from report_generator_lib.utility.FileSystem import FileSystem as FS
from report_generator_lib.utility.htmlCtx import *


class DoorsReport:
    def __init__(self):
        self.dirWithRawDoorsExtracts = str()
        self.saveToDir = str()
        self.log = print
        self.endCall = lambda : None
        self.setProgressMax = lambda x: None
        self.setProgressValue = lambda x: None
        self.incProgress = lambda : None
        #***************************************************************************************************************
        self.allTrsList = list()


    def convert(self):
        files = FS.getFilesInDir(".xlsx", self.dirWithRawDoorsExtracts)
        self.log(colorStr("%d xlsx file were found in given directory" % len(files), "green"))
        self.allTrsList = list()
        FS.checkCreateDir(self.saveToDir)

        self.setProgressMax(len(files))

        for x in files:
            table = self._parseRawFile(x)
            self._table2Xlsx(FS.join(self.saveToDir, FS.fileName(x)), table)
            self.incProgress()

        self._generateAllTrsListFile(FS.join(self.saveToDir, "all_trs_list.txt"))
        self.log(colorStr("Process has been finished".upper(), "green"))

        self.endCall()


    def _parseRawFile(self, path):
        table = list()
        self.log(colorStr("Processing file [%s]" % path, "blue"))
        table.append(["TRS ID", "TRS Content", "HB_Level_of_Maturity"])

        try:
            workbook = xlrd.open_workbook(path)
            sheet = workbook.sheet_by_index(0)

            trsIdIndex = self._takeColByName(sheet, "TRS ID")
            trsContent = self._takeColByName(sheet, "TRS Content")
            hbIndex = self._takeColByName(sheet, "HB_Level_of_Maturity")
            inLinks = self._takeColByName(sheet, "In-links (All modules)")

            if not -1 in [trsIdIndex, trsContent, hbIndex, inLinks]:
                for i in range(0, sheet.nrows):
                    row = sheet.row(i)

                    if len(row[trsIdIndex].value) and len(row[hbIndex].value) and "HLF" in row[inLinks].value:
                        table.append([row[trsIdIndex].value, row[trsContent].value, row[hbIndex].value])
                        if row[trsIdIndex].value != "TRS ID":
                            self.allTrsList.append(row[trsIdIndex].value)

        except:
           self.log(colorStr("Error while parsing %s. Details: %s"
                                    % (path, traceback.format_exc().replace("\n", "<br>")), "red"))

        return table


    def _takeColByName(self, sheet, colName):
        result = -1

        for i in range(sheet.ncols):
            col = sheet.col(i)
            if col[0].value == colName:
                result = i
                break

        if result == -1:
            self.log(colorStr("No such col name found: %s" % colName, "red"))

        return result


    def _table2Xlsx(self, path, table):
        try:
            xlsx = xlsxwriter.Workbook(path)

            formatNormal = xlsx.add_format({'valign': 'top',
                                            'text_wrap': True,
                                            'border': True})


            formatHeader = xlsx.add_format({'bold': True,
                                            'text_wrap': True,
                                            'align':'center',
                                            'valign': 'vcenter',
                                            'font_size': 12,
                                            'bg_color': '#C0C0C0',
                                            'border': True})


            xlsxSheet = xlsx.add_worksheet()
            xlsxSheet.autofilter("A1:C1")
            xlsxSheet.set_column("A:A", 40)
            xlsxSheet.set_column("B:B", 160)
            xlsxSheet.set_column("C:C", 40)

            for i in range(len(table)):
                for j in range(len(table[i])):
                    if i == 0:
                        xlsxSheet.write(i, j, table[i][j], formatHeader)

                    else:
                        xlsxSheet.write(i, j, table[i][j], formatNormal)

            xlsxSheet.freeze_panes(1, 0)
            xlsx.close()

        except:
            self.log(colorStr("Error while saving %s. Details: %s"
                                    % (path, traceback.format_exc().replace("\n", "<br>")), "red"))


    def _generateAllTrsListFile(self, path):
        try:
            file = open(path, 'w')
            sortedTrs = sorted(self.allTrsList)

            for i in sortedTrs:
                file.write(i + "\n")

            file.close()
            self.log(colorStr("All TRS list file generated", "green"))

        except FileNotFoundError:
            self.log(colorStr("All TRS list file generating error", "red"))
