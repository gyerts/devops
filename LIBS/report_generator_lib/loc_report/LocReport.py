import os

class Fields:
    def __init__(self, line_from_loc_report='empty;OK;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0', path=None):
        obj = line_from_loc_report.strip().replace(' ', '').split(';')

        self.nof = 0
        self.c = 0
        self.h = 0
        self.cpp = 0
        self.hpp = 0

        if path is None:
            self.Path = obj[0]
            extension = os.path.splitext(self.Path)[1]
            if extension == '.c':        self.c += 1
            elif extension == '.cpp':    self.cpp += 1
            elif extension == '.h':      self.h += 1
            elif extension == '.hpp':    self.hpp += 1
            self.nof += 1

        else:
            self.Path = path

        self.Checks = obj[1]
        self.LoC    = int(obj[2])
        self.ELoC   = int(obj[3])
        self.SLoC   = int(obj[4])
        self.PSLoC  = int(obj[5])
        self.BSLoC  = int(obj[6])
        self.CLoC   = int(obj[7])
        self.FCLoC  = int(obj[8])
        self.ICLoC  = int(obj[9])
        self.CCLoC  = int(obj[10])
        self.DCLoC  = int(obj[11])
        self.XLoC   = int(obj[12])
        self.SXLoC  = int(obj[13])
        self.PXLoC  = int(obj[14])
        self.DXLoC  = int(obj[15])
        self.RDLoC  = int(obj[16])

    def add(self, objFields):
        self.nof   += objFields.nof
        self.c     += objFields.c
        self.h     += objFields.h
        self.cpp   += objFields.cpp
        self.hpp   += objFields.hpp

        self.LoC   += objFields.LoC
        self.ELoC  += objFields.ELoC
        self.SLoC  += objFields.SLoC
        self.PSLoC += objFields.PSLoC
        self.BSLoC += objFields.BSLoC
        self.CLoC  += objFields.CLoC
        self.FCLoC += objFields.FCLoC
        self.ICLoC += objFields.ICLoC
        self.CCLoC += objFields.CCLoC
        self.DCLoC += objFields.DCLoC
        self.XLoC  += objFields.XLoC
        self.SXLoC += objFields.SXLoC
        self.PXLoC += objFields.PXLoC
        self.DXLoC += objFields.DXLoC
        self.RDLoC += objFields.RDLoC

        if objFields.Checks != 'OK':
            self.Checks = objFields.Checks

    @staticmethod
    def get_header():
        return ['Path', 'LoC', 'ELoC', 'SLoC', 'PSLoC', 'BSLoC', 'CLoC', 'FCLoC',
                'ICLoC', 'CCLoC', 'DCLoC', 'XLoC', 'SXLoC', 'PXLoC', 'DXLoC', 'RDLoC',
                '', 'files', '№ .c', '№ .h', '№ .cpp',
                '№ .hpp']

    def get_list(self):
        return [self.Path, self.LoC, self.ELoC, self.SLoC, self.PSLoC, self.BSLoC, self.CLoC, self.FCLoC,
                self.ICLoC, self.CCLoC, self.DCLoC, self.XLoC, self.SXLoC, self.PXLoC, self.DXLoC, self.RDLoC,
                '', self.nof, self.c, self.h, self.cpp, self.hpp]

class LocFile:
    def __init__(self, objFields):
        self.fields = objFields

class LocFolder:
    def __init__(self, path):
        self.path = path
        self.files = list()

    def add_file(self, objLocFile):
        self.files.append(objLocFile)

    def get_loc(self):
        output = Fields(path=self.path)
        for file in self.files:
            output.add(file.fields)

        return output


class Domain:
    def __init__(self, name):
        self.name = name
        self.registered_paths = list()

    def add_folder(self, path):
        self.registered_paths.append(LocFolder(path))

    def get_loc(self):
        output = Fields(path=self.name)
        for folder in self.registered_paths:
            output.add(folder.get_loc())

        return output.get_list()


class LocReport:
    def __init__(self):
        self.path2domainMappingFile = str()
        self.pathLocCsvFile = str()



    def get_non_gen_sctatisctics(self):
        csv_file = open(self.pathLocCsvFile).read().strip().split('\n')
        del(csv_file[0])
        del(csv_file[0])
        del(csv_file[0])
        copy = csv_file.copy()

        return get_non_generated_domains_files(csv_file, copy, self.path2domainMappingFile)


    def get_gen_sctatictics(self):
        csv_file = open(self.pathLocCsvFile).read().strip().split('\n')
        del(csv_file[0])
        del(csv_file[0])
        del(csv_file[0])
        copy = csv_file.copy()

        return get_generated_domains_files(copy, self.path2domainMappingFile)


    def get_other_sctatictics(self):
        csv_file = open(self.pathLocCsvFile).read().strip().split('\n')
        del(csv_file[0])
        del(csv_file[0])
        del(csv_file[0])
        copy = csv_file.copy()

        return get_other_files(copy)


# fetching mapping from LintResponsibles.ini file
# in LintResponsibles.ini should be [PathRespMapping] line
def parse_LintResponsibles_file(path_to_LintResponsibles):
    lr_file = open(path_to_LintResponsibles).read().split('\n')
    map = dict()
    start = False
    for line in lr_file:
        if '[PathRespMapping]' in line:
            start = True
            continue

        # [PathRespMapping] block finished
        if '[' in line:
            break

        if start:
            line = line.split('=')
            if len(line) > 1:
                domain_name = line[1].strip()
                if domain_name not in map:
                    map[domain_name] = list()
                map[domain_name].append(line[0].strip())
    return map


# add full path to relative paths of map
# example: imp/Diagnosis -> /home/usr/project/imp/Diagnosis
def add_asolute_path_to_mapping(map, path_to_project):
    new_map = dict()
    for domain in map:
        new_paths = list()
        for path in map[domain]:
            new_paths.append(os.path.join(path_to_project, path))
        new_map[domain] = new_paths
    return new_map


def convert_dict_to_Domin_objects(map):
    new_map = list()
    for domain in map:
        new_domain = Domain(domain)
        for path in map[domain]:
            new_domain.add_folder(path)
        new_map.append(new_domain)
    return new_map


def rename(find_in_col_idx, old_name, move_to_row_idx, with_new_name, find_in_array):
    for line in find_in_array:
        if line[find_in_col_idx] == old_name:
            new_line = line.copy()
            new_line[find_in_col_idx] = with_new_name

            find_in_array.remove(line)
            find_in_array.insert(move_to_row_idx, new_line)


def get_domains_files(csv_file, map, copy, exclude=None):
    for line in csv_file:
        for domain in map:
            for objPath in domain.registered_paths:
                if objPath.path in line:
                    if exclude != None and exclude in line:
                        continue
                    else:
                        objPath.add_file(LocFile(Fields(line)))
                        copy.remove(line)
    return map


def convert_map_to_list(map):
    RESULT_OF_SCRIPT = list()
    RESULT_OF_SCRIPT.append(Fields.get_header())
    for domain in map:
        RESULT_OF_SCRIPT.append(domain.get_loc())


    rename(0, 'Applications>Engine',   1,  'Applications / Engine', RESULT_OF_SCRIPT)
    rename(0, 'Applications>HTML',     2,  'Applications / HTML', RESULT_OF_SCRIPT)
    rename(0, 'Diagnosis',             3,  'Diagnosis', RESULT_OF_SCRIPT)
    rename(0, 'HMI',                   4,  'HMI', RESULT_OF_SCRIPT)
    rename(0, 'OnlineServices',        5,  'OnlineServices', RESULT_OF_SCRIPT)
    rename(0, 'Speech',                6,  'Speech', RESULT_OF_SCRIPT)
    rename(0, 'SWUpdate',              7,  'SWUpdate', RESULT_OF_SCRIPT)
    rename(0, 'SWSystem>AvcLan',       8,  'SWSystem / AvcLan', RESULT_OF_SCRIPT)
    rename(0, 'SWSystem>Connectivity', 9,  'SWSystem / Connectivity', RESULT_OF_SCRIPT)
    rename(0, 'SWSystem>IODevices',    10, 'SWSystem / IODevices', RESULT_OF_SCRIPT)
    rename(0, 'SWSystem>SystemCore',   11, 'SWSystem / SystemCore', RESULT_OF_SCRIPT)

    return RESULT_OF_SCRIPT


def get_non_generated_domains_files(csv_file, copy, path_to_LintResponsibles):
    map = parse_LintResponsibles_file(path_to_LintResponsibles)
    map = convert_dict_to_Domin_objects(map)
    map = get_domains_files(csv_file, map, copy, exclude='/gen/')
    RESULT_OF_SCRIPT = convert_map_to_list(map)
    return RESULT_OF_SCRIPT


def get_generated_domains_files(copy, path_to_LintResponsibles):
    csv_file = copy.copy()
    map = parse_LintResponsibles_file(path_to_LintResponsibles)
    map = convert_dict_to_Domin_objects(map)
    map = get_domains_files(csv_file, map, copy)
    RESULT_OF_SCRIPT = convert_map_to_list(map)
    return RESULT_OF_SCRIPT


def get_other_files(copy):
    csv_file = copy.copy()
    map = {'sys': ['gen/api/sys/', 'gen/imp/sys/'], 'navigation': ['gen/api/nav']}
    map = convert_dict_to_Domin_objects(map)
    map = get_domains_files(csv_file, map, copy)
    RESULT_OF_SCRIPT = convert_map_to_list(map)
    return RESULT_OF_SCRIPT



def print_unknown_files(copy):
    # end of script
    if len(copy) > 0:
        print('<h1 style="color:red">Files left out of LOC counting: ' + str(len(copy)) + ', you should add them to "LintResponsibles.ini" file </h1>')
        for line in copy:
           print(line)