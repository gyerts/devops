import paramiko
import re
import xml.etree.ElementTree as ET
from report_generator_lib.trs_tse_report.TrsTseItem import TrsTseItem
from report_generator_lib.utility.FileSystem import FileSystem as FS
from report_generator_lib.utility.NiceData import NiceData as ND
from report_generator_lib.utility.Scp import SCPClient
from report_generator_lib.utility.other import *
from report_generator_lib.utility.htmlCtx import *


class TrsTseReport:
    def __init__(self):
        self.trsListPath = str()
        self.trsFilter = str()
        self.clientDir = str()
        self.productsDir = str()
        self.targetIp = str()
        self.targetPort = str()
        self.targetUser = str()
        self.targetPassword = str()
        self.targetBinDir = str()
        self.preExecSettings = None
        self.getHbtsOption = False
        self.getHbcmOption = False
        self.getBinOption = False
        self.execTestsOption = False
        self.log = print
        self.cmdTimeout = 300
        self.endCall = lambda : None
        self.setProgressMax = lambda x: None
        self.setProgressValue = lambda x: None
        self.incProgress = lambda : None
        self.saveDumps = False
        #******************************
        self.features = list()
        self.paramikoClient = None
        self.scpClient = None


    def start(self):
        self.log(colorStr("Processing trs list".upper(), "purple"))
        self.processTrsList()

        if self.getHbtsOption:
            self.log(colorStr("Searching hbts files".upper(), "purple"))
            self.getHbtsFiles()

        if self.getHbcmOption:
            self.log(colorStr("Searching hbcm files".upper(), "purple"))
            self.getHbcmFiles()

        if self.getBinOption:
            self.log(colorStr("Searching hbac files".upper(), "purple"))
            self.getHbacFiles()
            self.log(colorStr("Searching bin files".upper(), "purple"))
            self.getBinFiles()

        if self.execTestsOption:
            self.log(colorStr("Executing test on target".upper(), "purple"))
            self.execTestsOnTarget()

        self.log(colorStr("Process has been finished".upper(), "green"))

        if self.saveDumps:
            self.saveDump()

        self.endCall()


    def saveDump(self):
        data = {"root": [f.serialize() for f in self.features]}
        FS.checkCreateDir("data")
        ND.dict2json(data, "data/dump_trs_tse_report.json")


    def processTrsList(self):
        with open(self.trsListPath, "r") as file:
            data = file.read().splitlines()
            file.close()
            self.features = [TrsTseItem(x, self) for x in data if x != "" and self.trsFilter in x]


    def getHbtsFiles(self):
        files = FS.find(".hbts", self.clientDir, FS.endsWith)
        self.log(colorStr("%d hbts file were found in given directory" % len(files), "green"))
        occurrencesMap = self.parseHbtsFiles(files)

        matched = list()

        self.setProgressValue(0)
        self.setProgressMax(len(self.features))

        for i in self.features:
            self.log(colorStr("Searching hbts files for feature [%s]" % i.id, "blue"))

            if i.id in occurrencesMap.keys():
                for occurrence in occurrencesMap[i.id]:
                    name = FS.fileName(occurrence)
                    path = occurrence
                    i.hbtsFiles.append({"name": name, "path": path})

                matched.append(i.id)

            self.incProgress()


    def getHbcmFiles(self):
        files = FS.find(".hbcm", self.clientDir, FS.endsWith)
        self.log(colorStr("%d hbcm file were found in given directory" % len(files), "green"))
        occurrencesMap = self.parseHbcmFiles(files)

        matched = list()
        self.setProgressValue(0)
        self.setProgressMax(len(self.features))

        for i in self.features:
            self.log(colorStr("Searching hbcm files for feature [%s]" % i.id, "blue"))

            if i.id in occurrencesMap.keys():
                for occurrence in occurrencesMap[i.id]:
                    name = FS.fileName(occurrence)
                    path = occurrence
                    i.hbcmFiles.append({"name": name, "path": path})

                matched.append(i.id)

            self.incProgress()


    def parseHbtsFiles(self, files):
        map = dict()

        for i in range(len(files)):
            curFilePath = files[i]
            searchResults = list()
            tree = ET.parse(curFilePath)
            root = tree.getroot()
            cutDescriptionTag = root[0].find(".//cutDescription")

            if cutDescriptionTag is not None:
                searchResults += re.findall("(TRS-MEUCY17[\w\-]+)", cutDescriptionTag.get("description"))

            for i in root[0].findall(".//testOrigins"):
                searchResults += re.findall("(TRS-MEUCY17[\w\-]+)", i.get("id"))

            searchResults = list(set(searchResults))

            for trs in searchResults:
                if trs in map.keys():
                    map[trs].append(curFilePath)
                else:
                    map[trs] = [curFilePath]

        return map


    def parseHbcmFiles(self, files):
        map = dict()

        for i in range(len(files)):
            curFilePath = files[i]
            searchResults = list()
            tree = ET.parse(curFilePath)
            root = tree.getroot()
            cutDescriptionTag = root[0].find(".//componentDescription")

            if cutDescriptionTag is not None:
                comment = cutDescriptionTag.get("comment")
                if comment  is not None:
                    searchResults += re.findall("(TRS-MEUCY17[\w\-]+)", cutDescriptionTag.get("comment"))
                    searchResults = list(set(searchResults))

            for trs in searchResults:
                if trs in map.keys():
                    map[trs].append(curFilePath)
                else:
                    map[trs] = [curFilePath]

        return map


    def getHbacFiles(self):
        self.setProgressValue(0)
        self.setProgressMax(len(self.features))

        for f in self.features:
            f.getHbacFiles()
            self.incProgress()


    def getBinFiles(self):
        self.setProgressValue(0)
        self.setProgressMax(len(self.features))

        for f in self.features:
            f.getBinFiles()
            self.incProgress()


    def execTestsOnTarget(self):
        self.paramikoClient = paramiko.SSHClient()
        self.paramikoClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        connected = False

        try:
            self.paramikoClient.connect(self.targetIp, int(self.targetPort), self.targetUser, self.targetPassword)
            connected = True
        except:
            self.log(colorStr("Cannot connect via ssh. Detials: %s" % traceErr(), "red"))

        if connected:
            self.scpClient = SCPClient(self.paramikoClient.get_transport())

            self.setProgressValue(0)
            self.setProgressMax(len(self.features))

            for f in self.features:
                f.getExecResults()
                self.incProgress()

            self.scpClient.close()
            self.paramikoClient.close()
