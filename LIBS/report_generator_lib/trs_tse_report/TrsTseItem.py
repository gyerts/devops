import re
import xml.etree.ElementTree as ET

from report_generator_lib.utility.FileSystem import FileSystem as FS
from report_generator_lib.utility.other import traceErr

from report_generator_lib.utility.htmlCtx import *


class TrsTseItem():
    def __init__(self, id, parent):
        self.id = id
        self.hbtsFiles = list()
        self.hbcmFiles = list()
        self.hbacFiles = list()
        self.binFiles = list()
        self.parent = parent

        self.logNormal = parent.log


    def serialize(self):
        return {"trsFeature": {"id": self.id,
                               "hbtsFiles": list(self.hbtsFiles),
                               "hbcmFiles": list(self.hbcmFiles),
                               "hbacFiles": list(self.hbacFiles),
                               "binFiles": list(self.binFiles)}}

    @staticmethod
    def deserialize(data, parent):
        obj = TrsTseItem(data["id"], parent)
        obj.hbtsFiles = data["hbtsFiles"]
        obj.hbcmFiles = data["hbcmFiles"]
        obj.hbacFiles = data["hbacFiles"]
        obj.binFiles = data["binFiles"]

        return obj


    def getHbacFiles(self):
        self.logNormal(colorStr("Getting hbac for feature [%s]" % self.id, "blue"))
        self.hbacFiles.clear()

        for i in self.hbtsFiles:
            k = self._getHbacFilePath(i["path"])
            if k and k not in self.hbacFiles:
                self.hbacFiles.append(k)


    def getBinFiles(self):
        self.logNormal(colorStr("Getting binaries for feature [%s]" % self.id, "blue"))
        self.binFiles.clear()

        for i in self.hbacFiles:
            k = self._getBinName(i["path"])
            if k and k not in self.binFiles:
                self.binFiles.append(k)


    def getExecResults(self):
        self.logNormal(colorStr("Executing tests for feature [%s]" % self.id, "blue"))

        for bin in self.binFiles:
            if len(bin["path"]):
                # TEST********************************

                #alreadyTested = self.optimization1(bin["path"])


                if self.optimization2(bin):
                    self.logNormal("Test binary [%s] from [%s] was already tested" % (bin["name"], bin["path"]))
                    self.logNormal(colorStr(bin["execResult"], "grey"))

                else:
                    self._execTest(bin)


                #**************************************
                #bin["execResult"] = self._execTest(bin)


    def optimization1(self, binFilePath):
        result = str()

        for f in self.parent.features:
            for b in f.binFiles:
                if b["path"] == binFilePath and len(b["execResult"]):
                    return b["execResult"]

        return result


    def optimization2(self, binFile):
        result = False

        for f in self.parent.features:
            for b in f.binFiles:
                if b["path"] == binFile["path"] and len(b["execResult"]):
                    binFile["execResult"] =  b["execResult"]
                    binFile["details"] =  b["details"]
                    result = True

        return result



    def _getHbacFilePath(self, pathToHbtsFile):
        result = None
        folderPath = FS.dirName(pathToHbtsFile)
        jamfilePath = FS.join(folderPath, "Jamfile")

        if FS.isFile(jamfilePath):
            hbtsFileName = (pathToHbtsFile.split("/"))[-1]

            with open(jamfilePath, "r", errors = "ignore") as ctx:
                r = re.findall("%s[\w\s\#\.]+:[\s]+([\w]+)" % hbtsFileName, ctx.read())
                ctx.close()

                if len(r):
                    name = r[0] + ".hbac"
                    path = FS.join(folderPath, name)

                    if FS.isFile(path):
                        result = {"name": name, "path": path}

        return result


    def _getBinName(self, pathToHbacFile):
        result = None
        tree = ET.parse(pathToHbacFile)
        root = tree.getroot()
        processesTag = root.find(".//processes")

        name = processesTag.get("name")
        path = self._getBinPath(name)

        if processesTag:
            result = {"name": name, "path": path, "execResult": str(), "details": str()}

        return result


    def _getBinPath(self, binaryName):
        result = str()
        searchRes = FS.find(binaryName, self.parent.productsDir, FS.fullMatch)

        if len(searchRes):
            result = searchRes[0]

        return result


    def _execTest(self, binary):
        self.logNormal("Executing test [%s] from [%s]" % (binary["name"], binary["path"]))

        try:
            self.parent.scpClient.put(binary["path"], self.parent.targetBinDir)
            binPathOnTarget = FS.join(self.parent.targetBinDir, binary["name"])
            self._runCommand("chmod +x " + binPathOnTarget, self.parent.cmdTimeout)
            command = binPathOnTarget

            if self.parent.preExecSettings:
                command = self.parent.preExecSettings + command

            try:
                binary["execResult"] = self._runCommand(command, self.parent.cmdTimeout)
                binary["details"] = self._runCommand("cat %s.hbtr" % binary["name"])

            except:
                binary["execResult"] = "Timeout exceeded %d sec" % self.parent.cmdTimeout

            self._runCommand("rm -f " + binPathOnTarget, self.parent.cmdTimeout)

        except:
            self.logNormal("Error. Details: %s" % traceErr())


    def _runCommand(self, command, timeout = None):
        self.logNormal(colorStr(command))
        stdin, stdout, stderr = self.parent.paramikoClient.exec_command(command, timeout = timeout)
        output = (stdout.read().decode(errors = "ignore") + " " + stderr.read().decode(errors = "ignore")).strip()

        if len(output):
            self.logNormal(colorStr(output, "cyan"))

        return output