from report_generator_lib.utility.other import traceErr
from report_generator_lib.utility.htmlCtx import *
from multiprocessing.pool import ThreadPool

class Commit:
    def __init__(self, cl, date, reviewKey, parent, crusible_repo_name, author):
        self.cl = cl
        self.date = date
        self.author = author
        self.reviewKey = reviewKey
        self.reviewUrl = str()
        self.reviewState = str()
        self.reviewers = list()
        self.files = list()
        self.commentsCount = 0
        self.commentsText = str()
        self.timeSpent = str()
        self.foundInPer = False
        self.foundInCru = False

        self.parent = parent
        self.logNormal = parent.log

        # TEMP VARS
        self.REPO_NAME = crusible_repo_name


    def serialize(self):
        return {
            "commit": { "cl": self.cl,
            "date": self.date,
            "author": self.author,
            "reviewKey": self.reviewKey,
            "reviewLink": self.reviewUrl,
            "reviewState": self.reviewState,
            "reviewers": self.reviewers,
            "files": self.files,
            "commentsCount": self.commentsCount,
            "commentsText": self.commentsText,
            "timeSpent": self.timeSpent,
        }}

    def getReviewKey(self):
        # self.logNormal(colorStr("Getting review key for CL #%s" % self.cl, "cyan"))
        print("Getting review key for CL #%s" % self.cl)

        reviewKeyPer = self.reviewKey
        reviewKeyCru = str()

        changeSetData = self.parent.cru.getReviewsForChangeset(self.REPO_NAME, self.cl)

        if "reviews" in changeSetData.keys():
            reviewKeys = [x for x in changeSetData["reviews"]]

            if(len(reviewKeys)):
                reviewKeyCru = reviewKeys[0]["permaId"]["id"]

        if len(reviewKeyPer):
            self.foundInPer = True
            self.reviewKey = reviewKeyPer

        if len(reviewKeyCru):
            self.foundInCru = True

            if not len(self.reviewKey):
                self.reviewKey = reviewKeyCru


    def getReviewFiles(self):
        print("Getting review files for CL #%s" % self.cl)
        # self.logNormal(colorStr("Getting review files for CL #%s" % self.cl, "cyan"))

        data = self.parent.cru.getChangeset(self.REPO_NAME, self.cl)
        self.files = sorted([x["path"] for x in data["fileRevisionKey"]])

    def do(self, rItem):
        commentsText = str()
        rItemId = rItem["permId"]["id"]
        rItemPath = rItem["fromPath"]
        k = rItemPath.split("/")
        if len(k):
            rItemPath = k[-1]

        commentsData = self.parent.cru.getReviewItemComments(self.reviewKey, rItemId)
        if len(commentsData["comments"]):
            commentForItem = [self._parseComment(x) for x in commentsData["comments"]]
            if len(commentForItem):
                commentsText += "In review item %s (%s):\n" % (rItemId, rItemPath)
                commentsText += "\n".join(commentForItem)
                commentsText += "\n\n"
        return commentsText

    def getReviewData(self, threads_count):
        # self.logNormal(colorStr("Getting general review data for CL #%s" % self.cl, "cyan"))
        print("Getting general review data for CL #%s" % self.cl)

        try:
            data = self.parent.cru.getDetails(self.reviewKey)
            self.reviewUrl = self.parent.url + "cru/" + self.reviewKey
            self.author = data["author"]["displayName"]

            for rev in data["reviewers"]["reviewer"]:
                status = "(  ) "
                if True == rev["completed"]:
                    status = "(v) "
                # https://adc.luxoft.com/fisheye/rest-service/reviews-v1/CR-HMCDIS2-21699/reviewers
                self.reviewers.append(status + rev["displayName"])

            self.reviewState = data["state"]

            #*************************************************************
            self.commentsCount = 0

            for comment in data["stats"]:
                self.commentsCount += int(comment["published"])

            #*************************************************************
            aggTime = 0

            # for rev in data["reviewers"]["reviewer"]:
            #     if "timeSpent" in rev.keys():
            #         aggTime += int(rev["timeSpent"])
            #
            # self.timeSpent = self._msec2format(str(aggTime))
            self.timeSpent = self.parent.cru.getTimeSpent(self.reviewKey)
            #*************************************************************
            commentsText = str()

            generalCommentsData = data["generalComments"]["comments"]

            if len(generalCommentsData):
                generalComments = [self._parseComment(x) for x in generalCommentsData]
                commentsText += "General comments:\n"
                commentsText += "\n".join(generalComments)
                commentsText += "\n\n"

            # self.logNormal(colorStr("Getting comments for CL #%s" % self.cl, "cyan"))
            print("Getting comments for CL #%s" % self.cl)

            commit_pool = ThreadPool(processes=threads_count)
            processes = list()
            for rItem in data["reviewItems"]["reviewItem"]:
                process = commit_pool.apply_async(func=self.do, args=(rItem, ))
                processes.append(process)

            for process in processes:
                commentsText += process.get()

            commit_pool.close()
            commit_pool.join()

            self.commentsText = commentsText

        except:
            self.logNormal(colorStr("Error. Details: %s" % traceErr(), "red"))
            raise



    def _parseComment(self, data, depth = 0):
        author = data["user"]["displayName"]
        message = data["message"]
        result = "[%s] %s" % (author, message)
        pre = str()

        if depth:
            pre = "\n"

            for i in range(depth):
                pre += "---"

        for replie in data["replies"]:
            result += self._parseComment(replie, depth + 1)

        return pre + result


    @staticmethod
    def _msec2format(seconds):
        result = str()

        try:
            s, ms = divmod(int(seconds), 1000)
            m, s = divmod(s, 60)

            if m != 0:
                result = "%dm %02ds" % (m, s)
            else:
                result = "%ds" % s
        except:
            pass

        return result