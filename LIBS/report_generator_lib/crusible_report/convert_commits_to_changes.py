from report_generator_lib.crusible_report.Change import Change
from report_generator_lib.crusible_report.add_involved_domains_into_the_all_changes import add_involved_domains_into_the_all_changes_by_mapping
from report_generator_lib.crusible_report.cut_comments import cut_comments
from report_generator_lib.crusible_report.change_names import change_names
from report_generator_lib.stash_report.Runner.read_PathRespMapping import read_PathRespMapping


def convert_commits_to_changes(LintResponsibles_ini, Names_ini, commits):
    changes = list()

    for commit in commits:
        commit = commit.serialize()["commit"]
        ch = Change()

        ch.change = commit["cl"]

        groups = commit["date"].split('/')
        ch.date = groups[2] + "/" + groups[1] + "/" + groups[0]

        # https://adc.luxoft.com/fisheye/rest-service/reviews-v1/CR-HMCDIS2-21699/reviewers
        ch.reviewers = list()
        finished = 0
        for reviewer in commit["reviewers"]:
            if "(v)" in reviewer or "(x)" in reviewer:
                finished += 1
            ch.reviewers.append(reviewer)

        if len(ch.reviewers) > 0:
            ch.reviewed = finished / len(ch.reviewers)
        else:
            ch.reviewed = 0
            print("!!! WARNING !!! Review has no reviewers !!!")

        ch.review = commit["reviewLink"].replace("fisheyecru", "fisheye/cru")

        reviewState = commit["reviewState"]
        """
            there are all states described here: https://confluence.atlassian.com/crucible/state-298977329.html

            Draft              See Creating a Review.
            Require Approval   Relevant only when the moderator is not the creator. See Issuing a Review.
            Under Review       See Issuing a Review and Reviewing the Code.
            Summarize          See Summarizing and Closing the Review.
            Closed             See Summarizing and Closing the Review.
            Abandoned          This happens when a review is deleted.
            Rejected           Any reviews that a moderator has rejected.
            Needs Fixing	   This means that the review state is not understood by Crucible, and indicates a
                               programming or data issue. The review moderator can move the review into a known
                               state if this happens.
        """

        if "Closed" == reviewState and 1 == ch.reviewed:
            ch.state = "Review done"
        elif reviewState is None or reviewState == "":
            ch.state = "No review found"
        else:
            ch.state = "Review in progress"

        ch.author = commit["author"]
        ch.files = commit["files"]

        ch.formats = set()
        for file in ch.files:
            index = file.rfind(".")
            extension = ""
            if index != -1:
                extension = file[index:]
            ch.formats.add(extension)

        ch.comments = commit["commentsCount"]
        ch.comments_text = commit["commentsText"]
        ch.time_spent = commit["timeSpent"]

        changes.append(ch)

    changes = add_involved_domains_into_the_all_changes_by_mapping(LintResponsibles_ini, changes)
    changes = change_names(changes, read_PathRespMapping(Names_ini, "Workers"))
    changes = cut_comments(changes)

    output = list()
    for change in changes:
        output.append(change.to_dict())

    return output
