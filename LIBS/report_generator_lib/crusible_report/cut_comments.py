import re


def cut_comments(changes, cut=50):
    for change in changes:

        # find ----[text text] text \n
        pattern = r"([-]*\[[^^]*?\][^\n]*)"
        all_lines = change.comments_text.split("\n")
        lines_to_add = list()
        for counter, line in enumerate(all_lines):
            if re.match(pattern, line):
                lines_to_add.append(line)
            elif "In review item " in line or "General comments:" in line:
                if counter == 0:
                    lines_to_add.append(line)
                else:
                    lines_to_add.append("\n" + line)
        change.comments_text = "\n".join(lines_to_add)
        change.comments_text = change.comments_text.replace("---", "      ")
        lines_to_add = change.comments_text.split('\n')

        for counter, line in enumerate(lines_to_add):
            lines_to_add[counter] = lines_to_add[counter].replace("\n", "; ")
            if len(lines_to_add[counter]) > cut:
                lines_to_add[counter] = lines_to_add[counter][0:cut-3] + "..."
        change.comments_text = "\n".join(lines_to_add)

    return changes
