import re
from datetime import datetime
from threading import Thread

from report_generator_lib.crusible_report.Commit import Commit
from report_generator_lib.crusible_report.Crucible import Crucible
from report_generator_lib.utility.FileSystem import FileSystem as FS
from report_generator_lib.utility.NiceData import NiceData as ND
from report_generator_lib.utility.htmlCtx import *


class CrucibleReport:
    def __init__(self):
        self.p4ChangeFile = str()
        self.dateFrom = str()
        self.dateTo = str()
        self.url = str()
        self.user = str()
        self.password = str()
        self.threads_count = 1
        self.log = print
        self.endCall = lambda s, e, l, f: None
        self.setProgressMax = lambda x: None
        self.setProgressValue = lambda x: None
        self.incProgress = lambda : None
        self.saveDumps = False
        #  *************************
        self.commits = list()
        self.cru = None
        #  *************************
        self.crusible_repo_name = None
        self.review_prefix = None
        self.name_of_xlsx_output_file = None
        self.path_responsibilities_ini_file = None
        self.path_to_names_map_ini = None

    def start(self):
        self.commits = self.getCommits()
        if len(self.commits) == 0:
            raise ValueError("Possible you've specified to old dates \"from-to\" or no commits in this diapason\n"
                             "or not match commits was specified from the start of this script \"count_of_commits\"")

        self.cru = Crucible(self.url, self.user, self.password)
        self.setProgressValue(0)
        self.setProgressMax(len(self.commits))

        if self.cru:

            parts = [list() for i in range(self.threads_count)]

            for i in range(len(self.commits)):
                parts[i % self.threads_count].append(self.commits[i])

            pool = [Thread(target=self._processFoo, args=(x,)) for x in parts]
            [x.start() for x in pool]

            while True in [x.is_alive() for x in pool]:
                pass

            if self.saveDumps:
                self.saveDump()

        # self.log(colorStr("PROCESS HAS BEEN FINISHED", "green"))
        print("PROCESS HAS BEEN FINISHED")
        return self.endCall(
            self,
            self.name_of_xlsx_output_file,
            self.path_responsibilities_ini_file,
            self.path_to_names_map_ini
        )

    def _processFoo(self, part):
        for commit in part:
            # self.log(colorStr("Processing CL #%s" % commit.cl, "blue"))
            print("Processing CL #%s" % commit.cl)
            commit.getReviewKey()
            commit.getReviewFiles()

            if len(commit.reviewKey):
                commit.getReviewData(self.threads_count)

            self.incProgress()

    def getCommits(self):
        commits = list()
        startDate = datetime.strptime(self.dateFrom, "%d/%m/%Y")
        endDate = datetime.strptime(self.dateTo, "%d/%m/%Y")

        with open(self.p4ChangeFile, "r") as file:
            ctx = file.read()
            commitInfo = ctx.split("/-1.1.1**")

            for info in commitInfo:
                cls = re.findall("Change (\d+) on", info)

                if len(cls):
                    dateStr = re.findall("on ([\w\/]+) by", info)

                    if len(dateStr):
                        cl = cls[0]
                        date = datetime.strptime(dateStr[0], "%Y/%m/%d")
                        reviewKey = str()

                        if date >= startDate and date <= endDate:
                            # find CR-{PROJECT} links in description of ChangeList, If not: that means
                            # CL not reviewed (this is one way o define is CL reviewed or not)
                            reviewKeys = re.findall("({}-[\w]+)".format(self.review_prefix), info)

                            if len(reviewKeys):
                                reviewKey = reviewKeys[0]

                            pattern = re.compile(r" by ([\w]*?)@")
                            author_name = pattern.findall(info)[0]

                            commits.append(Commit(cl, dateStr[0], reviewKey, self, self.crusible_repo_name, author_name))
        return commits

    def saveDump(self):
        data = {"root": [f.serialize() for f in self.commits]}
        FS.checkCreateDir("data")
        ND.dict2json(data, "data/dump_crucible_report.json")
