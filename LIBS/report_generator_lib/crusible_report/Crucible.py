from urllib import request
from urllib import parse
import json, re


class Crucible():
    def __init__(self, url, user, pw):
        self.baseUrl = url

        self.baseUrl = self.baseUrl.strip("/")
        self.user = user
        self.pw = pw
        self.httpParams = { 'Content-Type': 'application/json', 'Accept': 'application/json' }
        self.token = None
        self.login()

    def _createRequest(self, url, data = None):
        return request.Request(url, data, self.httpParams)

    def _urlOpen(self, url, data = None):
        print("     ", url[23:])
        return request.urlopen(self._createRequest(url, data)).read().decode(errors = "ignore")

    def _addParams(self, url, params = list()):
        if len(params):
            url += '?'
            first = True

            for key, value in params:
                if first:
                    first = False
                else:
                    url += '&'

                url += parse.quote(key) + '=' + parse.quote(str(value))

        return url

    def _defaultParams(self):
        params = list()
        if self.token: params.append(('FEAUTH', self.token))
        return params

    def login(self):
        result = False

        url = '%s/rest-service/auth-v1/login?userName=%s&password=%s' % (self.baseUrl, parse.quote(self.user), parse.quote(self.pw))
        res = json.loads(self._urlOpen(url))

        if "token" in res.keys():
            self.token = res["token"]
            result = True

        return result

    def getDetails(self, reviewId):
        url = "%s/rest-service/reviews-v1/%s/details" % (self.baseUrl, parse.quote(reviewId))
        #print(url)
        url = self._addParams(url, self._defaultParams())

        return json.loads(self._urlOpen(url))

    def getTimeSpent(self, reviewId):
        result = "?"

        url = "%s/cru/%s" % (self.baseUrl, parse.quote(reviewId))
        url = self._addParams(url, self._defaultParams())
        #print(url)

        data = self._urlOpen(url)

        pattern = """<tr id="details-participants-total">
            <td>Total</td>
            <td>&nbsp;</td>
            <td class="timeSpent">([\w\s]+)</td>"""

        fr = re.findall(pattern, data)

        if len(fr):
            result = fr[0]

        return result

    def getReviewItemComments(self, reviewId, reviewItemId):
        url = "%s/rest-service/reviews-v1/%s/reviewitems/%s/comments" % (self.baseUrl, parse.quote(reviewId), parse.quote(reviewItemId))
        url = self._addParams(url, self._defaultParams())

        return json.loads(self._urlOpen(url))

    def getChangeset(self, repoName, changeset):
        url = "%s/rest-service-fe/revisionData-v1/changeset/%s/%s" % (self.baseUrl, parse.quote(repoName), parse.quote(changeset))
        url = self._addParams(url, self._defaultParams())

        return json.loads(self._urlOpen(url))

    def getReviewsForChangeset(self, repoName, cl):
        url = "%s/rest-service-fe/search-v1/reviewsForChangeset/%s" % (self.baseUrl, parse.quote(repoName))
        url = self._addParams(url, self._defaultParams())

        headers = {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                  }
        data = ("cs=%s" % cl).encode()

        req = request.Request(url, data, headers)

        try:
            resp = request.urlopen(req).read()
        except:
            print("---------------------------")
            print("---------------------------")
            print(url)
            print("---------------------------")
            print("---------------------------")

        return json.loads(resp.decode(errors = "ignore"))
