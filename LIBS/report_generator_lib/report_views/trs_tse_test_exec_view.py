from report_generator_lib.report_views.xlsx_utils import *
import xlsxwriter


def testExecView(features, path):
    mtx = [["Feature id",
            "bin files (name)",
            "bin files (path)",
            "bin files (exec result)"]]

    for f in features:
        mtx.append([
            f.id,
            "\n".join([x["name"] for x in f.binFiles]),
            "\n".join([x["path"] for x in f.binFiles]),
            "\n".join([x["execResult"] for x in f.binFiles])
        ])

    xlsx = xlsxwriter.Workbook(path)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)
    formatGreen = xlsx.add_format(XlsxCellStyle.green)
    formatRed = xlsx.add_format(XlsxCellStyle.red)
    formatHeader = xlsx.add_format(XlsxCellStyle.header)

    generalTab = xlsx.add_worksheet()
    generalTab.autofilter("A1:D1")
    generalTab.set_column("A:A", 30)
    generalTab.set_column("B:B", 50)
    generalTab.set_column("C:C", 110)
    generalTab.set_column("D:D", 60)


    for i in range(len(mtx)):
        for j in range(len(mtx[i])):
            if i == 0:
                generalTab.write(i, j, mtx[i][j], formatHeader)

            elif "SUCCESS" in mtx[i][j]:
                generalTab.write(i, j, mtx[i][j], formatGreen)

            elif "FAILURE" in mtx[i][j]:
                generalTab.write(i, j, mtx[i][j], formatRed)

            else:
                generalTab.write(i, j, mtx[i][j], formatNormal)

    generalTab.freeze_panes(1, 0)
    xlsx.close()