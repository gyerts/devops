from report_generator_lib.report_views.xlsx_utils import *
from report_generator_lib.stash_report.Runner.read_PathRespMapping import *

import xlsxwriter
from datetime import datetime

height = dict()
line_height = 11
UNKNOWN_DOMAIN = "Unknown domain"


def get_content_height(content):
    if isinstance(content, str):
        length = content.count("\n", 0) + 1
    else:
        length = len(content)

    return line_height * length


def get_height(line, content):
    if line in height:
        content_height = get_content_height(content)
        if content_height > height[line]:
            height[line] = content_height
    else:
        height[line] = line_height * len(content)
    return height[line]


def general(xlsx, json_object, formatHeader, formatNormal, formatGreen, formatRed, formatYellow, percentageRed, percentageGreen, percentageYellow):
    # step 1: init work sheet
    generalTab = xlsx.add_worksheet("General")
    generalTab.freeze_panes(1, 1)

    row = 0
    col = 0

    # step 2: headers
    header = ["Change list", "Date", "Review", "Reviewed", "Author", "Domain", "State",
              "Reviewers", "Files", "Formats", "Comments", "Comments text", "Time spent"]

    for i in range(len(header)):
        generalTab.write(row, col + i, header[i], formatHeader)

    row += 1

    # step 3: main data
    for line in json_object:
        generalTab.write(row, col + 0, line["change"], formatNormal)

        try:
            d = datetime.strptime(line["date"], "%a %b %d %X %Y")
            date_out = d.strftime("%d/%m/%Y")
        except:
            date_out = line["date"]

        generalTab.write(row, col + 1, date_out, formatNormal)

        generalTab.write(row, col + 2, line["review"], formatNormal)

        if line["reviewed"] == 1:
            generalTab.write(row, col + 3, line["reviewed"], percentageGreen)
        elif line["reviewed"] == 0:
            generalTab.write(row, col + 3, line["reviewed"], percentageRed)
        else:
            generalTab.write(row, col + 3, line["reviewed"], percentageYellow)

        generalTab.write(row, col + 4, line["author"], formatNormal)

        generalTab.write(row, col + 5,   ",\n".join(line["domain"]), formatNormal)
        generalTab.set_row(row, get_height(row, line["domain"]))

        if "Review done" in line["state"]:
            generalTab.write(row, col + 6, line["state"], formatGreen)
        elif "Review in progress" in line["state"]:
            generalTab.write(row, col + 6, line["state"], formatYellow)
        else:
            generalTab.write(row, col + 6, line["state"], formatRed)

        generalTab.write(row, col + 7,   ",\n".join(line["reviewers"]), formatNormal)
        generalTab.set_row(row, get_height(row, line["reviewers"]))

        generalTab.write(row, col + 8,   ",\n".join(line["files"]), formatNormal)
        generalTab.set_row(row, get_height(row, line["files"]))

        generalTab.write(row, col + 9,   ",\n".join(line["formats"]), formatNormal)
        generalTab.set_row(row, get_height(row, line["formats"]))

        generalTab.write(row, col + 10,  line["comments"], formatNormal)

        generalTab.write(row, col + 11,  line["comments_text"], formatNormal)
        generalTab.set_row(row, get_height(row, line["comments_text"]))

        try:
            generalTab.write(row, col + 12,  line["time_spent"], formatNormal)
        except:
            generalTab.write(row, col + 12,  "N/A", formatNormal)

        row += 1

    generalTab.autofilter(0, 0, row, 12)

    generalTab.set_column(0, 0, 36)    # Change list
    generalTab.set_column(1, 1, 18)    # Date
    generalTab.set_column(2, 2, 10)    # Review
    generalTab.set_column(3, 3, 10)    # Reviewed
    generalTab.set_column(4, 4, 10)    # Author
    generalTab.set_column(5, 5, 15)    # Domain
    generalTab.set_column(6, 6, 20)    # State
    generalTab.set_column(7, 7, 25)    # Reviewers
    generalTab.set_column(8, 8, 80)    # Files
    generalTab.set_column(9, 9, 8)     # Formats
    generalTab.set_column(10, 10, 3)   # Comments
    generalTab.set_column(11, 11, 40)  # Comments text
    generalTab.set_column(12, 12, 10)  # Time spent


def statistics(xlsx, json_object, domains, formatHeader, formatNormal, percentage):
    #  Init Worksheet ---------------------- >
    statisticTab = xlsx.add_worksheet("Statistics")
    statisticTab.freeze_panes(1, 1)
    #  < ---------------------- Init Worksheet

    row = 0
    col = 0

    #  Write HEADER ---------------------- >

    header = [
                "Domain", "All commits", "Direct commits", "Reviewed commits", "Not reviewed commits",
                "Review coverage percentage", "Reviews with comments", "Reviews with comments percentage",
                "Reviews with ts <= 3m", "Reviews with ts <= 3m percentage"
             ]
    for i in range(len(header)):
        statisticTab.write(row, col + i, header[i], formatHeader)
    row += 1
    #  < ---------------------- Write HEADER


    start_row = row

    #  Write Domains ---------------------- >
    row = start_row
    for domain in domains:
        statisticTab.write(
            row, col + 0,
            domain,
            formatNormal
        )
        row += 1
    #  < ---------------------- Write Domains

    #  Write All commits ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        statisticTab.write_formula(
            row, col + 1,
            "=COUNTIF(General!F:F, \"*\"&A{}&\"*\")".format(counter),
            formatNormal
        )
        counter += 1
        row += 1
    #  < ---------------------- Write All commits

    #  Direct commits ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        # =COUNTIFS(General!F:F, "*"&A2&"*", General!D:D,  1)
        statisticTab.write_formula(
            row, col + 2,
            "=COUNTIFS(General!F:F, \"*\"&A{}&\"*\", General!G:G,  \"No review found\")".format(counter),
            formatNormal
        )

        counter += 1
        row += 1
    #  < ---------------------- Direct commits

    #  Write Reviewed commits ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        statisticTab.write_formula(
            row, col + 3,
            "=COUNTIFS(General!F:F, \"*\"&A{}&\"*\", General!D:D,  1)".format(counter),
            formatNormal
        )
        counter += 1
        row += 1
    #  < ---------------------- Write Reviewed commits

    #  Write Not reviewed commits ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        statisticTab.write_formula(
            row, col + 4,
            "=B{}-D{}".format(counter, counter),
            formatNormal
        )
        counter += 1
        row += 1
    #  < ---------------------- Write Not reviewed commits

    #  Write Review coverage percentage ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        statisticTab.write_formula(
            row, col + 5,
            "=IFERROR(D{}/B{}, 0)".format(counter, counter),
            percentage
        )
        counter += 1
        row += 1
    #  < ---------------------- Write Review coverage percentage

    #  Write Reviews with comments ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        statisticTab.write_formula(
            row, col + 6,
            "=COUNTIFS(General!F:F, A{}, General!L:L,  \"*\")".format(counter),
            formatNormal
        )
        counter += 1
        row += 1
    #  < ---------------------- Write Reviews with comments

    #  Write Reviews with comments percentage ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        statisticTab.write_formula(
            row, col + 7,
            "=IFERROR(G{}/B{}, 0)".format(counter, counter),
            percentage
        )
        counter += 1
        row += 1
    #  < ---------------------- Write Reviews with comments percentage

    #  Reviews with ts <= 3m ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        formula  = "=COUNTIFS(General!F:F, \"*\"&A{}&\"*\", General!M:M,  \"3m\") + ".format(counter)
        formula += "COUNTIFS(General!F:F, \"*\"&A{}&\"*\", General!M:M,  \"2m\") + ".format(counter)
        formula += "COUNTIFS(General!F:F, \"*\"&A{}&\"*\", General!M:M,  \"1m\")".format(counter)

        statisticTab.write(row, col + 8, formula, formatNormal)
        counter += 1
        row += 1
    #  < ---------------------- Reviews with ts <= 3m

    #  Reviews with ts <= 3m percentage ---------------------- >
    row = start_row
    counter = 2  # =COUNTIF(General!G:G, A2)       A2 - start of counting
    for domain in domains:
        statisticTab.write(
            row, col + 9,
            "=IFERROR(I{}/B{}, 0)".format(counter, counter),
            percentage
        )

        counter += 1
        row += 1
    #  < ---------------------- Reviews with ts <= 3m percentage


    #  Set Autofilter ---------------------- >
    statisticTab.autofilter(0, 0, row - 1, 9)
    # < ---------------------- Set Autofilter

    #  Width of columns ---------------------- >
    statisticTab.set_column(0, 0, 15)    # Domain
    statisticTab.set_column(1, 1, 15)    # All commits
    statisticTab.set_column(2, 2, 15)    # Direct commits
    statisticTab.set_column(3, 3, 15)    # Reviewed commits
    statisticTab.set_column(4, 4, 15)    # Not reviewed commits
    statisticTab.set_column(5, 5, 15)    # Review coverage percentage
    statisticTab.set_column(6, 6, 15)    # Reviews with comments
    statisticTab.set_column(7, 7, 15)    # Reviews with comments percentage
    statisticTab.set_column(8, 8, 15)    # Reviews with ts <= 3m
    statisticTab.set_column(9, 9, 15)    # Reviews with ts <= 3m percentage
    #  < ---------------------- Width of columns


def sca_report_view(json_object, path_of_output, responsibilities_ini):
    xlsx = xlsxwriter.Workbook(path_of_output)

    formatHeader = xlsx.add_format(XlsxCellStyle.header)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)
    formatGreen = xlsx.add_format(XlsxCellStyle.green)
    formatYellow = xlsx.add_format(XlsxCellStyle.yellow)
    formatRed = xlsx.add_format(XlsxCellStyle.red)
    percentage = xlsx.add_format(XlsxCellStyle.percentage)
    percentageRed = xlsx.add_format(XlsxCellStyle.percentageRed)
    percentageGreen = xlsx.add_format(XlsxCellStyle.percentageGreen)
    percentageYellow = xlsx.add_format(XlsxCellStyle.percentageYellow)

    settings = read_PathRespMapping(responsibilities_ini, "PathRespMapping")
    domains = set(settings.values())
    domains.add(UNKNOWN_DOMAIN)
    domains = list(domains)

    while True:
        try:
            general(xlsx, json_object, formatHeader, formatNormal, formatGreen, formatRed, formatYellow, percentageRed, percentageGreen, percentageYellow)
            statistics(xlsx, json_object, domains, formatHeader, formatNormal, percentage)

            xlsx.close()
            break

        except Exception as ex:
            if "[Errno 13] Permission denied" in str(ex):
                input("First close %s than press Enter to try again..." % path_of_output)
            else:
                raise ex
