from report_generator_lib.report_views.xlsx_utils import *
import xlsxwriter


def loc_report_view(nonGenFilesArr, genFilesArr, otherFilesArr, path):
    xlsx = xlsxwriter.Workbook(path)
    _standart_loc_structure(xlsx, "Non generated files", nonGenFilesArr)
    _standart_loc_structure(xlsx, "Generated files", genFilesArr)
    _standart_loc_structure(xlsx, "Other files", otherFilesArr)

    xlsx.close()


def _standart_loc_structure(xlsxObj, sheetName, data):
    formatHeader = xlsxObj.add_format(XlsxCellStyle.header)
    formatNormal = xlsxObj.add_format(XlsxCellStyle.normal)
    formatFooter = xlsxObj.add_format(XlsxCellStyle.footer)


    # step 1: init work sheet
    tab = xlsxObj.add_worksheet(sheetName)
    row = 0
    col = 0

    # step 2: headers
    for i in range(len(data[0])):
        tab.write(row, col + i, data[0][i], formatHeader)

    row += 1

    # step 3: main data
    for Path, LoC, ELoC, SLoC, PSLoC, BSLoC, CLoC, FCLoC, ICLoC, CCLoC, DCLoC,\
        XLoC, SXLoC, PXLoC, DXLoC, RDLoC, emptyCol, files, numC, numH, numCpp, numHpp in data[1:len(data)]:
        tab.write(row, col, Path, formatNormal)
        tab.write_number(row, col + 1, int(LoC), formatNormal)
        tab.write_number(row, col + 2, int(ELoC), formatNormal)
        tab.write_number(row, col + 3, int(SLoC), formatNormal)
        tab.write_number(row, col + 4, int(PSLoC), formatNormal)
        tab.write_number(row, col + 5, int(BSLoC), formatNormal)
        tab.write_number(row, col + 6, int(CLoC), formatNormal)
        tab.write_number(row, col + 7, int(FCLoC), formatNormal)
        tab.write_number(row, col + 8, int(ICLoC), formatNormal)
        tab.write_number(row, col + 9, int(CCLoC), formatNormal)
        tab.write_number(row, col + 10, int(DCLoC), formatNormal)
        tab.write_number(row, col + 11, int(XLoC), formatNormal)
        tab.write_number(row, col + 12, int(SXLoC), formatNormal)
        tab.write_number(row, col + 13, int(PXLoC), formatNormal)
        tab.write_number(row, col + 14, int(DXLoC), formatNormal)
        tab.write_number(row, col + 15, int(RDLoC), formatNormal)
        tab.write_blank(row, col + 16, emptyCol, formatNormal)
        tab.write_number(row, col + 17, int(files), formatNormal)
        tab.write_number(row, col + 18, int(numC), formatNormal)
        tab.write_number(row, col + 19, int(numH), formatNormal)
        tab.write_number(row, col + 20, int(numCpp), formatNormal)
        tab.write_number(row, col + 21, int(numHpp), formatNormal)

        row += 1

    #******************************************************************
    tab.write(0, 16, "Total LoC", formatFooter)

    for y in range(1, row):
        formula = "=SUM(%s:%s)" % (i2A1(y, 1), i2A1(y, 15))
        tab.write(y, col + 16, formula, formatFooter)

    #******************************************************************
    # tab.write(0, 22, "Total files", formatFooter)
    #
    # for y in range(1, row):
    #     formula = "=SUM(%s:%s)" % (i2A1(y, 17), i2A1(y, 21))
    #     tab.write(y, col + 22, formula, formatFooter)

    #******************************************************************
    tab.write(row, col, "Total", formatFooter)

    for x in range(1, 22):
        formula = "=SUM(%s:%s)" % (i2A1(1, x), i2A1(row - 1, x))
        tab.write(row, x, formula, formatFooter)

    # step 4: prettifying
    tab.set_column("A:A", 25)
    tab.set_column("B:V", 7)


