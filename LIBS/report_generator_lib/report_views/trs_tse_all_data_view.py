from report_generator_lib.report_views.xlsx_utils import *
import xlsxwriter


def allDataView(features, path):
    mtx = [["Feature id",
            "hbts files (path)",
            "hbts files (name)",
            "hbcm files (path)",
            "hbcm files (name)",
            "hbac files (path)",
            "hbac files (name)",
            "bin files (path)",
            "bin files (name)",
            "bin files (exec result)"]]

    for f in features:
        mtx.append([
            f.id,
            "\n".join([x["path"] for x in f.hbtsFiles]),
            "\n".join([x["name"] for x in f.hbtsFiles]),
            "\n".join([x["path"] for x in f.hbcmFiles]),
            "\n".join([x["name"] for x in f.hbcmFiles]),
            "\n".join([x["path"] for x in f.hbacFiles]),
            "\n".join([x["name"] for x in f.hbacFiles]),
            "\n".join([x["path"] for x in f.binFiles]),
            "\n".join([x["name"] for x in f.binFiles]),
            "\n".join([x["execResult"] for x in f.binFiles])
        ])

    xlsx = xlsxwriter.Workbook(path)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)
    formatHeader = xlsx.add_format(XlsxCellStyle.header)

    generalTab = xlsx.add_worksheet()
    generalTab.autofilter("A1:J1")


    for i in range(len(mtx)):
        for j in range(len(mtx[i])):
            if i == 0:
                generalTab.write(i, j, mtx[i][j], formatHeader)

            else:
                generalTab.write(i, j, mtx[i][j], formatNormal)

    generalTab.freeze_panes(1, 0)
    xlsx.close()