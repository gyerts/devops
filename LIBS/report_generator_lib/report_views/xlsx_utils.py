class XlsxCellStyle:
    normal = {'valign': 'top', 'text_wrap': True, 'border': True, 'font_size': 9}
    header = {'bold': True, 'text_wrap': True, 'align':'center', 'valign': 'vcenter', 'font_size': 9, 'border': True, 'bg_color': '#C0C0C0'}
    footer = {'bold': True, 'font_size': 9, 'border': True, 'bg_color': '#CC99FF'}
    red = {'valign': 'top', 'text_wrap': True, 'border': True, 'font_size': 9, 'bg_color': '#FF8080'}
    green = {'valign': 'top', 'text_wrap': True, 'border': True, 'font_size': 9, 'bg_color': '#99CC00'}
    yellow = {'valign': 'top', 'text_wrap': True, 'border': True, 'font_size': 9, 'bg_color': '#FFFF00'}
    percentage = {'valign': 'top', 'border': True, 'font_size': 9, 'num_format': '0.00%'}
    percentageGreen = {'valign': 'top', 'border': True, 'font_size': 9, 'num_format': '0.00%', 'bg_color': '#99CC00'}
    percentageYellow = {'valign': 'top', 'border': True, 'font_size': 9, 'num_format': '0.00%', 'bg_color': '#FFFF00'}
    percentageRed = {'valign': 'top', 'border': True, 'font_size': 9, 'num_format': '0.00%', 'bg_color': '#FF8080'}


    footerPercentage = {'bold': True, 'border': True, 'font_size': 9, 'bg_color': '#CC99FF', 'num_format': '0.00%'}
    link = {'valign': 'top', 'text_wrap': True, 'border': True, 'font_color': 'blue', 'underline':  1, 'font_size': 9}


def i2A1(row, col):
    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    return '%s%s' % (letters[col], str(row + 1))
