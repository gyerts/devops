from report_generator_lib.report_views.xlsx_utils import *
from collections import OrderedDict
import xlsxwriter
import xml.etree.ElementTree as ET


def defineTestsResut(testsOutput):
    result = str()

    if "FAILURE" in testsOutput:
        result = "FAILURE"
    elif  "SUCCESS" in testsOutput:
        result = "SUCCESS"
    else:
        result = "N/A"

    return result


def parseDetails(details):
    result = list()
    if len(details):
        try:
            root = ET.fromstring(details)
            tcs = root.find("TestCases")

            if tcs is not None:
                result = [parseTestCase(tc) for tc in tcs]

        except:
            pass

    return result


def parseTestCase(tc):
    name = tc.find("Name").text
    run = tc.find(".//Run")
    result = str()
    start = str()
    end = str()

    if run is not None:
        result = run.get("Result")
        start = run.find("Start").text
        end = run.find("End").text

    return {"name": name, "result": result, "start": start, "end": end}



def uniReportView(features, path):
    mtx = []

    for f in features:
        mtx.append([
            f.id,
            "\n".join([x["name"] for x in f.hbtsFiles]),
            "\n".join([x["name"] for x in f.hbcmFiles]),
            "\n".join([x["name"] for x in f.binFiles]),
            "\n".join([str(bool(len(x["path"]))) for x in f.binFiles]),
            defineTestsResut("\n".join([x["execResult"] for x in f.binFiles]))
            #"\n".join([x["execResult"] for x in f.binFiles]),
            #"\n".join([x["details"] for x in f.binFiles]),
        ])


    xlsx = xlsxwriter.Workbook(path)

    formatGreen = xlsx.add_format(XlsxCellStyle.green)
    formatRed = xlsx.add_format(XlsxCellStyle.red)
    formatYellow = xlsx.add_format(XlsxCellStyle.yellow)
    formatHeader = xlsx.add_format(XlsxCellStyle.header)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)
    formatPercentage = xlsx.add_format(XlsxCellStyle.percentage)
    formatFooter = xlsx.add_format(XlsxCellStyle.footer)
    formatFooterPercentage = xlsx.add_format(XlsxCellStyle.footerPercentage)


    # step 1: init work sheet
    generalTab = xlsx.add_worksheet("General")
    row = 0
    col = 0

    # step 2: headers
    generalTab.write(row, col, "Feature id", formatHeader)
    generalTab.write(row, col + 1, "hbts files", formatHeader)
    generalTab.write(row, col + 2, "hbcm files", formatHeader)
    generalTab.write(row, col + 3, "bin files", formatHeader)
    generalTab.write(row, col + 4, "Is compiled", formatHeader)
    generalTab.write(row, col + 5, "Exec result", formatHeader)

    row += 1

    # step 3: main data
    for featureId, hbtsFiles, hbcmFiles, binFiles, isCompiled, execResult in mtx:
        generalTab.write(row, col, featureId, formatNormal)
        generalTab.write(row, col + 1, hbtsFiles, formatNormal)
        generalTab.write(row, col + 2, hbcmFiles, formatNormal)

        #formula = '=HYPERLINK("#\'Execution\'!" & ADDRESS(MATCH("%s", Execution!A:A), 1), "%s")' % ()
        #generalTab._write_hyperlink_internal(row, col + 3, )
        generalTab.write(row, col + 3, binFiles, formatNormal)
        generalTab.write(row, col + 4, isCompiled, formatNormal)
        generalTab.write(row, col + 5, execResult, formatNormal)

        row += 1

    #step 4: prettifying
    generalTab.autofilter("A1:G1")
    generalTab.set_column("A:G", 40)
    generalTab.freeze_panes(1, 0)

    #************************************************************************************
    divDomain = OrderedDict()

    for f in features:
        dom = (f.id.split("-"))[2]

        if dom not in divDomain.keys():
            divDomain[dom] = list()

        divDomain[dom].append(f)


    for dom in divDomain.keys():
        print(dom)

        uniqueBins = list()


        for feature in divDomain[dom]:
            for bin in feature.binFiles:
                if not bin["name"] in [x["name"] for x in uniqueBins]:
                    uniqueBins.append({"name": bin["name"], "execResult": bin["execResult"], "details": bin["details"]})

        # step 1: init work sheet
        execTab = xlsx.add_worksheet(dom)
        row = 0
        col = 0

        # step 2: headers
        execTab.write(row, col, "TSE binary name", formatHeader)
        execTab.write(row, col + 1, "General run result", formatHeader)
        execTab.write(row, col + 2, "Test cases", formatHeader)
        execTab.write(row, col + 3, "Start", formatHeader)
        execTab.write(row, col + 4, "End", formatHeader)
        execTab.write(row, col + 5, "Run result", formatHeader)

        row += 1

        # step 3: main data
        for bin in uniqueBins:
            execTab.write(row, col, bin["name"], formatNormal)
            execTab.write(row, col + 1, bin["execResult"], formatNormal)

            tcData = parseDetails(bin["details"])
            start_row = row

            if len(tcData):
                for tc in tcData:
                    execTab.write(row, col + 2, tc["name"], formatNormal)
                    execTab.write(row, col + 3, tc["start"], formatNormal)
                    execTab.write(row, col + 4, tc["end"], formatNormal)
                    execTab.write(row, col + 5, tc["result"], formatNormal)
                    row += 1

                execTab.merge_range(start_row, col, row - 1, col, bin["name"], formatNormal)
                execTab.merge_range(start_row, col + 1, row - 1, col + 1, bin["execResult"], formatNormal)

            else:
                execTab.write(row, col + 2, str(), formatNormal)
                execTab.write(row, col + 3, str(), formatNormal)
                execTab.write(row, col + 4, str(), formatNormal)
                execTab.write(row, col + 5, str(), formatNormal)
                row += 1

            #row += 1

        #step 4: prettifying
        execTab.autofilter("A1:F1")
        execTab.set_column("A:C", 40)
        execTab.set_column("D:F", 15)
        execTab.freeze_panes(1, 0)

    #*******************************************************************************************************************
    # step 1: init work sheet
    staticsTab = xlsx.add_worksheet("Statictics")
    row = 0
    col = 0

    # step 2: headers
    staticsTab.write(row, col, "Domain", formatHeader)
    staticsTab.write(row, col + 1, "Overall features", formatHeader)
    staticsTab.write(row, col + 2, "Covered by TSE", formatHeader)
    staticsTab.write(row, col + 3, "Covered by TSE, %", formatHeader)
    staticsTab.write(row, col + 4, "Covered by component", formatHeader)
    staticsTab.write(row, col + 5, "Covered by component, %", formatHeader)
    staticsTab.write(row, col + 6, "Expected TSE binaries", formatHeader)
    staticsTab.write(row, col + 7, "Available TSE binaries", formatHeader)
    staticsTab.write(row, col + 8, "Available TSE binaries, %", formatHeader)
    staticsTab.write(row, col + 9, "Available test cases", formatHeader)
    staticsTab.write(row, col + 10, "Successful test cases", formatHeader)
    staticsTab.write(row, col + 11, "Failed test cases", formatHeader)
    staticsTab.write(row, col + 12, "Successful test cases, %", formatHeader)

    row += 1

    domains = ["Audio", "Data", "Diagnosis", "Driver", "HMI", "Internet_Application", "OnOff",
               "Online_Services", "SWDL_CORE", "Systems", "VOICE", "Voice_Recognition"]

    for dom in domains:
        staticsTab.write(row, col, '%s' % dom, formatNormal)
        staticsTab.write(row, col + 1, '=COUNTIF(General!A:A,"*%s-*")' % dom, formatNormal)
        staticsTab.write(row, col + 2, '=COUNTIFS(General!A:A, "*%s-*", General!B:B, "*")' % dom, formatNormal)
        staticsTab.write(row, col + 3, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 2), i2A1(row, 1)), formatPercentage)
        staticsTab.write(row, col + 4, '=COUNTIFS(General!A:A, "*%s-*", General!C:C, "*")' % dom, formatNormal)
        staticsTab.write(row, col + 5, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 4), i2A1(row, 1)), formatPercentage)
        staticsTab.write(row, col + 6, '=COUNTIF(%s!A:A, "*") - 1' % dom, formatNormal)
        staticsTab.write(row, col + 7, '=COUNTIFS(%s!A:A, "*", %s!B:B, "*") - 1' % (dom, dom), formatNormal)
        staticsTab.write(row, col + 8, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 7), i2A1(row, 6)), formatPercentage)
        staticsTab.write(row, col + 9, '=COUNTIF(%s!C:C, "*") - 1' % dom, formatNormal)
        staticsTab.write(row, col + 10, '=COUNTIF(%s!F:F, "Success")' % dom, formatNormal)
        staticsTab.write(row, col + 11, '=COUNTIF(%s!F:F, "Fail")' % dom, formatNormal)
        staticsTab.write(row, col + 12, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 10), i2A1(row, 9)), formatPercentage)

        row += 1

    # footer
    staticsTab.write(row, col, "Overall", formatFooter)
    staticsTab.write(row, col + 1, '=SUM(%s:%s)' % (i2A1(1, 1), i2A1(row - 1, 1)), formatFooter)
    staticsTab.write(row, col + 2, '=SUM(%s:%s)' % (i2A1(1, 2), i2A1(row - 1, 2)), formatFooter)
    staticsTab.write(row, col + 3, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 2), i2A1(row, 1)), formatFooterPercentage)
    staticsTab.write(row, col + 4, '=SUM(%s:%s)' % (i2A1(1, 4), i2A1(row - 1, 4)), formatFooter)
    staticsTab.write(row, col + 5, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 4), i2A1(row, 1)), formatFooterPercentage)
    staticsTab.write(row, col + 6, '=SUM(%s:%s)' % (i2A1(1, 6), i2A1(row - 1, 6)), formatFooter)
    staticsTab.write(row, col + 7, '=SUM(%s:%s)' % (i2A1(1, 7), i2A1(row - 1, 7)), formatFooter)
    staticsTab.write(row, col + 8, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 7), i2A1(row, 6)), formatFooterPercentage)
    staticsTab.write(row, col + 9, '=SUM(%s:%s)' % (i2A1(1, 9), i2A1(row - 1, 9)), formatFooter)
    staticsTab.write(row, col + 10, '=SUM(%s:%s)' % (i2A1(1, 10), i2A1(row - 1, 10)), formatFooter)
    staticsTab.write(row, col + 11, '=SUM(%s:%s)' % (i2A1(1, 11), i2A1(row - 1, 11)), formatFooter)
    staticsTab.write(row, col + 12, '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 10), i2A1(row, 9)), formatFooterPercentage)



    #step 4: prettifying
    staticsTab.autofilter("A1:F1")
    staticsTab.set_column("A:A", 25)

    xlsx.close()
