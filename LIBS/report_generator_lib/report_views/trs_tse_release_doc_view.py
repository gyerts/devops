from report_generator_lib.report_views.xlsx_utils import *
import xlsxwriter
from collections import OrderedDict


def releaseDocView(features, path):
    xlsx = xlsxwriter.Workbook(path)
    formatGreen = xlsx.add_format(XlsxCellStyle.green)
    formatRed = xlsx.add_format(XlsxCellStyle.red)
    formatHeader = xlsx.add_format(XlsxCellStyle.header)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)

    # all tab
    allTabRaw = [[
        "Feature id",
        "hbts files (name)",
        "hbcm files (name)",
        "bin files (name)",
        "bin files (is compiled)",
        "bin files (exec result)"
    ]]

    for f in features:
        allTabRaw.append([
            f.id,
            "\n".join([x["name"] for x in f.hbtsFiles]),
            "\n".join([x["name"] for x in f.hbcmFiles]),
            "\n".join([x["name"] for x in f.binFiles]),
            "\n".join([str(bool(len(x["path"]))) for x in f.binFiles]),
            "\n".join([x["execResult"] for x in f.binFiles])
        ])

    file = xlsx.add_worksheet("All")

    for i in range(len(allTabRaw)):
        for j in range(len(allTabRaw[i])):
            if i == 0:
                file.write(i, j, allTabRaw[i][j], formatHeader)


            elif i != 0 and j == (len(allTabRaw[i]) - 1):
                if "SUCCESS" in allTabRaw[i][j]:
                    file.write(i, j, allTabRaw[i][j], formatGreen)

                elif "FAILURE" in allTabRaw[i][j]:
                    file.write(i, j, allTabRaw[i][j], formatRed)

                else:
                    file.write(i, j, allTabRaw[i][j], formatNormal)

            else:
                file.write(i, j, allTabRaw[i][j], formatNormal)

    # statistics

    domains = OrderedDict()


    statisticsTabRaw = [[
        "Domain",
        "Overall features",
        "Covered by TSE",
        "Covered by component",
        "Overall TSE test",
        "Successful TSE",
        "Failed TSE"
    ]]

    file = xlsx.add_worksheet("Statistics")


    for f in features:
        domain = (f.id.split("-"))[2]

        if not domain in domains.keys():
            domains[domain] = list()

        domains[domain].append(f)


    for domain in domains.keys():
        domainName = domain
        domainItems = domains[domain]

        overallFeatures = 0
        hbtsCovered = 0
        hbcmCovered = 0

        allBinNames = list()
        allBinExecResults = list()

        for item in domainItems:
            overallFeatures += 1

            for hbtsFile in item.hbtsFiles:
                if len(hbtsFile["path"]): hbtsCovered += 1
                break

            for hbcmFile in item.hbcmFiles:
                if len(hbcmFile["path"]): hbcmCovered += 1
                break

            for bin in item.binFiles:
                allBinNames.append(bin["name"])
                allBinExecResults.append(bin["execResult"])

        overallBins = len(set(allBinNames))
        successfulBins = len([x for x in set(allBinExecResults) if "SUCCESS" in x])
        failedBins = len([x for x in set(allBinExecResults) if "FAILURE" in x])

        statisticsTabRaw.append([domainName, overallFeatures, hbtsCovered, hbcmCovered, overallBins, successfulBins, failedBins])


    for i in range(len(statisticsTabRaw)):
        for j in range(len(statisticsTabRaw[i])):
            if i == 0:
                file.write(i, j, statisticsTabRaw[i][j], formatHeader)
            else:
                file.write(i, j, statisticsTabRaw[i][j], formatNormal)

    xlsx.close()