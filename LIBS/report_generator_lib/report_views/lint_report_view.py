from report_generator_lib.report_views.xlsx_utils import *
import xlsxwriter


def lint_report_view(arr2d, path):

    xlsx = xlsxwriter.Workbook(path)

    formatHeader = xlsx.add_format(XlsxCellStyle.header)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)
    formatFooter = xlsx.add_format(XlsxCellStyle.footer)

    # step 1: init work sheet
    generalTab = xlsx.add_worksheet("General")
    row = 0
    col = 0

    # step 2: headers
    for i in range(len(arr2d[0])):
        generalTab.write(row, col + i, arr2d[0][i], formatHeader)

    row += 1


    # step 3: main data
    for path, pr1, pr2, pr3, pr4, pr5, pr6 in arr2d[1:len(arr2d)]:
        generalTab.write(row, col, path, formatNormal)
        generalTab.write_number(row, col + 1, int(pr1), formatNormal)
        generalTab.write_number(row, col + 2, int(pr2), formatNormal)
        generalTab.write_number(row, col + 3, int(pr3), formatNormal)
        generalTab.write_number(row, col + 4, int(pr4), formatNormal)
        generalTab.write_number(row, col + 5, int(pr5), formatNormal)
        generalTab.write_number(row, col + 6, int(pr6), formatNormal)

        row += 1


    generalTab.write(0, 7, "Total", formatFooter)

    for y in range(1, row):
        formula = "=SUM(%s:%s)" % (i2A1(y, 1), i2A1(y, 6))
        generalTab.write(y, col + 7, formula, formatFooter)


    generalTab.write(row, col, "Total", formatFooter)

    for x in range(1, 8):
        formula = "=SUM(%s:%s)" % (i2A1(1, x), i2A1(row - 1, x))
        generalTab.write(row, x, formula, formatFooter)

    # step 4: prettifying
    generalTab.set_column("A:A", 25)
    generalTab.set_column("B:H", 10)



    xlsx.close()