from report_generator_lib.report_views.xlsx_utils import *
import xlsxwriter
from collections import OrderedDict


def componentCoverageView(features, path):
    mtx = []

    for f in features:
        mtx.append([
            f.id,
            "\n".join([x["name"] for x in f.hbcmFiles]),
            "\n".join([x["path"] for x in f.hbcmFiles])
        ])


    xlsx = xlsxwriter.Workbook(path)

    formatGreen = xlsx.add_format(XlsxCellStyle.green)
    formatRed = xlsx.add_format(XlsxCellStyle.red)
    formatHeader = xlsx.add_format(XlsxCellStyle.header)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)
    formatPercentage = xlsx.add_format(XlsxCellStyle.percentage)
    formatFooter = xlsx.add_format(XlsxCellStyle.footer)
    formatFooterPercentage = xlsx.add_format(XlsxCellStyle.footerPercentage)


    # step 1: init work sheet
    generalTab = xlsx.add_worksheet("General")
    row = 0
    col = 0

    # step 2: headers
    generalTab.write(row, col,       "Feature id",             formatHeader)
    generalTab.write(row, col + 1,   "hbcm files (name)",      formatHeader)
    generalTab.write(row, col + 2,   "hbcm files (path)",      formatHeader)

    row += 1

    # step 3: main data
    for featureId, hbcmFileName, hbcmFilePath in mtx:
        rowFormat = formatRed

        if len(hbcmFileName):
            rowFormat = formatGreen

        generalTab.write(row, col,       featureId,     rowFormat)
        generalTab.write(row, col + 1,   hbcmFileName,  rowFormat)
        generalTab.write(row, col + 2,   hbcmFilePath,  rowFormat)

        row += 1

    # step 4: prettifying
    generalTab.autofilter("A1:C1")
    generalTab.set_column("A:A", 40)
    generalTab.set_column("B:B", 50)
    generalTab.set_column("C:C", 150)
    generalTab.freeze_panes(1, 0)


    #*********************************************************
    domains = list()

    for f in features:
        domain = (f.id.split("-"))[2]
        if not domain in domains:
            domains.append(domain)


    # step 1: init work sheat
    statisticsTab = xlsx.add_worksheet("Statistics")
    row = 0
    col = 0

    # step 2: headers
    statisticsTab.write(row, col, "Domain name", formatHeader)
    statisticsTab.write(row, col + 1, "Covered", formatHeader)
    statisticsTab.write(row, col + 2, "Overall", formatHeader)
    statisticsTab.write(row, col + 3, "Percentage covered", formatHeader)

    row += 1

    # step 3: main data
    for domain in domains:
        statisticsTab.write(row, col,       domain,     formatNormal)
        formula = '=COUNTIFS(General!A:A, "*"&%s&"-*", General!B:B,  "*")' % i2A1(row, col)
        statisticsTab.write(row, col + 1, formula, formatNormal)
        formula = '=COUNTIF(General!A:A,"*"&%s&"-*")' % i2A1(row, col)
        statisticsTab.write(row, col + 2, formula, formatNormal)
        formula = '=IFERROR(%s/%s, "N/A")' % (i2A1(row, col + 1), i2A1(row, col + 2))
        statisticsTab.write(row, col + 3, formula, formatPercentage)

        row += 1

    statisticsTab.write(row, col, "Overall", formatFooter)
    f1 = '=SUM(%s:%s)' % (i2A1(1, 1), i2A1(row - 1, 1))
    f2 = '=SUM(%s:%s)' % (i2A1(1, 2), i2A1(row - 1, 2))
    f3 = '=IFERROR(%s/%s, "N/A")' % (i2A1(row, 1), i2A1(row, 2))
    statisticsTab.write(row, col + 1, f1, formatFooter)
    statisticsTab.write(row, col + 2, f2, formatFooter)
    statisticsTab.write(row, col + 3, f3, formatFooterPercentage)

    row += 1

    # step 4: prettifying
    statisticsTab.set_column("A:A", 25)
    statisticsTab.set_column("B:D", 15)
    statisticsTab.autofilter("A1:D1")

    xlsx.close()