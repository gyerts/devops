from report_generator_lib.report_views.xlsx_utils import *
import re, xlsxwriter


def defineDomain(string, mapping):
    result = "Unknown domain"

    for key in mapping.keys():
        if key in string:
            result = mapping[key]
            break

    return result


def replaceNames(string, mapping):
    result = string

    for name in mapping.keys():
        result = result.replace(name, mapping[name])

    return result


def getFileFormat(text):
    result = str()
    results = re.findall("(\.[\w\d\_]+)", text)

    if len(results):
        result = results[-1]

    return result


def read_ini(path, section):
    mappingDict = dict()

    with open(path, "r") as file:
        searchResults = re.findall("\[%s\]([\w\s\/\=\>\-]+)\[" % section, file.read())

        if len(searchResults):
            for x in searchResults[0].split("\n"):
                if "=" in x:
                    parts = x.split("=")
                    mappingDict[parts[0].strip()] = parts[1].strip().replace(">", " / ")

    return mappingDict



def fullDataView(commits, path, domainsMapFile=None, namesMapFile=None):
    domainsMap = None
    namesMap = None

    if domainsMapFile is not None:
        domainsMap = read_ini(domainsMapFile, "PathRespMapping")
        #[print(x) for x in domainsMap.items()]

    if namesMapFile is not None:
        namesMap = read_ini(namesMapFile, "Workers")
        #[print(x) for x in namesMap.items()]


    mtx = []

    for x in commits:
        row = [x.cl, x.date]
        row.append(x.reviewUrl)
        row.append(str(x.foundInPer))
        row.append(str(x.foundInCru))

        if namesMap:
            row.append(replaceNames(x.author, namesMap))
        else:
            row.append(x.author)

        if domainsMap:
            row.append(defineDomain("\n".join(x.files), domainsMap))
        else:
            row.append("")

        row.append(x.reviewState)

        if namesMap:
            row.append(replaceNames("\n".join(x.reviewers), namesMap))
        else:
            row.append("\n".join(x.reviewers))

        row.append("\n".join(x.files))
        row.append("\n".join(list(set([getFileFormat(f) for f in x.files]))))
        row.append(str(x.commentsCount))

        if namesMap:
            row.append(replaceNames(x.commentsText, namesMap))
        else:
            row.append(x.commentsText)

        row.append(x.timeSpent)

        mtx.append(row)


    xlsx = xlsxwriter.Workbook(path)
    formatNormal = xlsx.add_format(XlsxCellStyle.normal)
    formatHeader = xlsx.add_format(XlsxCellStyle.header)
    formatLink = xlsx.add_format(XlsxCellStyle.link)
    formatGreen = xlsx.add_format(XlsxCellStyle.green)
    formatRed = xlsx.add_format(XlsxCellStyle.red)
    formatPercentage = xlsx.add_format(XlsxCellStyle.percentage)
    formatFooter = xlsx.add_format(XlsxCellStyle.footer)
    formatFooterPercentage = xlsx.add_format(XlsxCellStyle.footerPercentage)

    # step 1: init work sheat
    generalTab = xlsx.add_worksheet("General")
    row = 0
    col = 0

    # step 2: headers
    generalTab.write(row, col,     "Change list", formatHeader)
    generalTab.write(row, col + 1, "Date", formatHeader)
    generalTab.write(row, col + 2, "Review", formatHeader)
    generalTab.write(row, col + 3, "Found in Perforce", formatHeader)
    generalTab.write(row, col + 4, "Found in Crucible", formatHeader)
    generalTab.write(row, col + 5, "Author", formatHeader)
    generalTab.write(row, col + 6, "Domain", formatHeader)
    generalTab.write(row, col + 7, "State", formatHeader)
    generalTab.write(row, col + 8, "Reviewers", formatHeader)
    generalTab.write(row, col + 9, "Files", formatHeader)
    generalTab.write(row, col + 10,"Formats", formatHeader)
    generalTab.write(row, col + 11,"Comments", formatHeader)
    generalTab.write(row, col + 12,"Comments text", formatHeader)
    generalTab.write(row, col + 13,"Time spent", formatHeader)

    row += 1

    # step 3: main data
    for cl, date, key, foundInPer, foundInCru, author, domain, state, reviewers, files, formats, comments, commentsText, timeSpent in mtx:
        generalTab.write(row, col, cl, formatNormal)
        generalTab.write(row, col + 1, date, formatNormal)
        linkText = key.split("/")[-1]
        generalTab.write_url(row, col + 2, key, formatLink, linkText)
        formatForFoundInPer = formatRed
        formatForFoundInCru = formatRed

        if foundInPer == "True":
            formatForFoundInPer = formatGreen

        if foundInCru == "True":
            formatForFoundInCru = formatGreen

        generalTab.write(row, col + 3, foundInPer, formatForFoundInPer)
        generalTab.write(row, col + 4, foundInCru, formatForFoundInCru)
        generalTab.write(row, col + 5, author, formatNormal)
        generalTab.write(row, col + 6, domain, formatNormal)
        generalTab.write(row, col + 7, state, formatNormal)
        generalTab.write(row, col + 8, reviewers, formatNormal)
        generalTab.write(row, col + 9, files, formatNormal)
        generalTab.write(row, col + 10, formats, formatNormal)
        generalTab.write(row, col + 11, comments, formatNormal)
        generalTab.write(row, col + 12, commentsText, formatNormal)
        generalTab.write(row, col + 13, timeSpent, formatNormal)

        row += 1

    # step 4: prettifying
    generalTab.freeze_panes(1, 0)
    generalTab.autofilter("A1:L1")
    generalTab.set_column("A:A", 8)
    generalTab.set_column("B:B", 10)
    generalTab.set_column("C:C", 16)
    generalTab.set_column("D:E", 5)
    generalTab.set_column("F:F", 15)
    generalTab.set_column("G:G", 20)
    generalTab.set_column("H:H", 10)
    generalTab.set_column("I:I", 15)
    generalTab.set_column("J:J", 120)
    generalTab.set_column("K:K", 8)
    generalTab.set_column("L:L", 3)
    generalTab.set_column("M:M", 50)
    generalTab.set_column("N:N", 4)

    #*******************************************************************************************************************
    # Statistics
    domains = [
        "Applications / Engine",
        "Applications / HTML",
        "Diagnosis",
        "OnlineServices",
        "SWUpdate",
        "HMI",
        "Speech",
        "SWSystem / AvcLan",
        "SWSystem / Connectivity",
        "SWSystem / IODevices",
        "SWSystem / SystemCore",
        "UnknownDomain"
    ]

    # step 1: init work sheat
    statisticsTab = xlsx.add_worksheet("Statistics")
    row = 0
    col = 0

    # step 2: headers
    statisticsTab.write(row, col,      "Domain", formatHeader)
    statisticsTab.write(row, col + 1,  "All commits", formatHeader)
    statisticsTab.write(row, col + 2,  "Reviewed commits", formatHeader)
    statisticsTab.write(row, col + 3,  "Not reviewed commits", formatHeader)
    statisticsTab.write(row, col + 4,  "Review coverage percentage", formatHeader)
    statisticsTab.write(row, col + 5,  "Reviews with comments", formatHeader)
    statisticsTab.write(row, col + 6,  "Reviews with comments percentage", formatHeader)
    statisticsTab.write(row, col + 7,  "Reviews with ts <= 3m", formatHeader)
    statisticsTab.write(row, col + 8,  "Reviews with ts <= 3m percentage", formatHeader)

    row += 1

    # step 3: main data

    for domain in domains:
        statisticsTab.write(row, col, domain, formatNormal)
        formula = '=COUNTIF(General!G:G, %s)' % i2A1(row, col)
        statisticsTab.write(row, col + 1, formula, formatNormal)

        formula = '=COUNTIFS(General!G:G, %s, General!C:C,  "CR-HTOYOTA-*")' % i2A1(row, col)
        statisticsTab.write(row, col + 2, formula, formatNormal)

        formula = '=%s-%s' % (i2A1(row, col + 1), i2A1(row, col + 2))
        statisticsTab.write(row, col + 3, formula, formatNormal)

        formula = '=IFERROR(%s/%s, "N/A")' % (i2A1(row, col + 2), i2A1(row, col + 1))
        statisticsTab.write(row, col + 4, formula, formatPercentage)

        formula = '=COUNTIFS(General!G:G, %s, General!M:M,  "*")' % i2A1(row, col)
        statisticsTab.write(row, col + 5, formula, formatNormal)

        formula = '=IFERROR(%s/%s, "N/A")' % (i2A1(row, col + 5), i2A1(row, col + 1))
        statisticsTab.write(row, col + 6, formula, formatPercentage)

        formula = '=COUNTIFS(General!G:G, %s, General!N:N,  "3m") + COUNTIFS(General!G:G, %s, General!N:N,  "2m") + COUNTIFS(General!G:G, %s, General!N:N,  "1m")'\
                  % (i2A1(row, col), i2A1(row, col), i2A1(row, col))
        statisticsTab.write(row, col + 7, formula, formatNormal)

        formula = '=IFERROR(%s/%s, "N/A")' % (i2A1(row, col + 7), i2A1(row, col + 1))
        statisticsTab.write(row, col + 8, formula, formatPercentage)

        row += 1

    statisticsTab.write(row, col, "Overall", formatFooter)
    fB = "=SUM(%s:%s)" % (i2A1(1, 1), i2A1(row - 1, 1))
    fC = "=SUM(%s:%s)" % (i2A1(1, 2), i2A1(row - 1, 2))
    fD = "=SUM(%s:%s)" % (i2A1(1, 3), i2A1(row - 1, 3))
    fE = "=AVERAGE(%s:%s)" % (i2A1(1, 4), i2A1(row - 1, 4))
    fF = "=SUM(%s:%s)" % (i2A1(1, 5), i2A1(row - 1, 5))
    fG = "=AVERAGE(%s:%s)" % (i2A1(1, 6), i2A1(row - 1, 6))
    fH = "=SUM(%s:%s)" % (i2A1(1, 7), i2A1(row - 1, 7))
    fI = "=AVERAGE(%s:%s)" % (i2A1(1, 8), i2A1(row - 1, 8))

    statisticsTab.write(row, col + 1, fB, formatFooter)
    statisticsTab.write(row, col + 2, fC, formatFooter)
    statisticsTab.write(row, col + 3, fD, formatFooter)
    statisticsTab.write(row, col + 4, fE, formatFooterPercentage)
    statisticsTab.write(row, col + 5, fF, formatFooter)
    statisticsTab.write(row, col + 6, fG, formatFooterPercentage)
    statisticsTab.write(row, col + 7, fH, formatFooter)
    statisticsTab.write(row, col + 8, fI, formatFooterPercentage)

    # step 4: prettifying
    statisticsTab.set_column("A:A", 25)
    statisticsTab.set_column("B:I", 15)
    statisticsTab.autofilter("A1:I1")

    xlsx.close()
