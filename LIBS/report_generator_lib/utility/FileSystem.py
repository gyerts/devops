import os

class FileSystem():
    occurrence = 1
    endsWith = 2
    fullMatch = 3

    @staticmethod
    def getFilesInDir(type, path):
        result = list()

        for x in os.listdir(path):
            p = os.path.join(path, x)
            if os.path.isfile(p):
                if x.lower().endswith(type):
                    result.append(p)

        return result



    @staticmethod
    def find(filter, searchDir, searchType = occurrence):
        result = list()

        if os.path.isdir(searchDir):
            for dirpath, dirnames, files in os.walk(searchDir):
                for name in files:
                    if searchType == FileSystem.occurrence:
                        if filter in name:
                            result.append(os.path.join(dirpath, name))

                    elif searchType == FileSystem.endsWith:
                        if name.lower().endswith(filter):
                            result.append(os.path.join(dirpath, name))

                    elif searchType == FileSystem.fullMatch:
                        if filter == name:
                            result.append(os.path.join(dirpath, name))

        return result

    @staticmethod
    def fileName(path):
        return os.path.basename(path)


    @staticmethod
    def isFile(path):
        return os.path.isfile(path)


    @staticmethod
    def isDir(path):
        return os.path.isdir(path)


    @staticmethod
    def dirName(path):
        return os.path.dirname(path)


    @staticmethod
    def join(left, right):
        return os.path.join(left, right)


    @staticmethod
    def checkCreateDir(path):
        if not os.path.exists(path):
            os.makedirs(path)

