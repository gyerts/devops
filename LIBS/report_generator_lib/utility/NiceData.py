import json
from collections import OrderedDict


class NiceData:
    @staticmethod
    def dict2json(inputDict, outputJsonFile):
        with open(outputJsonFile, "w") as outFile:
            json.dump(inputDict, outFile, ensure_ascii = False, sort_keys = True, indent = 4)
            outFile.close()


    @staticmethod
    def json2dict(inputJsonFile):
        data = OrderedDict()
        with open(inputJsonFile, "r") as inFile:
            data = json.load(inFile)#, object_pairs_hook = OrderedDict)

        return data


    @staticmethod
    def json2html(inputJsonFile, outputHtmlFile, headers = list()):
        with open(inputJsonFile, "r") as inFile:
            data = json.load(inFile, object_pairs_hook = OrderedDict)

            htmlTable = "<table border = 1 cellpadding = 4 cellspacing = 0>"
            mtx = [headers]

            for x in data.keys():
                mtx.append([x, "<br/>".join(data[x])])

            for i in range(len(mtx)):
                htmlTable += "<tr>"

                for j in range(len(mtx[i])):
                    if i == 0:
                        htmlTable += ("<th>%s</th>" % mtx[i][j])
                    else:
                        htmlTable += "<td>%s</td>" % mtx[i][j]

                htmlTable += "</tr>"

            htmlTable += "</table><br>"

            with open(outputHtmlFile, "w") as outFile:
                outFile.write(htmlTable)
                outFile.close()
