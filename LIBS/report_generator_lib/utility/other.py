import traceback

def traceErr():
        return traceback.format_exc().replace("\n", "<br>")


def list2str(list):
    return "\n".join(list)