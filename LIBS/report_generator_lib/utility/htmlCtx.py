def colorStr(string, color = "black"):
    col = color

    return "<b><font color='%s'>%s</font></b>" % (col, string)


def highlightedStr(string, color):
    return "<span style='background-color: %s'>%s</span>" % (color, string)


def href(string, url):
    return "<b><a href='%s'>%s</a></b>" % (url, string)


def striked(string):
    return "<s>%s</s>" % string


def headerL2(text):
    return "<h2>%s</h2>" % text


def headerL3(text):
    return "<h3>%s</h3>" % text


