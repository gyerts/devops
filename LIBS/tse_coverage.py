
#to use this script you should install:
# sudo apt-get install libssl-dev
# sudo pip3 install paramiko

from report_generator_lib.trs_tse_report.TrsTseReport import TrsTseReport
from report_generator_lib.report_views.trs_tse_test_coverage_view import testCoverageView
import os, sys
from threading import Thread

def foo():
    path = saveReportAs
    testCoverageView(x.features, path)

if __name__ == '__main__':
	dr = sys.argv[1]
	path_to_project = sys.argv[2]
	path_to_artifact = sys.argv[3]
	x = TrsTseReport()
	x.trsListPath = os.path.join(path_to_project, 'config', 'test_coverage', 'all_trs_list.txt')
	# -----------------------------------------------
	# -----------------------------------------------
	x.trsFilter = ""
	# -----------------------------------------------
	# -----------------------------------------------
	x.clientDir = path_to_project

	#binaries path from where binareies fill be taken
	x.productsDir = path_to_project + "_products"
	x.getHbtsOption = True
	x.getHbcmOption = False
	x.getBinOption = False
	x.execTestsOption = False

	path_where_report_should_by_placed = os.path.join(path_to_artifact, 'Generated_Reports')
	if not os.path.exists(path_where_report_should_by_placed):
	    os.makedirs(path_where_report_should_by_placed)

	saveReportAs = os.path.join(path_where_report_should_by_placed, 'tse_coverage_CL%s.xlsx' % dr)
	print('<h1>', saveReportAs, '</h1>')

	x.endCall = foo

	Thread(target = x.start).start()
