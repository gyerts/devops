from datetime import datetime

alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']


def __divX(x, base, buff):
    nx =  int(x / base)
    rx = (x - nx * base) % base

    buff.append(rx)

    if nx >= base:
        __divX(nx, base, buff)
    else:
        if nx:
            buff.append(nx - 1)


def int_to_alphabet(int_x):
    numbers = list()
    __divX(int_x, len(alphabet), numbers)
    numbers.reverse()
    liters = [alphabet[x] for x in numbers]

    return liters


def get_label_meucy17(list_of_exists_labels):

    now = datetime.now()

    year = str(now.year)[2:]
    day = str(now.isoweekday())
    week = str(now.isocalendar()[1])
    branch = 'A'
    new_name = None

    counter = 0

    while True:
        last_part = str()
        for liter in int_to_alphabet(counter):
            last_part += liter

        new_name = branch + year + week + day + last_part

        for exists_label in list_of_exists_labels:
            if exists_label.upper() == new_name.upper():
                in_list = True
                break
        else:
            break
        counter += 1

    return new_name
