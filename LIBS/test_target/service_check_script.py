#!/usr/bin/python


import paramiko
import datetime
import stat, sys, os, string, commands
import psutil
import argparse

host = sys.argv[1]
user = sys.argv[2]
secret = sys.argv[3]
port = sys.argv[4]

path = os.getcwd() + "/Luxoft/base/test_target/"

client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname=host, username=user, password=secret, port=int(port))

stdin, stdout, stderr = client.exec_command("ls -l /lib/systemd/system")
data = stdout.read() + stderr.read()

logName = str(datetime.datetime.now())

f1 = open(path + "services" + logName+'.txt', "w")
f2 = open(path + 'list-of-serv.textile', 'r')

serv_str_good = "\n services OK-------- \n\n"
serv_str_bad = "\n services NOK-------- \n\n"

print ("---------------------------------SERVICES---------------------------------")

for val in f2:
	if data.find(val.strip()) == -1:
		serv_str_bad = (serv_str_bad + val.strip() +'\n')
		print (val.strip() + "'\033[1;41m	- NOK\033[1;m'")
	else:
		serv_str_good = (serv_str_good + val.strip() +'\n')
		print (val.strip() + "	- OK")

f1.write(serv_str_good + serv_str_bad + "\n\n")



