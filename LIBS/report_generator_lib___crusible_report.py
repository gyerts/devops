from report_generator_lib.crusible_report.convert_commits_to_changes import convert_commits_to_changes
from report_generator_lib.report_views.sca_report_view import sca_report_view
from report_generator_lib.crusible_report.CrucibleReport import CrucibleReport

from perforce.p4cover import P4Cover

import sys, getopt, json


def post_action(x, name_of_xlsx_output_file, path_responsibilities_ini_file, path_to_names_map_ini):
    output = list()

    for commit in x.commits:
        output.append(commit.serialize())

    if True == x.json_output:
        with open("{}.json".format(name_of_xlsx_output_file), "w") as j:
            json.dump(output, j, indent=4)

    output = convert_commits_to_changes(path_responsibilities_ini_file, path_to_names_map_ini, x.commits)
    sca_report_view(output, "{}.xlsx".format(name_of_xlsx_output_file), path_responsibilities_ini_file)
    return output


def run(
        path_to_perforce_submits_txt_file,
        count_of_commits,
        from_date,
        to_date,
        json_output,
        responsibilities_ini,
        names_ini,
        threads_count,
        path_to_p4,
        p4_server_port,
        p4_user,
        p4_password,
        p4_repository_to_parse_commits,
        fisheye_url,
        crusible_repo_name,
        review_prefix,
        crusible_user,
        crusible_password,
        crusible_single_file
):
    p4 = P4Cover(
        path_to_p4=path_to_p4,
        user=p4_user,
        password=p4_password,
        ip_port=p4_server_port
    )

    if "/..." in p4_repository_to_parse_commits:
        p4.execute('changes -m {} -l -s submitted {} > {}'.format(
            count_of_commits,
            p4_repository_to_parse_commits,
            path_to_perforce_submits_txt_file
        ))
    else:
        raise ValueError("p4_repository_to_parse_commits should ends with /...")

    x = CrucibleReport()
    x.p4ChangeFile = path_to_perforce_submits_txt_file

    x.dateFrom = from_date
    x.dateTo = to_date

    print(x.dateFrom)
    print(x.dateTo)

    x.url = fisheye_url
    x.crusible_repo_name = crusible_repo_name
    x.review_prefix = review_prefix
    x.user = crusible_user
    x.password = crusible_password
    x.threads_count = threads_count
    x.endCall = post_action
    x.json_output = json_output

    x.name_of_xlsx_output_file = crusible_single_file
    x.path_responsibilities_ini_file = responsibilities_ini
    x.path_to_names_map_ini = names_ini

    output = x.start()

    return output


if __name__ == "__main__":
    long = [
        "count_of_commits",
        "from_date",
        "to_date",
        "json_output",
        "responsibilities_ini",
        "names_ini",
        "threads_count",
        "path_to_p4",
        "p4_server_port",
        "p4_user",
        "p4_password",
        "p4_repository_to_parse_commits",
        "fisheye_url",
        "crusible_repo_name",
        "review_prefix",
        "crusible_user",
        "crusible_password",
        "crusible_single_file"
    ]

    try:
        opts, args = getopt.getopt(sys.argv[1:], "", long)

    except getopt.GetoptError as opt:
        print(opt)
        sys.exit(2)

    # convert [ ('', ''), ... ] => { '': '', ... }
    opts = dict()

    for arg in args:
        print("-------------------->", arg)
        l = arg.split("=")
        opts[l[0]] = l[1]

    try:
        output = run(
            path_to_perforce_submits_txt_file='perforce_submits.txt',
            count_of_commits=opts["count_of_commits"],
            from_date=opts["from_date"],
            to_date=opts["to_date"],
            json_output=True if "True" == opts["json_output"] else False,
            responsibilities_ini=opts["responsibilities_ini"],
            names_ini=opts["names_ini"],
            threads_count=int(opts["threads_count"]),
            path_to_p4=opts["path_to_p4"],
            p4_server_port=opts["p4_server_port"],
            p4_user=opts["p4_user"],
            p4_password=opts["p4_password"],
            p4_repository_to_parse_commits=opts["p4_repository_to_parse_commits"],
            fisheye_url=opts["fisheye_url"],
            crusible_repo_name=opts["crusible_repo_name"],
            review_prefix=opts["review_prefix"],
            crusible_user=opts["crusible_user"],
            crusible_password=opts["crusible_password"],
            crusible_single_file=opts["crusible_single_file"]
        )

    except KeyError as key:
        print("no parameter {}, please use next pattern:\n{}".format(key.args[0], ""))
        raise
