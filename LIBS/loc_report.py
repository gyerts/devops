from report_generator_lib.loc_report.LocReport import LocReport
from report_generator_lib.report_views.loc_report_view import *
from converters.FHtml import list_to_html

import os, sys

if __name__ == '__main__':
	dr = sys.argv[1]
	path_to_project = sys.argv[2]
	path_to_artifact = sys.argv[3]
	y = LocReport()
	y.path2domainMappingFile = os.path.join(path_to_project, 'config', 'LintResponsibles.ini')


	y.pathLocCsvFile = os.path.join(path_to_project + '_products', 'Release_docs', 'LOC', 'loc_Result.csv')

	non_gen_data = y.get_non_gen_sctatisctics()
	print(list_to_html(non_gen_data))

	gen_data = y.get_gen_sctatictics()
	print(list_to_html(gen_data))

	other_date = y.get_other_sctatictics()
	print(list_to_html(other_date))

	path = os.path.join(path_to_artifact, 'Generated_Reports', 'loc_test_CL4722953.xlsx')
	loc_report_view(non_gen_data, gen_data, other_date, path)
