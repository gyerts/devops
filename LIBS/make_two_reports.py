from report_generator_lib___crusible_report import run as run_crusible
from report_generator_lib___stash_report import run as run_stash
from report_generator_lib.report_views.sca_report_view import sca_report_view
import sys, getopt
from multiprocessing.pool import ThreadPool

rule = r"""
    Date format should be next: "dd/mm/yyyy"

    make_two_reports.py
        count_of_commits=
        from_date=
        to_date=
        json_output=
        responsibilities_ini=
        names_ini=
        threads_count=
        all_single_file=

        stash_login=
        stash_password=
        stash_url=
        project_name=
        repo_slug=
        branch=
        stash_single_file=

        path_to_p4=
        p4_server_port=
        p4_user=
        p4_password=
        p4_repository_to_parse_commits=
        fisheye_url=
        crusible_repo_name=
        review_prefix=
        crusible_user=
        crusible_password=
        crusible_single_file=

"""


if __name__ == "__main__":
    long = [
        "count_of_commits=",
        "from_date=",
        "to_date=",
        "json_output=",
        "responsibilities_ini=",
        "names_ini=",
        "threads_count=",
        "all_single_file=",

        "stash_login=",
        "stash_password=",
        "stash_url=",
        "project_name=",
        "repo_slug=",
        "branch=",
        "stash_single_file=",

        "path_to_p4=",
        "p4_server_port=",
        "p4_user=",
        "p4_password=",
        "p4_repository_to_parse_commits=",
        "fisheye_url=",
        "crusible_repo_name=",
        "review_prefix=",
        "crusible_user=",
        "crusible_password=",
        "crusible_single_file="
    ]

    try:
        opts, args = getopt.getopt(sys.argv[1:], "", long)

    except getopt.GetoptError as opt:
        print(opt)
        print(rule)
        sys.exit(2)

    # convert [ ('', ''), ... ] => { '': '', ... }
    opts = dict()
    for arg in args:
        l = arg.split("=")
        opts[l[0]] = l[1]

    try:
        pool = ThreadPool(processes=2)

        async_result1 = pool.apply_async(func=run_stash, args=(
            opts["count_of_commits"],
            opts["from_date"],
            opts["to_date"],
            True if "True" == opts["json_output"] else False,
            opts["responsibilities_ini"],
            opts["names_ini"],
            int(opts["threads_count"]),
            opts["stash_login"],
            opts["stash_password"],
            opts["stash_url"],
            opts["project_name"],
            opts["repo_slug"],
            opts["branch"],
            opts["stash_single_file"]
        ))

        async_result2 = pool.apply_async(func=run_crusible, args=(
            'perforce_submits.txt',
            opts["count_of_commits"],
            opts["from_date"],
            opts["to_date"],
            True if "True" == opts["json_output"] else False,
            opts["responsibilities_ini"],
            opts["names_ini"],
            int(opts["threads_count"]),
            opts["path_to_p4"],
            opts["p4_server_port"],
            opts["p4_user"],
            opts["p4_password"],
            opts["p4_repository_to_parse_commits"],
            opts["fisheye_url"],
            opts["crusible_repo_name"],
            opts["review_prefix"],
            opts["crusible_user"],
            opts["crusible_password"],
            opts["crusible_single_file"]
        ))

        stash_changes = async_result1.get()
        crusible_changes = async_result2.get()

        all_changes = crusible_changes + stash_changes

        sca_report_view(
            all_changes,
            "{}.xlsx".format(opts["all_single_file"]),
            opts["responsibilities_ini"]
        )

    except KeyError as key:
        print("no parameter {}, please use next pattern:\n{}".format(key.args[0], rule))
        raise
