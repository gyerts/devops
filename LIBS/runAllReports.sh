
pathToProject='/media/yoctoadm/YoctoDisc/BUILDs/CW18'
cl='4722953'
pathToArt='/media/yoctoadm/YoctoDisc/BUILDs/reports'
pathToLog=log_$cl
mkdir $pathToLog
mkdir $pathToArt'/Generated_Reports'
echo component_coverage
python3 component_coverage.py $cl $pathToProject $pathToArt > $pathToLog/component_coverage.html
echo lint_report
python3 lint_report.py $cl $pathToProject $pathToArt > $pathToLog/lint_report.html
echo loc_report
python3 loc_report.py $cl $pathToProject $pathToArt > $pathToLog/loc_report.html
echo tse_coverage
python3 tse_coverage.py $cl $pathToProject $pathToArt > $pathToLog/tse_coverage.html
echo tse_tests_results
python3 tse_tests_results.py $cl $pathToProject $pathToArt > $pathToLog/tse_tests_results.html
echo run_crucible_report
python3 run_crucible_report.py $cl $pathToProject $pathToArt > $pathToLog/run_crucible_report.html

