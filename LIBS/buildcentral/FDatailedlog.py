def is_errors_in_detailed_log(path_to_detailed_log):
    f = open(path_to_detailed_log)
    lines = f.readlines()

    for line in lines:
        if 'error: ' in line:
            return True

    return False