import os

from mailer.smtplibmailer import all_project_mails, Mailer
from Engine.classes.messages import Messages
from Engine.classes.config import *
from buildcentral.FDatailedlog import is_errors_in_detailed_log
from Engine.classes.create_folder import make_dir_if_not_exists


class BC:
    def __init__(self, path_to_buildcentral, path_to_project_settings, path_to_user_settings):
        print(path_to_buildcentral, path_to_project_settings, path_to_user_settings)

        self.path_to_buildcentral = path_to_buildcentral
        self.path_to_project_settings = path_to_project_settings
        self.path_to_user_settings = path_to_user_settings
        self.is_clean_build = False

    def clean_build(self, boolean):
        self.is_clean_build = boolean


    def run(self, session):
        index = self.path_to_buildcentral.rfind('/')
        path_to_project = self.path_to_buildcentral[0:index]

        cmd = 'cd ' + path_to_project + '\n./buildcentral.sh -p ' + self.path_to_project_settings + ' -u ' + self.path_to_user_settings

        if self.is_clean_build:
            cmd += ' -c'

        cmd += ' -n=\'' + session + '\' -b'
        os.system(cmd)

    def check_and_send(self, domain_name, subject, output, mailer, path_to_project_products, path_to_reports, test):
    
        # build session first
        session = '[Elina2] - ' + domain_name
        print('<h1>Run building session \'' + session + '\'</h1>')
        self.run(session)
    
        
        # send mails to all or just to one recipient?
        if test:
            recipients = all_project_mails.get_domain_responsible_peoples('TEST')
        else:
            recipients = all_project_mails.get_domain_responsible_peoples(domain_name)
    
        # check detailed log and send mails
        path_to_detailed_log = os.path.join(path_to_project_products, 'DetailedLog.hbbl')
        if is_errors_in_detailed_log(path_to_detailed_log):
            mailer.set_recipients(recipients)
            mailer.set_subject(domain_name + subject)
            mailer.attach_file(path_to_detailed_log, 'DetailedLog.hbbl')
    
            mailer.set_html_body(output.replace('<!--domain_name-->', domain_name))
            mailer.send()
    
            # create file in reports
            return True
        return False
    
    def build_all_sessions(self, path_to_reports, settings, unique_nome_of_folder, subject, output, test=0):
        path_to_project_products = settings.get('path_to_project_products')
    
        mailer = Mailer()
        mailer.set_owner_of_mail('buildserver@luxoft.com')
    
        path_to_reports = os.path.join(path_to_reports, unique_nome_of_folder)
        make_dir_if_not_exists(path_to_reports)
    
        self.check_and_send('SWUpdate', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('OnlineServices', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('Diagnosis', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('Speech', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('Connectivity', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('Core', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('IODevices', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('InternetApps', subject, output, mailer, path_to_project_products, path_to_reports, test)
        self.check_and_send('ServiceAdaptation', subject, output, mailer, path_to_project_products, path_to_reports, test)
