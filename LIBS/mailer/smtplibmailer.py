#!/usr/bin/env python3

"""Send the contents of a directory as a MIME message."""

import os
import sys
import smtplib
# For guessing MIME type based on file name extension
import mimetypes

from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

COMMASPACE = ', '


class Mailer:
    def __init__(self):
        self.recipients = None
        self.owner_of_mail = None
        self.path_of_dirrectory_to_send = None
        self.subject = None
        self.outer = MIMEMultipart()

    def set_recipients(self, recipients):
        self.recipients = recipients

    def set_owner_of_mail(self, owner_of_mail):
        self.owner_of_mail = owner_of_mail

    def set_subject(self, subject):
        self.subject = subject

    def set_directory_to_send(self, path_to_directory):
        self.path_of_dirrectory_to_send = path_to_directory

        if os.path.exists(self.path_of_dirrectory_to_send):
            for filename in os.listdir(self.path_of_dirrectory_to_send):
                path = os.path.join(self.path_of_dirrectory_to_send, filename)
                if not os.path.isfile(path):
                    continue
                self.attach_file(path, filename)

    def attach_file(self, path_to_file, filename):
        ctype, encoding = mimetypes.guess_type(path_to_file)
        if ctype is None or encoding is not None:
            ctype = 'application/octet-stream'

        maintype, subtype = ctype.split('/', 1)
        if maintype == 'text':
            with open(path_to_file) as fp:
                msg = MIMEText(fp.read(), _subtype=subtype)
        elif maintype == 'image':
            with open(path_to_file, 'rb') as fp:
                msg = MIMEImage(fp.read(), _subtype=subtype)
        elif maintype == 'audio':
            with open(path_to_file, 'rb') as fp:
                msg = MIMEAudio(fp.read(), _subtype=subtype)
        else:
            with open(path_to_file, 'rb') as fp:
                msg = MIMEBase(maintype, subtype)
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
        msg.add_header('Content-Disposition', 'attachment', filename=filename)
        self.outer.attach(msg)


    def set_html_body(self, html):
        self.html = html
        self.outer.attach(MIMEText(self.html, 'html'))


    def send(self):
        s = smtplib.SMTP('puppy.luxoft.com')

        self.outer['Subject'] = self.subject
        self.outer['To'] = COMMASPACE.join(self.recipients)
        self.outer['From'] = self.owner_of_mail
        self.outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'

        composed = self.outer.as_string()
        s.sendmail(self.owner_of_mail, self.recipients, composed)

        self.clean()

    def clean(self):
        self.outer = MIMEMultipart()


class ProjectStructure:
    def __init__(self):
        self.domains = dict()
        self.everywhere = list()
    
    def add_domain(self, domain_name, TFO_mail, DI_mail, PL_mail, ADD=list()):
        domain_structure = dict()
        
        if TFO_mail != '': domain_structure['TFO'] = TFO_mail
        if DI_mail != '': domain_structure['DI'] = DI_mail
        if PL_mail != '': domain_structure['PL'] = PL_mail
        if len(ADD) > 0: domain_structure['ADD'] = ADD

        self.domains[domain_name] = domain_structure

    def get_domain_responsible_peoples(self, domain):
        ans = list()

        try:
            domain = self.domains[domain]
                        
            if 'TFO' in domain: ans.append(domain['TFO']) 
            if 'DI' in domain: ans.append(domain['DI']) 
            if 'PL' in domain: ans.append(domain['PL']) 
            if 'ADD' in domain:
                for recipient in domain['ADD']:
                    ans.append(recipient)

            if len(self.everywhere) > 0:
                for people in self.everywhere:
                    ans.append(people)
            
        except Exception as ex:
            print(ex)
        
        return ans
    
    def add_everywhere_people(self, people_mail):
        self.everywhere.append(people_mail)

    def del_everywhere_people(self, people_email):
        self.everywhere.remove(people_email)
            
all_project_mails = ProjectStructure()
all_project_mails.add_everywhere_people('YGyerts@luxoft.com')

all_project_mails.add_domain(
    'Networking', 'DRiabtsev@luxoft.com',
    '', '',
    ['ADudko@luxoft.com', 'OMorev@luxoft.com', 'EAndronov@luxoft.com']
)

all_project_mails.add_domain(
    'WiFi', 'DRiabtsev@luxoft.com',
    '', '',
    ['ADudko@luxoft.com', 'OMorev@luxoft.com', 'EAndronov@luxoft.com']
)

all_project_mails.add_domain(
    'Phone Connection Manager', 'VProtas@luxoft.com',
    '', '',
    ['SSidorenko@luxoft.com', 'DAKotov@luxoft.com', 'BChystikov@luxoft.com', 'MDenysiuk@luxoft.com']
)

all_project_mails.add_domain(
    'Personalization', 'VProtas@luxoft.com',
    '', '',
    ['SSidorenko@luxoft.com']
)

all_project_mails.add_domain('TEST', '', '', '')











# mailer = Mailer()
# mailer.set_recipients(['MDenysiuk@luxoft.com'])
# mailer.set_owner_of_mail('VProtas@luxoft.com')
# mailer.set_subject('AUTOMATION ERRORS BY CL 000000')
# mailer.attach_file('/home/mib3/mib3/B0/R14/sdk/readme.txt', 'report.dfgdfg')
#
# html = """
#             <p>Hi!<br>
#                <b style='color: red'>please check Junk E-Mail if you use owa.luxoft.com!</b>
#             </p>
#         """
# mailer.set_html_body(html)
#
# mailer.send()
