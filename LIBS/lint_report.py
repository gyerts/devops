from report_generator_lib.lint_report.LintReport import LintReport
from report_generator_lib.report_views.lint_report_view import lint_report_view
from converters.FHtml import list_to_html

import os, sys



if __name__ == '__main__':
	dr = sys.argv[1]
	path_to_project = sys.argv[2]
	path_to_artifact = sys.argv[3]
	x = LintReport()
	x.sourceFile = os.path.join(path_to_project + '_products', 'Release_docs', 'LINT', 'LintReport.txt')
	u = x.getLintStatistics()
	lint_report_view(u, os.path.join(path_to_artifact, 'Generated_Reports', 'lint_test_CL%s.xlsx' % dr))

	print(list_to_html(u))