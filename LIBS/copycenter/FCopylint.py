import shutil
import os

from Luxoft.base.utility.messages import Messages
from Luxoft.base.utility.execute import execute

LOGGER = Messages()

path_to_product_folder = '/home/ygyerts/MEUCY17/WEB_AUTOMATIZATION_products/armv7-meucy17-adt4.9.1-nop-trc-dbg-rtti'
path_to_output_report_folder = '/home/ygyerts/Reports'
path_to_lint_report = '/home/ygyerts/MEUCY17/WEB_AUTOMATIZATION_products/armv7-meucy17-adt4.9.1-nop-trc-dbg-rtti/lint/report'
backslash = '/'

def copy_lint(cl):
    global path_to_output_report_folder
    path_to_output_report_folder = path_to_output_report_folder + backslash + str(cl)
    lint_output_folder = path_to_output_report_folder + backslash + 'LINT'


    LOGGER.add_message('creating folder for lint report in \'' + path_to_output_report_folder + '\' folder')

    if os.path.exists(lint_output_folder):
            LOGGER.add_message('Error: Path already exists')
            return

    try:
        os.makedirs(lint_output_folder)
    except Exception as ex:
        LOGGER.add_message('Error: Can\'t create folder for some reason')
        LOGGER.add_message('Exeption: ' + str(ex))
        return


    LOGGER.add_message('creating lint.tar.gz to temporary folder')

    cmd = 'tar -cvzf lint.tar.gz -C ' + path_to_product_folder + backslash + ' lint'
    try:
        execute(LOGGER, cmd)
    except Exception as ex:
        LOGGER.add_message('Error: can\'t run the command: ' + cmd)
        return


    LOGGER.add_message('move lint.tar.gz to \'' + lint_output_folder + '\' folder')

    try:
        shutil.move('lint.tar.gz', lint_output_folder)
    except Exception as ex:
        LOGGER.add_message('Error: can\'t find \'lint.tar.gz\'')
        return

    path_to_lint_report_txt = path_to_lint_report + backslash + 'PCLintData' + backslash + 'LintReport.txt'
    path_to_lint_report_txt_destination = lint_output_folder + backslash + 'LintReport.txt'

    try:
        shutil.copyfile(path_to_lint_report_txt, path_to_lint_report_txt_destination)
    except Exception as ex:
        LOGGER.add_message('Error: file ' + path_to_lint_report_txt_destination + ' not exists!')

    LOGGER.add_message('Done!')