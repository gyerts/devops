import shutil
import os

from Luxoft.base.utility.messages import Messages
from Luxoft.base.utility.execute import execute

LOGGER = Messages()

path_to_product_folder = '/home/ygyerts/MEUCY17/WEB_AUTOMATIZATION_products/armv7-meucy17-adt4.9.1-nop-trc-dbg-rtti'
path_to_output_report_folder = '/home/ygyerts/Reports'
backslash = '/'

def copy_tests(cl):
    global path_to_output_report_folder
    path_to_output_report_folder = path_to_output_report_folder + backslash + str(cl)
    tests_output_folder = path_to_output_report_folder + backslash + 'TEST'

# ---------------------------------------------------------------------------------------------------------------------
    LOGGER.add_message('creating folder for test report in \'' + path_to_output_report_folder + '\' folder')

    if os.path.exists(tests_output_folder):
            LOGGER.add_message('Error: Path already exists')
            return

    try:
        os.makedirs(tests_output_folder)
    except Exception as ex:
        LOGGER.add_message('Error: Can\'t create folder for some reason')
        LOGGER.add_message('Exeption: ' + str(ex))
        return

# ---------------------------------------------------------------------------------------------------------------------
    LOGGER.add_message('creating test.tar.gz to temporary folder')

    cmd = 'tar -cvzf test.tar.gz -C ' + path_to_product_folder + backslash + ' test'
    try:
        execute(LOGGER, cmd)
    except Exception as ex:
        LOGGER.add_message('Error: can\'t run the command: ' + cmd)
        return


# ---------------------------------------------------------------------------------------------------------------------
    LOGGER.add_message('move test.tar.gz to \'' + tests_output_folder + '\' folder')

    try:
        shutil.move('test.tar.gz', tests_output_folder)
    except Exception as ex:
        LOGGER.add_message('Error: can\'t find \'test.tar.gz\'')
        return

    LOGGER.add_message('Done!')
    return