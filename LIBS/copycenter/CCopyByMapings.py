import shutil
import os


path_to_product_folder = '/home/ygyerts/MEUCY17/WEB_AUTOMATIZATION_products/armv7-meucy17-adt4.9.1-nop-trc-dbg-rtti'
path_to_output_report_folder = '/home/ygyerts/Reports'
backslash = '/'

def copy_loc(cl):
    global path_to_output_report_folder
    path_to_output_report_folder = path_to_output_report_folder + backslash + str(cl)
    loc_output_folder = path_to_output_report_folder + backslash + 'LOC'

# ---------------------------------------------------------------------------------------------------------------------
    LOGGER.add_message('creating folder for loc report in \'' + path_to_output_report_folder + '\' folder')

    if os.path.exists(loc_output_folder):
            LOGGER.add_message('Error: Path already exists')
            return

    try:
        os.makedirs(loc_output_folder)
    except Exception as ex:
        LOGGER.add_message('Error: Can\'t create folder for some reason')
        LOGGER.add_message('Exeption: ' + str(ex))
        return

# ---------------------------------------------------------------------------------------------------------------------
    LOGGER.add_message('creating loc.tar.gz to temporary folder')

    cmd = 'tar -cvzf loc.tar.gz -C ' + path_to_product_folder + backslash + ' loc'
    try:
        execute(LOGGER, cmd)
    except Exception as ex:
        LOGGER.add_message('Error: can\'t run the command: ' + cmd)
        return


# ---------------------------------------------------------------------------------------------------------------------
    LOGGER.add_message('move loc.tar.gz to \'' + loc_output_folder + '\' folder')

    try:
        shutil.move('loc.tar.gz', loc_output_folder)
    except Exception as ex:
        LOGGER.add_message('Error: can\'t find \'loc.tar.gz\'')
        return


# ---------------------------------------------------------------------------------------------------------------------
    LOGGER.add_message('Fetch loc output from DetailedLog.hbbl')

    path_to_datailed_log = path_to_product_folder + backslash + 'DetailedLog.hbbl'
    path_to_lint_report_txt_destination = loc_output_folder + backslash + 'LocReport.txt'

    try:
        f = open(path_to_datailed_log)
    except Exception as ex:
        # /-----------/ add to log -----------#
        LOGGER.add_message('  error: can\'t find: ' + path_to_datailed_log)
        # \----------- add to log /-----------#
        return

    lines = f.readlines()

    q = 26 #  should be lines in loc report
    counter = 0
    start = False
    report = str()
    for line in lines:
        if start == True or 'loc.pl - 2.10' in line:
            start = True
            if counter < q:
                report += line
                counter += 1
            else:
                break

    if len(report) == 0:
        LOGGER.add_message('  error: can\'t find loc lines in: ' + path_to_datailed_log)
        return


    try:
        output = open(path_to_lint_report_txt_destination, 'w')
        output.write(report)
        output.close()
    except Exception as ex:
        LOGGER.add_message('  error: can\'t open for write: ' + path_to_lint_report_txt_destination)
        return

    LOGGER.add_message('  ---- loc report saved in: ' + path_to_lint_report_txt_destination)

    LOGGER.add_message('Done!')
    return