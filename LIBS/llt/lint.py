# script which compares LintResponsibilities.ini file with existing files and
# tell you status is some folder are present or absent

import os

path_to_project = '/home/ygyerts/MEUCY17/REPORTS'

def get_list_of_files_in_dirrectory(path_to_project, add_before):
    output = list()
    dirs = os.listdir(path_to_project)

    for obj in dirs:
        full_path_to_obj = os.path.normpath(path_to_project + '/' + obj)
        if os.path.isdir(full_path_to_obj):
            output.append(os.path.normpath(add_before + '/' + obj))

    return output

def get_list_of_lines_in_file(path_to_file):
    f = open(path_to_file)
    lines = f.readlines()

    start = False

    lines_in_file = list()
    for line in lines:
        line = line.strip()

        if line == '':
            continue
        if '[PathRespMapping]' in line:
            start = True
            continue
        elif '[' in line:
            start = False
            break

        if start:
            lines_in_file.append(line)
    return lines_in_file

def compare_lines(list_of_files_on_drive, lines_in_file):
    lines_not_in_which_not_exists_any_more = list()
    for line in lines_in_file:
        founded = False
        for directory in list_of_files_on_drive:
            if directory + ' ' in line or directory + '=' in line:
                founded = True
                break
        if not founded:
            lines_not_in_which_not_exists_any_more.append(line)


    not_added_directories = list()
    for directory in list_of_files_on_drive:
        founded = False
        for line in lines_in_file:
            if directory + ' ' in line or directory + '=' in line:
                founded = True
                break
        if not founded:
            not_added_directories.append(directory)

    return {'lines_not_in_which_not_exists_any_more': lines_not_in_which_not_exists_any_more,
            'not_added_directories': not_added_directories }

def show(message, lines):
    if len(lines) == 0:
        return
    print('\n')
    print('---> ', message)
    for line in lines:
        print(line)

list_of_files_on_drive = list()
list_of_files_on_drive += get_list_of_files_in_dirrectory(os.path.normpath(path_to_project + '/imp'), 'imp')
list_of_files_on_drive += get_list_of_files_in_dirrectory(os.path.normpath(path_to_project + '/api'), 'api')

lines = get_list_of_lines_in_file(os.path.normpath(path_to_project + '/config/LintResponsibles.ini'))

diffs = compare_lines(list_of_files_on_drive, lines)

show('You should delete this lines from file:', diffs['lines_not_in_which_not_exists_any_more'])
show('You should add this dirs to file:', diffs['not_added_directories'])
