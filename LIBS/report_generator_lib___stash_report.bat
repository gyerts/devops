python3 ^
report_generator_lib___stash_report.py ^
count_of_commits=200 ^
from_date=27/09/2016 ^
to_date=28/09/2016 ^
json_output=True ^
responsibilities_ini=responsibilities.ini ^
names_ini=names_map.ini ^
threads_count=10 ^
stash_login= ^
stash_password= ^
stash_url=https://adc.luxoft.com/stash ^
project_name="CARNET" ^
repo_slug=carnet_poc ^
branch=develop ^
stash_single_file=CARNET

PAUSE
