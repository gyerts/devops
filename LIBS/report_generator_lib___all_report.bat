python3 ^
make_two_reports.py ^
count_of_commits=200 ^
from_date=01/09/2016 ^
to_date=28/09/2016 ^
json_output=True ^
responsibilities_ini=responsibilities.ini ^
names_ini=names_map.ini ^
threads_count=10 ^
all_single_file=TOTAL ^
stash_login= ^
stash_password= ^
stash_url=https://adc.luxoft.com/stash ^
project_name="CARNET" ^
repo_slug=carnet_poc ^
branch=develop ^
stash_single_file=CARNET ^
path_to_p4=p4 ^
p4_server_port=172.30.137.2:3500 ^
p4_user= ^
p4_password= ^
p4_repository_to_parse_commits=//Toyota_Lexus_MEU_CY17/... ^
fisheye_url=https://adc.luxoft.com/fisheye ^
crusible_repo_name=Toyota_Lexus_MEU_CY17_new ^
review_prefix=CR-HTOYOTA ^
crusible_user= ^
crusible_password= ^
crusible_single_file=HTOYOTA

PAUSE
