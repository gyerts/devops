import os, sys
import msvcrt as m

from filesystem import *

# cloc "--csv --out=x" "py" "__init__.py __pycache__ multirunner.py"

def run(program, flags=None, extension_to_parse=None, names_to_exclude=None, folder_to_parse=os.getcwd()):
    file_node = parse_all_files_and_folders(folder_to_parse)

    if names_to_exclude != None:
        exclude_files_witn_name(file_node, names_to_exclude.split(' '))
        exclude_folders_witn_name(file_node, names_to_exclude.split(' '))

    if extension_to_parse != None:
        filter_by_extension(file_node, extension_to_parse.split(' '))

    file_node.show()

    all_files = file_node.get_all_files_recursively()
    cmd = '%s %s %s'%(program, flags, " ".join(all_files))
    print("--------------------------------")
    print("--------------------------------")
    print("cmd:", cmd)
    print("--------------------------------")
    print("--------------------------------")
    os.system(cmd)
    exit()

def help():
    print("""
* <> - required
* [] - optionaly

usage example:
    python3 multirunner <program> [0] [1] [2] [3]

[0] - ["flags [flag]"]

[1] - ["extension_of_file [extension_of_file]"] 
    - if not specified, all files will be choosen.

[2] - ["names_to_exclude [names_to_exclude]"] 
    - should be listed witout extension if it file name

[3] - [path_where_find_files] 
    - if not specified, current directory will be choosen


for python code:
  python multirunner cloc "--by-file-by-lang --csv --out=x" "py"

for c/c++ code:
  python multirunner cloc "--by-file-by-lang --csv --out=x" "cpp hpp c h"
    

  Press Enter to continue...
    """)
    m.getch()
    exit()

if __name__ == "__main__":
    try:
        if sys.argv[1] == '-h' or sys.argv[1] == '-help' or sys.argv[1] == '--help':
            help()
    except Exception as ex:
        help()
    
    program = sys.argv[1]

    try:
        flags = sys.argv[2]
    except Exception as ex:
        run(program)

    try:
        extension_to_parse = sys.argv[3]
    except Exception as ex:
        run(program, flags)

    try:
        folder_to_parse = sys.argv[4]
    except Exception as ex:
        run(program, flags, extension_to_parse)
    
    run(program, flags, extension_to_parse, folder_to_parse)
    