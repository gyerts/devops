from collections import OrderedDict
import os

from mocca.classes.CFile_hbsi import HBSI_file

class HBCM_file():
    def __init__(self):
        self.name = None
        self.path = None
    # этот HBCM файл использует эти HBSI файлы
        self.dict_HBSI = OrderedDict() # {'HBSI_path': HBSI_file, ...}

    def parse_root(self, absolute_path_to_file, root, path_to_project):
    # 1) set name
        start = absolute_path_to_file.rfind(os.sep)
        finish = absolute_path_to_file.rfind('.')
        self.name = absolute_path_to_file[start + 1: finish]
    # 2) set path
        self.path = absolute_path_to_file
    # 3) set using HBSI files
        for child in root:
            if 'Root' in child.tag:
                for tag in child:
                    if tag.tag == 'definedInterfaces':
                        new_hbsi = HBSI_file()
                    # 1) set path of hbsi
                        new_hbsi.path = os.path.join(path_to_project, tag.attrib['name'].replace('/', os.sep) + '.hbsi')
                    # 2) set name of hbsi
                        index = tag.attrib['name'].rfind('/')
                        new_hbsi.name = tag.attrib['name'][index + 1:]
                    # 3) set version of hbsi file
                        index = tag.attrib['{http://www.omg.org/XMI}id'].rfind('_')
                        new_hbsi.version = tag.attrib['{http://www.omg.org/XMI}id'][index + 1:-2]
                    # 4) add new hbsi to list_HBSI
                        self.dict_HBSI[new_hbsi.path] = new_hbsi
            break
