from Luxoft.base.interfaces.api import XML
from Luxoft.base.utility.config import returned_values
import webbrowser
import os
import platform

backslesh = None

if "Windows" == platform.system():
    backslesh = '\\'
else:
    backslesh = '/'



class HBSI_file():
    def __init__(self):
        self.name = None
        self.path = None
        self.version = None
    # переменная что говорит о несоответствии интерфейсов, устанавливается методом check_diffs()
        self.errors = None
    # набор HBCM файлов которые используют этот интерфейс
        self.list_HBCM = list() # [ {'HBCM_name': name, 'HBCM_path': path, 'HBCM_using_version': version}, ... ]

    def parse_root(self, path, root): # { path: root }
    # 1) set name
        start = path.rfind(backslesh)
        finish = path.rfind('.')
        self.name = path[start + 1: finish]
    # 2) set path
        self.path = path
    # 3) set version
        for child in root:
            if child.tag == 'Version':
                self.version = child[0].text + '.' + child[1].text
                break

    def check_diffs(self):
        self.errors = False
        for hbcm in self.list_HBCM:
            if self.version != hbcm['version']:
                self.errors = True
                break

        return self.errors


class HBCM_file():
    def __init__(self):
        self.name = None
        self.path = None
    # этот HBCM файл использует эти HBSI файлы
        self.dict_HBSI = dict() # {'HBSI_path': HBSI_file, ...}

    def parse_root(self, absolute_path_to_file, root, path_to_project):
    # 1) set name
        start = absolute_path_to_file.rfind(backslesh)
        finish = absolute_path_to_file.rfind('.')
        self.name = absolute_path_to_file[start + 1: finish]
    # 2) set path
        self.path = absolute_path_to_file
    # 3) set using HBSI files
        for child in root:
            if 'Root' in child.tag:
                for tag in child:
                    if tag.tag == 'definedInterfaces':
                        new_hbsi = HBSI_file()
                    # 1) set path of hbsi
                        new_hbsi.path = path_to_project + backslesh + tag.attrib['name'].replace('/', backslesh) + '.hbsi'
                    # 2) set name of hbsi
                        index = tag.attrib['name'].rfind('/')
                        new_hbsi.name = tag.attrib['name'][index + 1:]
                    # 3) set version of hbsi file
                        index = tag.attrib['{http://www.omg.org/XMI}id'].rfind('_')
                        new_hbsi.version = tag.attrib['{http://www.omg.org/XMI}id'][index + 1:-2]
                    # 4) add new hbsi to list_HBSI
                        self.dict_HBSI[new_hbsi.path] = new_hbsi
            break


class FILE_arr():
    def __init__(self):
        self.all_HBSI_files = list()
        self.all_HBCM_files = list()
        self.all_file_pathes = dict({'all_HBSI_file_pathes': list(), 'all_HBCM_file_pathes': list()})
        self.updated = False

    def add_hbsi_files(self, path_to_hbsi_files):
        self.updated = False
        self.all_file_pathes['all_HBSI_file_pathes'].append(path_to_hbsi_files)
        xml_files = XML(path_to_hbsi_files, '.hbsi').xml_files
        for path_to_hbsi in xml_files:
            new_hbsi = HBSI_file()
            new_hbsi.parse_root(path_to_hbsi, xml_files[path_to_hbsi])
            self.all_HBSI_files.append(new_hbsi)

    def add_hbcm_files(self, path_to_hbcm_files, path_to_project):
        self.updated = False
        self.all_file_pathes['all_HBCM_file_pathes'].append(path_to_hbcm_files)
        xml_files = XML(path_to_hbcm_files, '.hbcm').xml_files
        for path_to_hbcm in xml_files:
            new_hbcm = HBCM_file()
            new_hbcm.parse_root(path_to_hbcm, xml_files[path_to_hbcm], path_to_project)
            self.all_HBCM_files.append(new_hbcm)

    def join_hbcm_to_hbsi(self):
        self.updated = True
        for hbsi in self.all_HBSI_files:
            for hbcm in self.all_HBCM_files:
                try:
                    hbsi.list_HBCM.append({'name': hbcm.name, 'path': hbcm.path, 'version': hbcm.dict_HBSI[hbsi.path].version})
                except:
                    pass

    def get_diffs(self):
        if not self.updated:
            self.join_hbcm_to_hbsi()

        result = list()
        for hbsi in self.all_HBSI_files:
            if hbsi.check_diffs():
                result.append(hbsi)
        return result


class Controller():
    def __init__(self):
        print('   __init__')
        self.all_cpm_hbsi_files = None
        self.all_hmi_hbsi_files = None
        self.all_nav_hbsi_files = None
        self.path_to_project_text = None
        self.input = None
        self.all_cpm_diffs = None
        self.all_hmi_diffs = None
        self.all_nav_diffs = None
        self.path_to_project = None


    def set_input(self, inputed_text):
        self.input = inputed_text

    def update(self):
        self.sync_settings()

        self.path_to_project = returned_values.get('PROJECT_FOLDER_WITH_API')

        if not os.path.exists(self.path_to_project):
            return False

        self.all_cpm_hbsi_files = FILE_arr()

        self.all_cpm_hbsi_files.add_hbsi_files(self.path_to_project + backslesh + 'api')

        self.all_cpm_hbsi_files.add_hbcm_files(self.path_to_project + backslesh + 'imp', self.path_to_project)
        self.all_cpm_diffs = self.all_cpm_hbsi_files.get_diffs()

        self.all_nav_hbsi_files = FILE_arr()

        self.all_nav_hbsi_files.add_hbsi_files(self.path_to_project + backslesh + 'deliveries' + backslesh + 'api' + backslesh + 'nav')

        self.all_nav_hbsi_files.add_hbcm_files(self.path_to_project + backslesh + 'imp', self.path_to_project + backslesh + 'deliveries')
        self.all_nav_diffs = self.all_nav_hbsi_files.get_diffs()

        self.all_hmi_hbsi_files = FILE_arr()

        self.all_hmi_hbsi_files.add_hbsi_files(self.path_to_project + backslesh + 'imp' + backslesh + 'HMI' + backslesh + 'qdsi' + backslesh + 'interfaces' + backslesh + 'api')
        self.all_hmi_diffs = self.find_all_diffs_in_hmi()


    def render_hbsi_table(self, result):
        print('   render_hbsi_table')

        output = str()
        output += """<table border='1'>
                        <tr>
                            <th class="hbcm">HBCM</th>
                            <th class="hbcmver">HBCM version</th>
                            <th class="hbsiver">HBSI version</th>
                            <th class="hbsi">HBSI</th>
                        </tr>"""

        for hbsi in result:
            output += """<tr>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
            </tr>"""

            quantyty = len(hbsi.list_HBCM)
            if quantyty > 0:
                pass
            else:
                 output += """<tr>
                    <td class="hbcm"><b style="color: orangered">CPM manifests do not use it</b></td>
                    <td class="hbcmver"></td>
                    <td class="hbsiver">""" + hbsi.version + """</td>
                    <td class="hbsi">""" + hbsi.path[len(str(self.path_to_project)) + 1:] + """</td>
                </tr>"""

            forloop = 0
            for hbcm in hbsi.list_HBCM:
                if hbcm != None:
                    output += "<tr><td class='hbcm'>" + hbcm['path'][len(str(self.path_to_project)) + 1:] + "</td>"
                    if hbsi.version == hbcm['version']:
                        output += "<td class='hbcmver'>" + hbcm['version'] + "</td>"
                    else:
                        output += "<td class='hbcmver' style='background-color: orangered'><b>WARNING: " + hbcm['version'] + "</b></td>"
                else:
                    output += "<td colspan='2' class='hbcmver'>Not found on the workspace</td>"

                if forloop == 0:
                    output += "<td rowspan=\"" + str(quantyty) + "\" class='hbsiver'>" + hbsi.version + "</td>"
                    output += "<td rowspan=\"" + str(quantyty) + "\" class='hbsi'>" + hbsi.path[len(str(self.path_to_project)) + 1:] + "</td></tr>"
                forloop += 1

        output += '</table>'

        if len(result) == 0:
            output = None
            #self.browser.updProcessComleteSignal.emit("<span>\"" + str(self.input) + "\" not found in a project root</span>")

        return output

    def render_hbsi_hmi_dif_table(self, result): # [ { 'hbsi_of_workspace': HBSI_file, 'hbsi_of_hmi': HBSI_file }, ... ]
        print('   render_hbsi_hmi_dif_table')
        output = str()
        output += """<table border='1'>
                        <tr>
                            <th>Workspace HBSI path</th>
                            <th>Workspace version</th>
                            <th>HMI version</th>
                            <th>HMI HBSI path</th>
                        </tr>"""

        for pair_hbsi in result:
            output += """<tr>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
            </tr>"""

            forloop = 0

            if pair_hbsi['hbsi_of_workspace'] != None:
                output += "<tr><td>" + pair_hbsi['hbsi_of_workspace'].path[len(str(self.path_to_project_text)) + 1:] + "</td>"

                if pair_hbsi['hbsi_of_hmi'].version == pair_hbsi['hbsi_of_workspace'].version:
                    output += "<td>" + pair_hbsi['hbsi_of_workspace'].version + "</td>"
                else:
                    output += "<td style='background-color: orangered'><b>WARNING: " + pair_hbsi['hbsi_of_workspace'].version + "</b></td>"
            else:
                output += "<td colspan='2' style='background-color: yellow'>Not found on the workspace</td>"


            output += "<td>" + pair_hbsi['hbsi_of_hmi'].version + "</td>"
            output += "<td>" + pair_hbsi['hbsi_of_hmi'].path[len(str(self.path_to_project_text)) + 1:] + "</td></tr>"


        output += '</table>'

        return output

    def sync_settings(self):
        self.input = returned_values.get('api_to_check')
        print('   sync_settings')
        if len(self.input) > 5:
            if self.input[-5:] == '.hbsi':
                self.input = self.input[:-len('.hbsi')]

        returned_values.set('api_to_check', self.input)
        print(self.input)

    def find_cpm_in_hmi(self, founded_cpm_hbsi):
        print('   find_cpm_in_hmi')
        founded_hmi_hbsi = None
        if founded_cpm_hbsi is not None:
            for hmi_hbsi in self.all_hmi_hbsi_files.all_HBSI_files:
                if hmi_hbsi.name == founded_cpm_hbsi.name:
                    founded_hmi_hbsi = hmi_hbsi

        return founded_hmi_hbsi

    def find_hmi_in_cpm(self, founded_hmi_hbsi):
        print('   find_hmi_in_cpm')
        founded_cpm_hbsi = None
        if founded_hmi_hbsi is not None:
            for cpm_hbsi in self.all_cpm_hbsi_files.all_HBSI_files:
                if cpm_hbsi.name == founded_hmi_hbsi.name:
                    founded_cpm_hbsi = cpm_hbsi

        return founded_cpm_hbsi

    def find_hmi_in_nav(self, founded_hmi_hbsi):
        print('   find_hmi_in_nav')
        founded_nav_hbsi = None
        if founded_hmi_hbsi is not None:
            for nav_hbsi in self.all_nav_hbsi_files.all_HBSI_files:
                if nav_hbsi.name == founded_hmi_hbsi.name:
                    founded_nav_hbsi = nav_hbsi

        return founded_nav_hbsi

    def find_all_diffs_in_hmi(self):
        print('   find_all_diffs_in_hmi_')
        founded_hbsies = list()

        for hmi_hbsi in self.all_hmi_hbsi_files.all_HBSI_files:
            founded = False
            for cpm_hbsi in self.all_cpm_hbsi_files.all_HBSI_files:
                if cpm_hbsi.name == hmi_hbsi.name:
                    founded = True
                    if cpm_hbsi.version != hmi_hbsi.version:
                        founded_hbsies.append({'hbsi_of_workspace': cpm_hbsi, 'hbsi_of_hmi': hmi_hbsi})
                    break

            for nav_hbsi in self.all_nav_hbsi_files.all_HBSI_files:
                if nav_hbsi.name == hmi_hbsi.name:
                    founded = True
                    if nav_hbsi.version != hmi_hbsi.version:
                        founded_hbsies.append({'hbsi_of_workspace': nav_hbsi, 'hbsi_of_hmi': hmi_hbsi})
                    break

            #if not founded:
                #founded_hbsies.append({'hbsi_of_workspace': None, 'hbsi_of_hmi': hmi_hbsi})

        return founded_hbsies

    def show_diffs(self):
        self.update()
        print('-------------show_diffs-------------')

        self.sync_settings()
        ans = {'CPM':None, 'NAVI':None, 'HMI':None}

        if self.all_cpm_diffs is not None:
            if len(self.all_cpm_diffs) > 0:
                ans['CPM'] = self.all_cpm_diffs

        if self.all_nav_diffs is not None:
            if len(self.all_nav_diffs) > 0:
                ans['NAVI'] = self.all_nav_diffs

        # find hmi diffs
        if self.all_hmi_diffs is not None:
            if len(self.all_hmi_diffs) > 0:
                ans['HMI'] = self.all_hmi_diffs

        return ans

    def show_interface(self):
        self.update()
        print('-------------show_interface-------------')
        try:
            self.sync_settings()

            html = list()
            founded_workspace_hbsi = None

            html += '<h1>Project path: ' + self.path_to_project + '</h1>'

            for hbsi in self.all_cpm_hbsi_files.all_HBSI_files:
                if hbsi.name.lower() == self.input.lower():
                    html += '<h1>Detected as CPM interface</h1>'
                    html += self.render_hbsi_table([hbsi])
                    founded_workspace_hbsi = hbsi
                    break


            if founded_workspace_hbsi is None:
                for hbsi in self.all_nav_hbsi_files.all_HBSI_files:
                    if hbsi.name.lower() == self.input.lower():
                        html += '<h1>Detected as NAVI interface</h1>'
                        html += self.render_hbsi_table([hbsi])
                        founded_workspace_hbsi = hbsi
                        break

            if founded_workspace_hbsi is None:
                html = '<h1>CPM do not uses this interface: ' + returned_values.get('api_to_check') + '</h1>'


            if founded_workspace_hbsi is not None:
                founded_hmi_hbsi = self.find_cpm_in_hmi(founded_workspace_hbsi)
                if founded_hmi_hbsi is not None:
                    html += '<h1>HMI uses this intrface</h1>'
                    html += self.render_hbsi_hmi_dif_table([{'hbsi_of_workspace': founded_workspace_hbsi, 'hbsi_of_hmi': founded_hmi_hbsi}])
            if html is not None:
                return html
            else:
                return '<span style="color: red">\'' + str(self.input) + '\' not found</span>'

        except Exception as ex:
            if str(ex) == "'NoneType' object has no attribute 'all_HBSI_files'":
                return '<span style="color: red">first you need update file tree</span> press green button'
            else:
                return str(ex)

    def show_all(self):
        self.update()
        print('-------------show_all------------')
        try:
            self.sync_settings()

            answer = dict()

            if self.all_cpm_hbsi_files.all_HBSI_files is not None:
                answer['CPM interfaces'] = self.all_cpm_hbsi_files.all_HBSI_files

            if self.all_nav_hbsi_files.all_HBSI_files is not None:
                answer['NAVI interfaces'] = self.all_nav_hbsi_files.all_HBSI_files

            list_of_hmi_hbsi = list() #[{'hbsi_of_workspace': founded_cpm_hbsi, 'hbsi_of_hmi': founded_hmi_hbsi}]

            for hmi_hbsi in self.all_hmi_hbsi_files.all_HBSI_files:
                founded_workspace_hbsi = self.find_hmi_in_cpm(hmi_hbsi)
                if founded_workspace_hbsi is None:
                    founded_workspace_hbsi = self.find_hmi_in_nav(hmi_hbsi)

                if founded_workspace_hbsi is not None:
                    list_of_hmi_hbsi.append({'hbsi_of_workspace': founded_workspace_hbsi, 'hbsi_of_hmi': hmi_hbsi})
                else:
                    list_of_hmi_hbsi.append({'hbsi_of_workspace': None, 'hbsi_of_hmi': hmi_hbsi})

            answer['HMI interfaces'] = list_of_hmi_hbsi

            return answer

        except Exception as ex:
            if str(ex) == "'NoneType' object has no attribute 'all_HBSI_files'":
                return '<span style="color: red">first you need update file tree</span> press green button'
            else:
                return str(ex)