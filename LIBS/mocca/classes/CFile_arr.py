from mocca.classes.CXML import XML
from mocca.classes.CFile_hbsi import HBSI_file
from mocca.classes.CFile_HBCM import HBCM_file

class FILE_arr():
    def __init__(self):
        self.all_HBSI_files = list()
        self.all_HBCM_files = list()
        self.all_file_pathes = dict({'all_HBSI_file_pathes': list(), 'all_HBCM_file_pathes': list()})
        self.updated = False

    def add_hbsi_files(self, path_to_hbsi_files):
        self.updated = False
        self.all_file_pathes['all_HBSI_file_pathes'].append(path_to_hbsi_files)
        xml_files = XML(path_to_hbsi_files, '.hbsi').xml_files
        for path_to_hbsi in xml_files:
            new_hbsi = HBSI_file()
            new_hbsi.parse_root(path_to_hbsi, xml_files[path_to_hbsi])
            self.all_HBSI_files.append(new_hbsi)

    def add_hbcm_files(self, path_to_hbcm_files, path_to_project):
        self.updated = False
        self.all_file_pathes['all_HBCM_file_pathes'].append(path_to_hbcm_files)
        xml_files = XML(path_to_hbcm_files, '.hbcm').xml_files
        for path_to_hbcm in xml_files:
            new_hbcm = HBCM_file()
            new_hbcm.parse_root(path_to_hbcm, xml_files[path_to_hbcm], path_to_project)
            self.all_HBCM_files.append(new_hbcm)

    def join_hbcm_to_hbsi(self):
        self.updated = True
        for hbsi in self.all_HBSI_files:
            for hbcm in self.all_HBCM_files:
                try:
                    hbsi.list_HBCM.append({'name': hbcm.name, 'path': hbcm.path, 'version': hbcm.dict_HBSI[hbsi.path].version})
                except:
                    pass

    def get_diffs(self):
        if not self.updated:
            self.join_hbcm_to_hbsi()

        result = list()
        for hbsi in self.all_HBSI_files:
            if hbsi.check_diffs():
                result.append(hbsi)

        return result