import os
import xml.etree.ElementTree as ET

class XML():
    def __init__(self, path, extention):
        self.xml_files = dict()
        self.path = path
        self.extention = extention
        self.find_all_xml_files(self.path)

    def init(self, exists_dict):
        self.xml_files = exists_dict

    def find_all_xml_files(self, path):
        files = os.listdir(path)
        for file in files:
            full_name_of_file = os.path.join(path, file)
            if os.path.isfile(full_name_of_file):
                filename, file_extension = os.path.splitext(file)
                if self.extention == file_extension:
                    tree = ET.parse(full_name_of_file)
                    self.xml_files[full_name_of_file] = tree.getroot()
            else:
                self.find_all_xml_files(os.path.join(path, file))

    def get(self, list_of_behaviours):
        output = list()
        for none in self.xml_files:
            output.append(list())
        for behaviour in list_of_behaviours:
            counter = 0
            for file in self.xml_files:
                output[counter].append(behaviour.get(file, self.xml_files[file]))
                counter += 1
        return output