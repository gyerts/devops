class HBSM_use_HBSI():
    def __init__(self, all_hbsm_files, path_to_project):
        self.all_hbsm_files = all_hbsm_files.xml_files
        self.path_to_project = path_to_project

    def get(self, name, root):
        name = name[len(self.path_to_project) + 1:-5].replace('\\', '/')
        result = list()
        for hbsm in self.all_hbsm_files:
            for child in self.all_hbsm_files[hbsm]:
                if 'Root' in child.tag:
                    for tag in child:
                        if tag.tag == 'definedInterfaces':
                            all = name.rfind('/')
                            all = name[all + 1:]
                            ins = tag.attrib['name'].rfind('/')
                            ins = tag.attrib['name'][ins + 1:]
                            if all == ins:
                                i = tag.attrib['{http://www.omg.org/XMI}id'].rfind('_')
                                ver = tag.attrib['{http://www.omg.org/XMI}id'][i + 1:]
                                result.append([hbsm[len(self.path_to_project + r'\imp') + 1:], ver])
                break
        return result