from mocca.classes.CController import Controller

def find(path_to_project, api_to_check):
    controller = Controller()
    ans = controller.show_interface(path_to_project, api_to_check)
    return ans

def show_all_errors(path_to_project, workspace):

    ans = controller.show_diffs(workspace)

    CPM = ans['CPM']
    HMI = ans['HMI']
    NAVI = ans['NAVI']

    response_dict = dict()
    response_dict['CPM'] = CPM
    response_dict['HMI'] = HMI
    response_dict['NAVI'] = NAVI
    response_dict['to_cut'] = path_to_project
    return render_to_response('interfaces/all_errors.html', response_dict)


def show_all_interfaces(path_to_project, workspace):
    try:
        ans = controller.show_all(workspace)

        CPM = ans['CPM interfaces']
        HMI = ans['HMI interfaces']
        NAVI = ans['NAVI interfaces']

        response_dict = dict()
        response_dict['CPM'] = CPM
        response_dict['HMI'] = HMI
        response_dict['NAVI'] = NAVI
        response_dict['to_cut'] = path_to_project
        return render_to_response('interfaces/all.html', response_dict)

    except Exception as ex:
        print(str(ex))




print(find('/home/ygyerts/MEUCY17/WORK', 'DModeManager.hbsi'))
