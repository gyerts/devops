
#to use this script you should install:
# sudo apt-get install libssl-dev
# sudo pip3 install paramiko

import os, sys

from report_generator_lib.trs_tse_report.TrsTseReport import TrsTseReport
from report_generator_lib.report_views.uni_report_view import uniReportView
from threading import Thread


def foo():
    path = saveReportAs
    uniReportView(x.features, path)


if __name__ == '__main__':
	dr = sys.argv[1]
	path_to_project = sys.argv[2]
	path_to_artifact = sys.argv[3]
	x = TrsTseReport()
	x.trsListPath = os.path.join(path_to_project, 'config', 'test_coverage', 'all_trs_list.txt')

	# -----------------------------------------------
	# -----------------------------------------------
	x.trsFilter = ""
	# -----------------------------------------------
	# -----------------------------------------------

	x.clientDir = path_to_project

	#binaries path from where binareies fill be taken
	x.productsDir = path_to_project + "_products"
	x.targetIp = "10.67.68.63"
	x.targetPort = "22"
	x.targetUser = "root"
	x.targetPassword = "root"
	x.targetBinDir = "/usr/bin"
	x.preExecSettings = "export MALLOC_CHECK_=0 && "
	x.getHbtsOption = True
	x.getHbcmOption = True
	x.getBinOption = True
	x.execTestsOption = True


	path_where_report_should_place = os.path.join(path_to_artifact, 'Generated_Reports')
	if not os.path.exists(path_where_report_should_place):
	    os.makedirs(path_where_report_should_place)

	saveReportAs = os.path.join(path_where_report_should_place, 'tse_tests_results_CL%s.xlsx' % dr)
	print('<h1>', saveReportAs, '</h1>')

	x.endCall = foo

	Thread(target = x.start).start()
