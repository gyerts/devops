python3 ^
report_generator_lib___crusible_report.py ^
count_of_commits=200 ^
from_date=27/09/2016 ^
to_date=28/09/2016 ^
json_output=True ^
responsibilities_ini=responsibilities.ini ^
names_ini=names_map.ini ^
threads_count=10 ^
path_to_p4=p4 ^
p4_server_port=172.30.137.2:3500 ^
p4_user= ^
p4_password= ^
p4_repository_to_parse_commits=//Toyota_Lexus_MEU_CY17/... ^
fisheye_url=https://adc.luxoft.com/fisheye ^
crusible_repo_name=Toyota_Lexus_MEU_CY17_new ^
review_prefix=CR-HTOYOTA ^
crusible_user= ^
crusible_password= ^
crusible_single_file=HTOYOTA

PAUSE
