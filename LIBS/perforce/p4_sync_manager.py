import os

class P4SyncManager:
    def __init__(self, absolute_path_to_sync_manager):
        self.path_to_sync_manager = absolute_path_to_sync_manager

    def sync_to_head_revision_p4sm(self, client, workspace, path_to_p4sm, server, port, user):
        cmd = self.path_to_sync_manager + \
              ' -d ' + path_to_p4sm + \
              ' -p ' + server + ':' + port + \
              ' -u ' + user + \
              ' -c ' + client + \
              ' -r ' + workspace
        os.system(cmd)
