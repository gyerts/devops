import os, subprocess
import re
import platform


def run(cmd):
    proc = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return proc


class Submit:
    def __init__(self):
        self.cl = None
        self.description = None
        self.owner = None
        self.workspace = None


class P4Cover:
    def __init__(self, path_to_p4, user, password, ip_port):
        self.__path_to_p4 = path_to_p4
        self.__user = user
        self.__password = password
        self.__ip_port = ip_port
        self.__start_command = self.__path_to_p4 + ' -u ' + self.__user + ' -p ' + self.__ip_port

        self.sh = "<<path_to_p4>> -p <<ip>>:<<port>> login <<password>>"\
            .replace('<<user>>', user)\
            .replace('<<password>>', password)\
            .replace('<<path_to_p4>>', path_to_p4)\
            .replace('<<ip_port>>', path_to_p4)\

        self.__connect()

    def __connect(self):
        pass_txt = "pass.txt"
        cmd = "set P4PORT=&set P4PASSWD=& set P4USER=<<user>>& set USERNAME=<<user>>& p4 -p <<ip_port>> login < <<file_with_credentials>>"
        os.system("echo {}>{}".format(self.__password, pass_txt))
        if platform.system() != "Windows":
            cmd = cmd.replace("&", ";")
        cmd = cmd.replace("<<user>>", self.__user)
        cmd = cmd.replace("<<ip_port>>", self.__ip_port)
        cmd = cmd.replace("<<file_with_credentials>>", pass_txt)
        os.system(cmd)
        os.remove(pass_txt)

    def get_info_of_latest_submits(self, depo_path, quantity):
        depo_path = self.__get_correct_depot_path(depo_path)

        cmd = self.__start_command + ' changes -m ' + str(quantity) + ' ' + depo_path
        os.system(cmd)
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        cl = 'Change ([\d]*) on '
        owner = 'Change [\d]* on [\d/]* by ([\w]*)@'
        workspace = 'Change [\d]* on [\d/]* by [\w]*@([\w-]*)'
        description = 'Change [\d]* on [\d/]* by [\w]*@[\w-]*([^^]*)##TypeOfChange#'

        submits = list()
        text_blocks = list()

        block = None
        for line in iter(process.stdout.readline, ""):
            if line == b'':
                break

            try:
                line = line.strip().decode()
            except Exception as ex:
                line = line.strip().decode('cp1251')
                print(line)

            try:
                re.search(cl, line).group(1)
                if block != None:
                    text_blocks.append(block)
                block = str()
            except:
                pass

            finally:
                block += line

        text_blocks.append(block)

        for block in text_blocks:
            submit = Submit()
            submit.cl = re.search(cl, block).group(1).strip()
            #submit.description = re.search(description, block).group(1).strip()
            submit.owner = re.search(owner, block).group(1).strip()
            submit.workspace = re.search(workspace, block).group(1).strip()
            submits.append(submit)
        return submits

    def sync_depo_path_to_cl(self, client, cl, depo_path):
        depo_path = self.__get_correct_depot_path(depo_path)
        cmd = self.__start_command + ' -c ' + client + ' sync ' + depo_path + '@' + str(cl)
        proc = run(cmd)
        return proc

    def mark_for_delete(self, client, file_path):
        cmd = self.__start_command + ' -c ' + client + ' delete -v ' + file_path
        print('start mark files for delete, please wait...')
        proc = run(cmd)
        return proc

    def checkout(self, client, depot_path):
        cmd = self.__start_command + ' -c ' + client + ' edit ' + depot_path
        print('start checkout files, please wait...')
        proc = run(cmd)
        proc.stdout.read()
        errors = proc.stderr.read().split('\n')
        deleted_files = list()
        for error in errors:
            if 'No such file or directory' in error:
                m = re.search('chmod: ([^^]*): No such file or directory', error)
                deleted_files.append(m.group(1).strip())
        return deleted_files

    def mark_for_add(self, client, depot_path):
        cmd = self.__start_command + ' -c ' + client + ' add ' + depot_path
        print('start mark files for add, please wait...')
        proc = run(cmd)
        output = proc.stdout.read().split('\n')

        added_files = list()

        if len(output):
            for line in output:
                if 'opened for add' in line:
                    m = re.search('([^^]*)#[\d]* - opened for add', line)
                    added_files.append(m.group(1))
        return added_files

    def revert_unchanged_files(self, client, depot_path):
        cmd = self.__start_command + ' -c ' + client + ' revert -a ' + depot_path
        print('start revert unchanged files, please wait...')
        proc = run(cmd)
        output = proc.stdout.read().split('\n')
        #print('<b>', len(output), '</b> has been reverted')
        return proc

    def submit(self, client, description):
        cmd = self.__start_command + ' -c ' + client + ' submit --parallel=0 -f leaveunchanged -d "' + description + '"'
        print('start submit files, please wait...')
        proc = run(cmd)
        return proc

    def show_all_files_in_default_cl(self, client, depot_path):
        depot_path = depot_path.replace('...', '')

        cmd = self.__start_command + ' -c ' + client + ' opened -c default'
        proc = run(cmd)
        output = proc.stdout.read().split('\n')

        for line in output:
            cat_dies = line.rfind('#')
            line = line[:cat_dies]
            cat_slash = line.rfind('/')
            line = line[0:len(depot_path)] + '<b style="color: orange">' + line[len(depot_path):cat_slash] + '</b>' + line[cat_slash:]
            print(line)
        return proc

    def execute(self, line):
        cmd = self.__start_command + ' ' + line
        proc = run(cmd)
        proc.wait()

        print('<h3 style="color:red">', proc.stderr.read(), '</h3>')

        return proc

    def get_latest_CL_of_path(self, path):
        path = self.__get_correct_depot_path(path)
        proc = self.execute('changes -m 10 -s submitted ' + path)
        output = proc.stdout.read()

        cl = re.search('Change ([\d]*) on', output).group(1).strip()
        return int(cl)

    def sync_to_head_revision(self, client, depot_path):
        CL = self.get_latest_CL_of_path(depot_path)
        self.sync_depo_path_to_cl(client, CL, depot_path)

        return int(CL)

    def __get_correct_depot_path(self, depo_path):
        if '/...' not in depo_path:
            if depo_path[-1] != '/': depo_path += '/...'
            else: depo_path += '...'
        return depo_path
