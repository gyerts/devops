import os, time

def get_all_files(path, extension):
    if os.path.exists(path):
        file_elements = os.listdir(path)
        all_files = dict()
        for file in file_elements:
            full_path = os.path.join(path, file)
            if os.path.isfile(full_path):
                if file[-3:] == ('.' + extension) and '__init__' not in file:
                    all_files[file] = full_path
        return all_files
    else: return {}

    
    
    
def filter_by_extension(file_node, list_extension_to_parse):
    """
    file_node                 - filesystem.Folder   
    # https://adc.luxoft.com/stash/projects/LUXTOOLS/repos/iwa/browse/iwa/Engine/classes/filesystem.py
    list_extension_to_parse   - list(str)
    """
    print("\n[TODO]: Removing from list files which one NOT is: %s"%str(list_extension_to_parse))
    __filter_by_extension(file_node, list_extension_to_parse)
    
def __filter_by_extension(file_node, list_extension_to_parse, tab=" . "):
    """
    file_node                 - filesystem.Folder   
    # https://adc.luxoft.com/stash/projects/LUXTOOLS/repos/iwa/browse/iwa/Engine/classes/filesystem.py
    list_extension_to_parse   - list(str)
    tab                       - str
    """
    files_to_delete = list()

    for file in file_node.files:
        if file.extension not in list_extension_to_parse:
            files_to_delete.append(file)

    for file_to_delete in files_to_delete:
        print(tab+"[x] "+file_to_delete.full_name)
        file_node.delete_file(file_to_delete)

    for folder in file_node.folders:
        __filter_by_extension(folder, list_extension_to_parse, tab+" . ")
    

    
    
    
def exclude_files_witn_name(file_node, list_of_names_to_exclude):
    """
    file_node                 - filesystem.Folder   
    # https://adc.luxoft.com/stash/projects/LUXTOOLS/repos/iwa/browse/iwa/Engine/classes/filesystem.py
    list_of_names_to_exclude  - list(str)
    """
    print("\n[TODO]: Removing from list next files: %s"%str(list_of_names_to_exclude))
    __exclude_files_witn_name(file_node, list_of_names_to_exclude)


def __exclude_files_witn_name(file_node, list_of_names_to_exclude, tab=" . "):
    """
    file_node                 - filesystem.Folder   
    # https://adc.luxoft.com/stash/projects/LUXTOOLS/repos/iwa/browse/iwa/Engine/classes/filesystem.py
    list_of_names_to_exclude  - list(str)
    tab                       - str
    """
    files_to_delete = list()

    for file in file_node.files:
        if file.full_name in list_of_names_to_exclude:
            files_to_delete.append(file)

    for file_to_delete in files_to_delete:
        print(tab+"[x] "+file_to_delete.full_name)
        file_node.delete_file(file_to_delete)

    for folder in file_node.folders:
        __exclude_files_witn_name(folder, list_of_names_to_exclude, tab+" . ")
    
    
    
def exclude_folders_witn_name(file_node, list_of_names_to_exclude):
    """
    file_node                 - filesystem.Folder   
    # https://adc.luxoft.com/stash/projects/LUXTOOLS/repos/iwa/browse/iwa/Engine/classes/filesystem.py
    list_of_names_to_exclude  - list(str)
    """
    print("\n[TODO]: Removing from list next folders: %s"%str(list_of_names_to_exclude))
    __exclude_folders_witn_name(file_node, list_of_names_to_exclude)


def __exclude_folders_witn_name(file_node, list_of_names_to_exclude, tab=" . "):
    """
    file_node                 - filesystem.Folder   
    # https://adc.luxoft.com/stash/projects/LUXTOOLS/repos/iwa/browse/iwa/Engine/classes/filesystem.py
    list_of_names_to_exclude  - list(str)
    tab                       - str
    """
    folders_to_delete = list()

    for folder in file_node.folders:
        if folder.name in list_of_names_to_exclude:
            folders_to_delete.append(folder)

    for folder_to_delete in folders_to_delete:
        print(tab+"[x] "+folder_to_delete.name)
        file_node.delete_folder(folder_to_delete)

    for folder in file_node.folders:
        __exclude_folders_witn_name(folder, list_of_names_to_exclude, tab+" . ")
    
    
    
    
    
    
    
    
    
    
class Info:
    def __init__(self, path):
        try:
            (mode, ino, dev, nlink, uid, gid, size,
             self.access_time, self.modified_time, self.created_time) = os.stat(path)
        except Exception as ex:
            print('exception', ex)

class File:
    def __init__(self, path):
        index          = path.rfind(os.sep)
        self.full_name = path[index + 1:]
        self.name      = self.full_name
        
        self.path      = path
        self.info      = Info(path)
        self.extension = ''
        
        if '.' in self.name:
            index  = self.name.rfind('.')
            self.name      = self.full_name[0:index]
            self.extension = self.full_name[index+1:]

    def is_same(self, file):
        """ 'file' parametr - could receive: 'str' and 'filesystem.File' types """
        status = False
        
        if isinstance(file, str):
            if '.' in file:
                index     = file.rfind('.')
                name      = file[0:index]
                extension = file[index+1:]
            else:
                name = file
                extension = ''
            
            if self.name == name and self.extension == extension:
                status = True
        
        elif isinstance(file, File):
            if file.full_name == self.full_name and \
                file.name == self.name and \
                file.path == self.path and \
                file.info == self.info and \
                file.extension == self.extension:
                status = True
        
        else:
            print("[ERROR]: unknown type: %s"%str(type(file)))
        
        return status

        
class Folder:
    def __init__(self, path):
        index = path.rfind(os.sep)
        self.name = path[index + 1:]
        self.path = path
        self.info = Info(path)

        self.files = list()
        self.folders = list()

    def add_file(self, objFile):
        self.files.append(objFile)

    def add_folder(self, objFolder):
        self.folders.append(objFolder)
    
    def delete_file(self, objFile):
        for file in self.files:
            if file.is_same(objFile):
                self.files.remove(file)
                break
    
    def is_same(self, folder):
        """ 'folder' parametr - could receive: 'str' and 'filesystem.Folder' types """
        status = False
        
        if isinstance(folder, str):
            if self.name == folder:
                status = True
        elif isinstance(folder, Folder):
            if folder.name == self.name and \
                folder.path == self.path and \
                folder.info == self.info:
                status = True
        else:
            print("[ERROR]: unknown type: %s"%str(type(folder)))
        
        return status

    def delete_folder(self, objFolder):
        for folder in self.folders:
            if folder.is_same(objFolder):
                self.folders.remove(folder)
                break
    
    def show(self):
        print("\n[TODO]: Show all existing files in list")
        self.__show(file_node=self)
    def __show(self, file_node, tab=" . "):
        for file in file_node.files:
            print(tab+"[v] "+file.full_name)

        for folder in file_node.folders:
            file_node.__show(folder, tab+" . ")
    
    def get_all_files_recursively(self):
        return self.__get_all_files_recursively(self)
    def __get_all_files_recursively(self, file_node):
        output = list()
        for file in file_node.files:
            output.append(file.path)
        for folder in file_node.folders:
            output += file_node.__get_all_files_recursively(folder)
        return output

def parse_all_files_and_folders(path):
    output_folder = Folder(path)

    if os.path.exists(path):
        file_elements = os.listdir(path)
        for file in file_elements:
            full_path = os.path.join(path, file)
            if os.path.isfile(full_path):
                output_folder.add_file(File(full_path))
            else:
                output_folder.add_folder(parse_all_files_and_folders(full_path))

    return output_folder