from report_generator_lib.stash_report.StashReport import generate_stash_review_report
import sys, getopt

rule = """
    Date format should be next: "dd/mm/yyyy"

    stash_report.py
       path_to_stash= <path to stash>
       project_name= <project name>
       repo_slug= <repository slug>
       branch= <branch>
       responsibilities_ini= <path to responsibilities.ini file>
       count_of_commits= <count of commits>
       name_without_extension= <name of file without extension>
       from_date= <from_Date>
       to_date= <to_Date>
       json_output= <do you want create .json file too?>
"""

"""
count_of_commits=200 ^
from_date=27/09/2016 ^
to_date=28/09/2016 ^
json_output=True ^
responsibilities_ini=responsibilities.ini ^
names_ini=names_map.ini ^
threads_count=10 ^
stash_login=AVasilkiv ^
stash_password=AVasilkyv22_90_7 ^
stash_url=https://adc.luxoft.com/stash ^
project_name=CARNET ^
repo_slug=carnet_poc ^
branch=develop ^
stash_single_file=CARNET
"""


def run(
    count_of_commits,
    from_date,
    to_date,
    json_output,
    responsibilities_ini,
    names_ini,
    threads_count,
    stash_login,
    stash_password,
    stash_url,
    project_name,
    repo_slug,
    branch,
    stash_single_file
):
    changes = generate_stash_review_report(
        count_of_commits=count_of_commits,
        from_date=from_date,
        to_date=to_date,
        json_output=json_output,
        responsibilities_ini=responsibilities_ini,
        names_ini=names_ini,
        threads_count=threads_count,
        stash_login=stash_login,
        stash_password=stash_password,
        stash_url=stash_url,
        project_name=project_name,
        repo_slug=repo_slug,
        branch=branch,
        stash_single_file=stash_single_file
    )
    return changes


if __name__ == "__main__":
    long = [
             "stash_url=",
             "project_name=",
             "repo_slug=",
             "branch=",
             "responsibilities_ini=",
             "count_of_commits",
             "names_ini=",
             "stash_single_file=",
             "from_date=",
             "to_date=",
             "threads_count=",
             "json_output="
         ]


    try:
        opts, args = getopt.getopt(sys.argv[1:], "", long)

    except getopt.GetoptError as opt:
        print(opt)
        print(rule)
        sys.exit(2)

    # convert [ ('', ''), ... ] => { '': '', ... }
    opts = dict()
    for arg in args:
        l = arg.split("=")
        opts[l[0]] = l[1]

    try:
        run(
            stash_url=opts["stash_url"],
            project_name=opts["project_name"],
            repo_slug=opts["repo_slug"],
            branch=opts["branch"],
            stash_login=opts["stash_login"],
            stash_password=opts["stash_password"],
            responsibilities_ini=opts["responsibilities_ini"],
            count_of_commits=opts["count_of_commits"],
            names_ini=opts["names_ini"],
            stash_single_file=opts["stash_single_file"],
            from_date=opts["from_date"],
            to_date=opts["to_date"],
            threads_count=int(opts["threads_count"]),
            json_output=True if "True" == opts["json_output"] else False
        )

    except KeyError as key:
        print("no parameter {}, please use next pattern:\n{}".format(key.args[0], rule))
        raise
