def list_to_html(array, header=None):
    i = 0
    j = 0

    output_html = str()

    if header:
        output_html += '<h1>' + header + '</h1>'

    output_html += '<table border="1" style="width:100%">'
    for line in array:
        j = 0
        output_html += '<tr>'
        for el in line:
            if (i == 0 or j == 0) and (j != 0 or i != 0):
                output_html += '<td style="padding:3px; background-color:#088A5E"><b>' + str(el) + '</b></td>'
            else:
                output_html += '<td style="padding:3px;">' + str(el) + '</td>'
            j += 1
        output_html += '</tr>'
        i += 1
    output_html += '</table>'
    return output_html
