#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    # default realization
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "WEB_AUTOMATIZATION.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

