import re


def get_match(expression, test_str):
    regex = re.compile(expression)
    result = regex.match(test_str)
    return result.group(1)
