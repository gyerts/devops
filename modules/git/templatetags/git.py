import os
from django import template
from django.template.defaultfilters import stringfilter
from .. import config
from ..libs import git_branch
from ..libs import git_configuration


register = template.Library()


@register.filter(name='in_list')
@stringfilter
def in_list(what, path):
    result = False
    for MODULE in config.OPEN_MODULES:
        if MODULE in path:
            return True
    return result


@register.filter(name='branches')
@stringfilter
def branches(what, path):
    path_to_submodule = os.path.join(config.PATH_TO_PROJECT, path)
    all_branches = git_branch.get_list_of_branches(path_to_submodule)
    return {br.lstrip("* ") for br in all_branches}


@register.filter(name='username')
@stringfilter
def username(what):
    return git_configuration.get_user_name(config.PATH_TO_PROJECT)


@register.filter(name='current_branch')
@stringfilter
def get_current_branch(what, path):
    path_to_repository = os.path.join(config.PATH_TO_PROJECT, path)
    return git_branch.get_current_branch(path_to_repository)
