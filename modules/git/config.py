OPEN_MODULES = [
    "wnpp/networking",
    "wnpp/wifi",
    "wnpp/phconnmng",
    "wnpp/pso",
    "wnpp/wnpp_common",
    "api_wnpp",
    "/bluetooth",
    "api_bt",
    "bluetooth",
    "iscpipe",
    "diag",
    "sdk",
    "devmng"
]
PATH_TO_PROJECT = "/home/gyerts/Desktop/git/main"
