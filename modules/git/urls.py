from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'update', views.update, name="update_all_submodules"),
    url(r'checkout_branch', views.checkout_branch, name="checkout_branch"),
    url(r'reset_main_project', views.reset_main_project, name="reset_main_project"),
    url(r'make_clean', views.make_clean, name="make_clean"),
    url(r'^', views.list_of_submodules, name="list_of_submodules"),
]
