from django.shortcuts import render, redirect, render_to_response

from Engine.classes.config import variables
from Engine.classes.login.login import *

from .libs import git_fetch
from .libs import git_submodule
from .libs import git_branch
from .libs import make

from . import config
import json
import os


def list_of_submodules(request):
    if not users.logged_in(request): return get_login_page(request)

    response_dict = dict()
    response_dict['vars'] = variables
    response_dict['request'] = request
    response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))
    response_dict['main_repository'] = config.PATH_TO_PROJECT
    response_dict['submodules'] = git_submodule.get_submodules(config.PATH_TO_PROJECT)

    return render_to_response('git/submodules.html', response_dict)


def update(request):
    if not users.logged_in(request): return get_login_page(request)

    git_fetch.update_repository(config.PATH_TO_PROJECT)
    return redirect('list_of_submodules')


def checkout_branch(request):
    if not users.logged_in(request): return get_login_page(request)

    if request.method == 'POST':
        json_data = json.loads(request.body.decode("utf-8"))

        path_to_repository = os.path.join(config.PATH_TO_PROJECT, json_data["path_to_repository"])
        branch = json_data["branch"]

        print(json_data)
        git_branch.change_branch(
            path_to_repository=path_to_repository,
            branch=branch
        )

    return redirect('list_of_submodules')


def reset_main_project(request):
    if not users.logged_in(request): return get_login_page(request)
    
    git_branch.reset(config.PATH_TO_PROJECT)
    return redirect('list_of_submodules')


def make_clean(request):
    if not users.logged_in(request): return get_login_page(request)

    if request.method == 'POST':
        json_data = json.loads(request.body.decode("utf-8"))

        path_to_repository = os.path.join(config.PATH_TO_PROJECT, json_data["path_to_repository"])

        print(json_data)
        make.clean(
            path_to_project=path_to_repository
        )

    return redirect('list_of_submodules')
