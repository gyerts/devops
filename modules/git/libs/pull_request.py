import requests

REST_API_APPENDIX = "/rest/api/1.0"


class PullRequest:
    def __init__(self, pull_request_path):
        ######################################################################
        ############################## link guard ############################
        ######################################################################
        if not pull_request_path.startswith("http"):
            raise Exception("The pull-request path should start with http/s, but it starts with: " + pull_request_path)

        if REST_API_APPENDIX in pull_request_path:
            pull_request_path = pull_request_path.replace(REST_API_APPENDIX, '')

        if pull_request_path.endswith("/overview"):
            pull_request_path = pull_request_path.rstrip("/overview")
        ######################################################################
        ######################################################################


        self.path_to_stash = pull_request_path.split("/projects")[0]
        self.rest_url = self.make_rest_link(pull_request_path)
        r = requests.get(self.rest_url, auth=('yuriy', 'ufuzil12'))
        json_ans = r.json()

        if "errors" in json_ans:
            for error in json_ans["errors"]:
                print(error["message"])
            exit(1)

        self.path = pull_request_path
        self.repository_slug = json_ans["toRef"]["repository"]["slug"]
        self.project = json_ans["toRef"]["repository"]["project"]["key"]
        self.from_branch = json_ans["fromRef"]["displayId"]
        self.to_branch = json_ans["toRef"]["displayId"]
        self.state = json_ans["state"]
        self.owner_email = json_ans["author"]["user"]["emailAddress"]
        self.description = json_ans["description"] if "description" in json_ans else ""
        self.latest_commit_fromRef = json_ans["fromRef"]["latestCommit"]
        self.latest_commit_toRef = json_ans["toRef"]["latestCommit"]
        self.owner = {
            "displayName": json_ans["author"]["user"]["displayName"],
            "emailAddress": json_ans["author"]["user"]["emailAddress"]
        }

        self.reviewers = dict()
        for reviewer in json_ans["reviewers"]:
            self.reviewers[reviewer["user"]["emailAddress"]] = {
                "displayName": reviewer["user"]["displayName"],
                "approved": reviewer["approved"]
            }

        self.dependencies = self.get_dependencies()


    def get_dependencies(self):
        start = False
        dependencies = list()
        if "## Dependencies:" in self.description:
            for line in self.description.split("\n"):
                if line == "## Dependencies:":
                    start = True
                    continue
                if start:
                    if "## " not in line:
                        if line != '':
                            dependencies.append(line.lstrip("*").strip())
                    else:
                        break
        return dependencies

    def __str__(self):
        return """        
        ---------------------------------------------------
        - pull_request_path: {}
        ---------------------------------------------------
        - project:             {}
        - repository_slug:     {}
        - from branch:         {}
        - to branch:           {}
        - state:               {}
        - description:         {}
        - latest_commit from:  {}
        - latest_commit to:    {}
        - reviewers:           {}
        - owner:               {}
        ---------------------------------------------------
        """.format(self.path, self.project, self.repository_slug,
                   self.from_branch, self.to_branch, self.state, self.description.replace("\n", "; "),
                   self.latest_commit_fromRef, self.latest_commit_toRef,
                   str([self.reviewers[reviewer]["displayName"] for reviewer in self.reviewers]),
                   self.owner
        )

    def __call__(self, *args, **kwargs):
        return PullRequest(args[0])

    def make_rest_link(self, link):
        link = link.replace(self.path_to_stash, "")
        link = self.path_to_stash + REST_API_APPENDIX + link
        return link
