from modules.cmd.shell import execute

branch_to_merge = "IWA/MERGE/BRANCH"


def get_list_of_branches(path_to_repository):
    # HOT TO SWITCH OF GIT CREDENTIALS
    # with no credentials https://git-scm.com/docs/git-credential-store

    # $ git config credential.helper store
    # $ git push
    # Password: < type your password >

    # [several days later]
    # $ git push
    # [your credentials are used automatically]

    cmd = """
    cd {path_to_submodule}
    git branch --all
    """.format(path_to_submodule=path_to_repository)

    list_of_branches = set()

    result = execute(cmd, print_output=False)
    for line in result:
        list_of_branches.add(line.rstrip().replace("remotes/origin/", ""))

    return list_of_branches


def apply_branch(*, path_to_repository, branch):
    cmd = """
            echo '*** go to repository {path_to_repository}'
            cd {path_to_repository}

            echo '*** clean directory'
            git clean -df
            git checkout -- .
            git reset --hard HEAD

            echo 'fetching all changes'
            git fetch --all

            echo '*** checkout to merge branch or create: {branch_to_merge}'
            git checkout -b {branch_to_merge} || git checkout {branch_to_merge}

            echo '*** merge branches {branch_to_merge}(copy of {current_branch}) + {branch}'
            git merge origin/{branch} -m "IWA: auto-merge {current_branch} + {branch}"
            """.format(
        path_to_repository=path_to_repository,
        branch=branch,
        branch_to_merge=branch_to_merge,
        current_branch=get_current_branch(path_to_repository)
    )
    execute(cmd)


def delete_branch(*, path_to_repository, branch_to_delete):
    cmd = """
        echo '        {branch_to_delete} found'
        echo '        try to delete {branch_to_delete} in {path_to_submodule}'
        cd {path_to_submodule} > /dev/null
        git branch -D {branch_to_delete}
        """.format(
        path_to_submodule=path_to_repository,
        branch_to_delete=branch_to_delete
    )
    execute(cmd)


def change_branch(*, path_to_repository, branch):
    cmd = """
    cd {path_to_submodule}
    git clean -df
    git checkout -- .
    git reset --hard HEAD
    git checkout {branch}

    cmake -E cmake_echo_color --yellow "Try to merge with develop branch!"
    git merge develop -m "IWA: automerge {branch} with all development changes."
    """.format(
        path_to_submodule=path_to_repository,
        branch=branch
    )
    execute(cmd)


def reset(path_to_project_with_submodules):
    cmd = """
    cd {path_to_project_with_submodules}
    git reset --hard HEAD
    git checkout -- .
    git clean -df
    git submodule foreach git reset --hard HEAD
    git submodule foreach git checkout -- .
    git submodule foreach git clean -df
    git submodule sync
    git submodule update --init --recursive
    git submodule update --force
    git submodule foreach make clean
    """.format(
        path_to_project_with_submodules=path_to_project_with_submodules
    )
    execute(cmd)


def get_current_branch(path_to_repository):
    for current_branch in get_list_of_branches(path_to_repository):
        if "* " in current_branch:
            return current_branch.lstrip("* ").strip()
    return None


def is_commit_are_in_branch(path_to_repository, branch, commit):
    current_branch = get_current_branch(path_to_repository)

    cmd = """
    cd {path_to_repository}
    git branch --contains {commit}
    """.format(commit=commit, path_to_repository=path_to_repository)
    branches_which_contains_required_commit = execute(cmd, print_output=False)

    if "* " + current_branch in branches_which_contains_required_commit:
        return True
    else:
        return False
