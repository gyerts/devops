import subprocess
from modules.git.libs import git_submodule
from modules.git.libs import git_branch
from modules.git.libs import commit


class MainGitRepository:
    def __init__(self, path_to_project):
        self.path_to_project = path_to_project
        self.submodules = self.get_all_submodules()

    def get_all_submodules(self):
        return git_submodule.get_submodules(self.path_to_project)

    def clean_all_changes(self):
        cmd = """
            echo "git force clean for: {path_to_project}"
            cd {path_to_project}
            git reset --hard HEAD > /dev/null
            git checkout -- . > /dev/null
            git clean -df > /dev/null
            git submodule foreach git reset --hard HEAD > /dev/null
            git submodule foreach git checkout -- . > /dev/null
            git submodule foreach git clean -df > /dev/null
            git submodule sync > /dev/null
            git submodule update --init --recursive > /dev/null
            git submodule update --force
        """.format(path_to_project=self.path_to_project)

        result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        for line in result.stdout:
            print(line.decode("utf-8"), end="")

    def make_cmd_foreach_submodule(self, cmd, *, ignore_errors=False):
        if ignore_errors:
            cmd = """
                cd {path_to_project}
                git submodule foreach "{cmd} || echo ''"
            """.format(path_to_project=self.path_to_project, cmd=cmd)
        else:
            cmd = """
                cd {path_to_project}
                git submodule foreach {cmd}
            """.format(path_to_project=self.path_to_project, cmd=cmd)

        result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        for line in result.stdout:  # read and store result in log file
            print(line.decode("utf-8"), end="")

    def delete_temporary_branches(self):
        for submodule in self.submodules:
            all_branches = git_branch.get_list_of_branches(self.submodules[submodule].path)
            if 'IWA/MERGE/BRANCH' in all_branches:
                if 'IWA/MERGE/BRANCH' != git_branch.get_current_branch(self.submodules[submodule].path):
                    git_branch.delete_branch(
                        path_to_repository=self.submodules[submodule].path,
                        branch_to_delete='IWA/MERGE/BRANCH'
                    )
                else:
                    raise Exception("The submodule {} still on branch IWA/MERGE/BRANCH".format(
                        self.submodules[submodule].path)
                    )

    def is_pull_request_in_a_project(self, pull_request):
        path_to_repository = self.submodules[pull_request.repository_slug].path
        from_ref = git_branch.is_commit_are_in_branch(
            path_to_repository,
            git_branch.get_current_branch(path_to_repository),
            pull_request.latest_commit_fromRef
        )
        to_ref = git_branch.is_commit_are_in_branch(
            path_to_repository,
            git_branch.get_current_branch(path_to_repository),
            pull_request.latest_commit_toRef
        )
        return from_ref and to_ref

    def apply_pull_request_changes(self, pull_request_to_applying):
        print(pull_request_to_applying)

        if self.is_pull_request_in_a_project(pull_request_to_applying):
            print("This pull-request already integrated. Ignore MainGitRepository.apply_pull_request_changes(...)")
        else:
            path_to_submodule = self.submodules[pull_request_to_applying.repository_slug].path
            git_branch.apply_branch(
                path_to_repository=path_to_submodule,
                branch=pull_request_to_applying.to_branch
            )
            git_branch.apply_branch(
                path_to_repository=path_to_submodule,
                branch=pull_request_to_applying.from_branch,
            )

    def merge_repo_with_branch(self, repo_slug, branches):
        path_to_repository = self.submodules[repo_slug].path

        print("000000000000" * 10)
        print(path_to_repository)

        for branch in branches:
            git_branch.apply_branch(
                path_to_repository=path_to_repository,
                branch=branch
            )
        print("000000000000" * 10)
