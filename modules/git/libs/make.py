import subprocess


def clean(*, path_to_project):
    cmd = """
    cd {path_to_project}
    make clean
    rm -rf products
    """.format(
        path_to_project=path_to_project
    )

    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    for line in result.stdout:  # read and store result in log file
        print(line.decode("utf-8"), end="")

    return "OK"
