import subprocess


def get_user_name(path_to_repository):
    cmd = """
    git config user.name
    """.format(path_to_submodule=path_to_repository)

    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    name = "unknown"
    for line in result.stdout:  # read and store result in log file
        name = line.decode("utf-8").strip()

    return name
