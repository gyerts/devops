import requests
import json
from modules.git.libs.pull_request import PullRequest


def rest_request(url):
    r = requests.get(url, auth=('Yuriy', 'ufuzil12'))
    json_ans = r.json()
    if "errors" in json_ans:
        for error in json_ans["errors"]:
            print(error["message"])
        exit(1)
    return json_ans


def get_dependency_structure(pull_request):
    """
    Adds dependencies to self.dependencies in case of this is unique dependencies

        known_links = [ "domain_1/pull_request/1", "domain_2/pull_request/1", "domain_3/pull_request/1" ]        
        dependencies = {
            "domain_1": {
                "link": "domain_1/pull_request/1",
                "depends": {
                    "domain_2": {
                        "link": "domain_2/pull_request/1",
                        "depends": {}
                    },
                    "domain_3": {
                        "link": "domain_3/pull_request/1",
                        "depends": { ... }
                    }
                }
            }
        }
    :param PullRequest pull_request: 
    :return dict: 
    """
    def add_dependency(pull_request, dependencies):
        """
        :param PullRequest pull_request: 
        :param dict dependencies: 
        :return: dict
        """
        if pull_request.path not in known_links:
            known_links.append(pull_request.path)

            if pull_request.repository_slug not in dependencies:
                dependencies[pull_request.repository_slug] = dict()

            dependencies[pull_request.repository_slug]["link"] = pull_request.path

            inner_dependencies = dict()
            for inner_dependency in pull_request.dependencies:
                add_dependency(PullRequest(inner_dependency), inner_dependencies)

            dependencies[pull_request.repository_slug]["depends"] = inner_dependencies
        return dependencies

    known_links = list()
    dependencies = dict()

    return add_dependency(pull_request, dependencies)


class Rule:
    def __init__(self):
        pass

    def rule_1(self, pull_request_url):
        """ only to develop """
        pr = PullRequest(pull_request_url)
        return pr.to_branch == "develop"

    def rule_2(self, pull_request_url, integrator_email):
        """ only with integrator or integrator is owner of pull-request """
        pr = PullRequest(pull_request_url)
        return integrator_email in pr.reviewers or integrator_email == pr.owner["emailAddress"]

    def rule_3(self, pull_request_url):
        """ everyone should approve review """
        pr = PullRequest(pull_request_url)
        approved = True
        for reviewer in pr.reviewers:
            if pr.reviewers[reviewer]["approved"] is not True:
                approved = False
                break
        return approved

    def check_dependencies(self, dependencies):
        for domain, info in dependencies.items():
            pull_request_url = info['link']

            if 'rules' not in info:
                info['rules'] = dict()

            info['rules']['rule-1'] = self.rule_1(pull_request_url)
            info['rules']['rule-2'] = self.rule_2(pull_request_url, "gyerts@gmail.com")
            info['rules']['rule-3'] = self.rule_3(pull_request_url)


class Project:
    def __init__(self):
        self.domains = self.find_all_repositories()
        for repo in self.domains:
            self.domains[repo]["pull-requests"] = self.find_all_pull_requests(repo)

    def show(self):
        print("**************************************")
        print("SHOW ALL DEPENDENCIES")
        print("**************************************")
        dependencies = dict()
        for domain in self.domains:
            print(domain)
            for pull_request_url in self.domains[domain]["pull-requests"]:
                pr = PullRequest(pull_request_url)
                dependencies = get_dependency_structure(pr)

                rule = Rule()
                rule.check_dependencies(dependencies)

                print(json.dumps(dependencies, indent=4))
                print("\n\n")

    def __iter__(self):
        return (el for el in self.domains.items())

    @staticmethod
    def find_all_pull_requests(repository_slug):
        rest_link = "http://localhost:7990/rest/api/1.0/projects/SUB/repos/" + repository_slug + "/pull-requests"
        json_ans = rest_request(rest_link)

        ans = list()
        for pull_request in json_ans["values"]:
            ans.append(rest_link + "/" + str(pull_request["id"]))
        return ans

    @staticmethod
    def find_all_repositories():
        json_ans = rest_request("http://localhost:7990/rest/api/1.0/projects/SUB/repos?limit=100")

        ans = dict()
        for value in json_ans["values"]:
            ans[value["slug"]] = dict()
        return ans


repo = Project()
repo.show()
