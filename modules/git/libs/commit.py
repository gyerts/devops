from modules.cmd.shell import execute


def get_latest_commit(path_to_repository):
    return execute("git rev-parse HEAD", print_output=False, cwd=path_to_repository)[0]
