import os
import re
from .. import config
from modules.git.libs import git_branch
from modules.regex.cover import get_match


def partitioning(lst, size):
    """Разбиение списка на куски равной длины размером size"""
    return [lst[i:i+size] for i in range(0, len(lst), size)]


class SubModule:
    def __init__(self, name, path, repo, slug, branch):
        self.name = name
        self.path = path
        self.repo = repo
        self.slug = slug

        self.detached = False
        self.branch = branch

        if "(HEAD detached at" in branch:
            self.detached = True
            self.branch = get_match(r"\(HEAD detached at ([\d\w]*)\)", branch)

    def __str__(self):
        return "--- {:>30}  {}".format(self.branch, self.path)


def get_submodules(path_to_project):
    submodules = dict()
    with open(os.path.join(path_to_project, ".gitmodules")) as f:
        lines = [line.strip() for line in f.readlines()]
        for partition in partitioning(lines, 3):
            name = get_match(r"\[submodule \"([^^]+)\"]", partition[0])
            path = os.path.join(path_to_project, get_match(r"path = ([^^]+)", partition[1]))
            repo = get_match(r"url = ([^^]+)", partition[2])
            slug = repo.split('/')[-1].split('.')[0]

            commit = git_branch.get_current_branch(path)

            submodules[slug] = SubModule(name, path, repo, slug, commit)
    return submodules
