import subprocess
import os
from ..libs import git_submodule
from ..libs import git_branch
from .. import config


def is_detached_branch(branch):
    SIGNS_OF_DETACHED_HEAD = "(detached from ", "(HEAD detached at "

    for sign_of_detached_head in SIGNS_OF_DETACHED_HEAD:
        if sign_of_detached_head in branch:
            return True
    return False


def update_repository(path_to_project):
    submodules = git_submodule.get_submodules(path_to_project)
    for sub_mod in submodules:
        path_to_repository = submodules[sub_mod].path

        current_branch = git_branch.get_current_branch(path_to_repository)

        print("#" * 30)
        if not is_detached_branch(current_branch):
            print("Checking {} repository".format(path_to_repository))
            cmd = """
                cd {path_to_submodule}
                git fetch --prune
                git pull
                """.format(path_to_submodule=path_to_repository)

            result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            for line in result.stdout:  # read and store result in log file
                print(line.decode("utf-8"), end="")
        else:
            print("Skip {} repository, because HEAD detached!".format(path_to_repository))
