import subprocess


def execute(cmd, *, cwd=None, print_output=True):
    output = []
    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, cwd=cwd)
    for line in result.stdout:
        line = line.decode("utf-8").strip()
        output.append(line.strip())
        if print_output:
            print(line)
    return output
