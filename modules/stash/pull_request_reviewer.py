from LIBS.mailer.smtplibmailer import Mailer


class PullRequestReviewer:
    def __init__(self, main_integrator_email):
        self.main_integrator_email = main_integrator_email

    def is_valid_description(self, description):
        return '## Description:' in description and \
               '## Dependencies:' in description and \
               '## How to test:' in description

    def notify_about_results_of_review(self, recipient, pull_request_path):
        print('send an email to: ', recipient, self.main_integrator_email)
        mailer = Mailer()
        mailer.set_recipients([recipient, self.main_integrator_email])
        mailer.set_owner_of_mail('IWA_SERVER@luxoft.com')
        mailer.set_subject('[MIB3_EI] - Pull-request format invalid!')
        # mailer.attach_file('/home/mib3/mib3/B0/R14/sdk/readme.txt', 'report.dfgdfg')

        html = """
<p>
    Please be aware that this pull-request:<br><a href="{pull_request_path}">
    {pull_request_path}</a><br>has incorrect format, please follow next structure of description<br>
</p>
<pre>
    ## Dependencies:
    * this pull-request has no dependencies
    * https://stash-pais.eu.panasonic.com/projects/MIB3LUX/repos/<b style="color: red">repo_slug</b>/pull-requests/<b style="color: red">number</b>/<b style="color: red">overview</b>

    ## Description:
    * message of commit 1
    * message of commit 2

    ## How to test:
    * this pull-request adding no functionality to test, just old changes should work
    * service functionality, not possible to test.
</pre>

<p>
    Please contact to Yuriy Gyerts for more information<br>
    Kind Regards.<br><br>
</p>""".format(pull_request_path=pull_request_path)
        mailer.set_html_body(html)
        mailer.send()
