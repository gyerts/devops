from modules.git.libs.main_repository import MainGitRepository
from modules.git.libs.pull_request import PullRequest
from modules.stash.pull_request_reviewer import PullRequestReviewer
# from modules.build import Build
import sys
import json


system = "linux"
# system = "mitron"
path_to_project = sys.argv[1]


#####################################################################
clean_temp_branches                        = 1
#####################################################################
integrated                                 = 1
#####################################################################
do_merge_branch                            = 0
#####################################################################
build                                      = 0
#####################################################################
review_description                         = 1
#####################################################################
apply                                      = 0
#####################################################################


def fetch_all_pull_requests(domains):
    array_of_new_pull_requests = []
    for domain in domains:
        for pull_request_url in domains[domain]:
            array_of_new_pull_requests.append(PullRequest(pull_request_url))
    return array_of_new_pull_requests


git_project = MainGitRepository(path_to_project)
pull_request_reviewer = PullRequestReviewer("YGyerts@luxoft.com")


with open("modules/stash/pull-requests-home.json") as f:
    jobs = json.load(f)

array_with_new_pull_requests =  fetch_all_pull_requests(domains=jobs[system]["new"])
array_with_new_pull_requests += fetch_all_pull_requests(domains=jobs["both"]["new"])


array_with_old_dependencies =   fetch_all_pull_requests(domains=jobs[system]["old_dependencies"])
array_with_old_dependencies +=  fetch_all_pull_requests(domains=jobs["both"]["old_dependencies"])


array_with_new_dependencies =   fetch_all_pull_requests(domains=jobs[system]["new_dependencies"])
array_with_new_dependencies +=  fetch_all_pull_requests(domains=jobs["both"]["new_dependencies"])


branch_which_should_be_applied_every_time =  jobs[system]["branch_which_should_be_applied_every_time"]
branch_which_should_be_applied_every_time.update(jobs["both"]["branch_which_should_be_applied_every_time"])


print("************************************************************************")
print("                        clean temp branches                             ")
print("************************************************************************")
if clean_temp_branches:
    git_project.clean_all_changes()
    git_project.delete_temporary_branches()
else:
    print("[x] pass")


print("************************************************************************")
print("                       find all dependencies                            ")
print("************************************************************************")
if clean_temp_branches:
    git_project.clean_all_changes()
    git_project.delete_temporary_branches()
else:
    print("[x] pass")


print("************************************************************************")
print("                        is review already IN                            ")
print("************************************************************************")
if integrated:
    for pull_request in array_with_new_pull_requests:
        print("|{:>10} | {}".format(
            'integrated' if git_project.is_pull_request_in_a_project(pull_request) else 'new', pull_request.path))
    for pull_request in array_with_old_dependencies:
        print("|{:>10} | {}".format(
            'integrated' if git_project.is_pull_request_in_a_project(pull_request) else 'new', pull_request.path))
    for pull_request in array_with_new_dependencies:
        print("|{:>10} | {}".format(
            'integrated' if git_project.is_pull_request_in_a_project(pull_request) else 'new', pull_request.path))
else:
    print("[x] pass")


print("************************************************************************")
print("                        review description                              ")
print("************************************************************************")
if review_description:
    for pull_request in array_with_new_pull_requests:
        print("|{:>10} | {}".format(
            'OK' if pull_request_reviewer.is_valid_description(pull_request.description)
                 else 'invalid', pull_request.path))
    for pull_request in array_with_old_dependencies:
        print("|{:>10} | {}".format(
            'OK' if pull_request_reviewer.is_valid_description(pull_request.description)
                 else 'invalid', pull_request.path))
    for pull_request in array_with_new_dependencies:
        print("|{:>10} | {}".format(
            'OK' if pull_request_reviewer.is_valid_description(pull_request.description)
                 else 'invalid', pull_request.path))
else:
    print("[x] pass")


print("************************************************************************")
print("                           apply branches                               ")
print("************************************************************************")
if apply:
    for repo_slug in branch_which_should_be_applied_every_time:
        git_project.merge_repo_with_branch(repo_slug, branch_which_should_be_applied_every_time[repo_slug])
else:
    print("[x] pass")


print("************************************************************************")
print("                                apply                                   ")
print("************************************************************************")
if apply:
    for pull_request in array_with_new_pull_requests:
        git_project.apply_pull_request_changes(pull_request)
    for pull_request in array_with_old_dependencies:
        git_project.apply_pull_request_changes(pull_request)
    for pull_request in array_with_new_dependencies:
        git_project.apply_pull_request_changes(pull_request)
else:
    print("[x] pass")


print("************************************************************************")
print("                               build                                    ")
print("************************************************************************")
if build:
    # project_builder = Build(path_to_project)
    # project_builder.build(clean=True, dist_clean=False)
    pass
else:
    print("[x] pass")


print("************************************************************************")
print("                             --- end ---                                ")
print("************************************************************************")
