#!/bin/bash

result=`lsb_release -i`
os=`echo $result |awk -F":" '{print $2}' |tr A-Z a-z`
if [[ $os =~ "ubuntu" ]] ; then
  sudo apt-get install -y python3-pip > /dev/null
  sudo pip3 install --upgrade pip > /dev/null
  sudo pip3 install django==1.10.6 > /dev/null
  sudo apt-get install -y libexpat1-dev cmake gcc g++ automake autoconf > /dev/null
else
  echo 'Not Known OS. Exiting!'
  exit 1
fi
