from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.http import JsonResponse
import os

from Luxoft.base.utility.execute import execute
from Luxoft.base.utility.process import Process
from Luxoft.base.utility.messages import Messages

all_detected_processes = dict()
backslash = '/'

# status - started | running | finished
# result - success | fail | running


def index(request):
    path = '/home/ygyerts/WEB_AUTOMATIZATION/Luxoft/interfaces'
    file_elements = os.listdir(path)

    for el in file_elements:
        full_path = path + backslash + el
        if os.path.isfile(full_path):
            if el[-3:] == '.py' and '__init__' not in el:
                name_of_process = el[:-3]
                if name_of_process not in all_detected_processes:
                    all_detected_processes[name_of_process] = Process(execute, [Messages(), 'python ' + full_path], name_of_process)

    return render_to_response('dashboard.html', {'all_detected_processes': all_detected_processes})


def cron(request):
    return render_to_response('dashboard.html', {})
