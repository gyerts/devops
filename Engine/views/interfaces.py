from django.shortcuts import render_to_response
from django.http import HttpResponse
from Engine.classes.interfaces.CController import Controller
from Engine.classes.config import *
from Engine.classes.login.login import *

controller = Controller()

def find(request):
    if not users.logged_in(request): return get_login_page(request)

    if request.method == 'GET':
        response_dict = dict()
        response_dict['vars'] = variables
        response_dict['request'] = request
        response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))
        return render_to_response('interfaces/find.html', response_dict)

    else:
        try:
            variables.workspaces['COMMON'].settings.set('api_to_check', request.POST['api_to_check'])
            ans = controller.show_interface(request.COOKIES.get('workspace'))
            return HttpResponse(ans)
        except Exception as ex:
            print(str(ex))
            return HttpResponse(str(ex))


def show_all_errors(request):
    if not users.logged_in(request): return get_login_page(request)

    workspace = request.COOKIES.get('workspace')
    ans = controller.show_diffs(request.COOKIES.get('workspace'))

    CPM = ans['CPM']
    HMI = ans['HMI']
    NAVI = ans['NAVI']


    response_dict = dict()
    response_dict['vars'] = variables
    response_dict['request'] = request
    response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))
    response_dict['CPM'] = CPM
    response_dict['HMI'] = HMI
    response_dict['NAVI'] = NAVI
    response_dict['to_cut'] = variables.workspaces[workspace].settings.get('path_to_project')
    return render_to_response('interfaces/all_errors.html', response_dict)



def show_all_interfaces(request):
    if not users.logged_in(request): return get_login_page(request)


    try:
        workspace = request.COOKIES.get('workspace')
        ans = controller.show_all(workspace)

        CPM = ans['CPM interfaces']
        HMI = ans['HMI interfaces']
        NAVI = ans['NAVI interfaces']

        response_dict = dict()
        response_dict['vars'] = variables
        response_dict['request'] = request
        response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))
        response_dict['CPM'] = CPM
        response_dict['HMI'] = HMI
        response_dict['NAVI'] = NAVI
        response_dict['to_cut'] = variables.workspaces[workspace].settings.get('path_to_project')
        return render_to_response('interfaces/all.html', response_dict)

    except Exception as ex:
        print(str(ex))
        return HttpResponse(str(ex))
