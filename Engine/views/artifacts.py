from django.shortcuts import render_to_response
from django.http.response import HttpResponse
from django.http.response import JsonResponse
import os, time
from datetime import datetime, timedelta


from Engine.classes.config import variables
from Engine.classes.filesystem import parse_all_files_and_folders
from Engine.classes.login.login import *
import calendar


class Artifacts:
    def __init__(self, path_to_artifacts=None):
        self.path_to_artifacts = path_to_artifacts
        self.folder = parse_all_files_and_folders(self.path_to_artifacts)


class Day:
    def __init__(self, day, weekday, month):
        self.number = day
        self.weekday = weekday
        self.month = month


class Week:
    def __init__(self):
        self.days = list()

    def add_day(self, dataObj):
        self.days.append(Day(dataObj.day, dataObj.weekday(), dataObj.month))


class Month:
    def __init__(self, dateObj):
        self.count_of_days = self.__get_count_of_days(dateObj)
        self.weeks = list()
        self.__fill_all_days(self.count_of_days, dateObj.month, dateObj.year)
        self.first_week = self.weeks[0]
        self.last_week = self.weeks[-1]
        self.second_week = self.weeks[1]

        self.count_of_month = dateObj.month
        self.year = dateObj.year

    def __get_count_of_days(self, date):
        return calendar.monthrange(date.year, date.month)[1]

    def __add_day(self, dataObj):
        weekday = dataObj.weekday()
        if len(self.weeks) == 0:
            self.weeks.append(Week())

        self.weeks[-1].add_day(dataObj)

        if weekday == 6:
            self.weeks.append(Week())

    def __fill_all_days(self, count_of_days, number_of_month, year):
        for day in range(1, count_of_days + 1):
            self.__add_day(datetime(year=year, month=number_of_month, day=day))

        if len(self.weeks[-1].days) == 0:
            del(self.weeks[-1])







def get_artifacts_by_name(path_to_artifacts):
    print('function get_artifacts_by_name', path_to_artifacts)
    artifacts_folder = Artifacts(path_to_artifacts).folder
    print('    took artifacts by path', path_to_artifacts)
    return  artifacts_folder


def get_folders_by_name(path, path_to_artifacts):
    print('function get_folders_by_name')
    all_folders_and_files = {'folders': list(), 'files': list()}
    artifacts_folder = get_artifacts_by_name(path)

    for folder in artifacts_folder.folders:
        all_folders_and_files['folders'].append({'name':folder.name, 'path':folder.path.replace(path_to_artifacts + os.sep, '')})

    print('finish with folders')

    for file in artifacts_folder.files:
        all_folders_and_files['files'].append({'name':file.name, 'path':file.path.replace(path_to_artifacts + os.sep, '')})

    print('finish with files')

    return all_folders_and_files


def get_folders_by_date(request):
    workspace = request.COOKIES.get('workspace')
    workspace = variables.workspaces[workspace]

    date = get_date(request, request.POST['day'])

    all_folders_and_files = {'folders': list(), 'files': list()}
    path_to_artifacts = os.path.join(workspace.path_to_workspace, 'ARTIFACTS')
    folder_with_artifacts = get_artifacts_by_name(path_to_artifacts)

    for folder in folder_with_artifacts.folders:
        ct = datetime.strptime(time.ctime(folder.info.created_time), "%a %b %d %H:%M:%S %Y")
        if ct.year == date.year and ct.month == date.month and ct.day == date.day:
            print('append folder: ', folder.name)
            all_folders_and_files['folders'].append({'name':folder.name, 'path':folder.path.replace(path_to_artifacts + os.sep, '')})

    for file in folder_with_artifacts.files:
        ct = datetime.strptime(time.ctime(file.info.created_time), "%a %b %d %H:%M:%S %Y")
        if ct.year == date.year and ct.month == date.month and ct.day == date.day:
            print('append folder: ', file.name)
            all_folders_and_files['files'].append({'name':file.name, 'path':file.path.replace(path_to_artifacts + os.sep, '')})

    return all_folders_and_files


def get_three_months(response_dict, new_date):
    new_date = datetime(year=new_date.year, month=new_date.month, day=1)
    response_dict['month_before'] = Month(new_date - timedelta(days=new_date.day))
    response_dict['month_now'] = Month(new_date)
    response_dict['month_after'] = Month(new_date + timedelta(days=new_date.day + (calendar.monthrange(new_date.year, new_date.month)[1]) - new_date.day + 1))


def get_date(request, day=1):
    month = int(request.POST['month'])
    year  = int(request.POST['year'])
    new_date = datetime(year=year, month=month, day=int(day))
    return new_date


def set_month(response_dict, request, todo):
    new_date = get_date(request)

    if todo   == 'minus_month':
        new_date = new_date - timedelta(days=1)
    elif todo == 'plus_month':
        new_date = new_date + timedelta(days=new_date.day + calendar.monthrange(new_date.year, new_date.month)[1])
    elif todo == 'minus_year':
        new_date = datetime(new_date.year - 1, new_date.month, new_date.day)
    elif todo == 'plus_year':
        new_date = datetime(new_date.year + 1, new_date.month, new_date.day)
    get_three_months(response_dict, new_date)


def get_content_of_file(path, path_to_artifacts):
    print('function get_content_of_file')
    content = open(path).readlines()
    print('    lengt of readed file is:', len(content))
    return {'content':content}


def validate_path(path_to_artifacts, request, function):
    path = os.path.join(path_to_artifacts, request.POST['path'])
    print(path)

    if path[-1] == os.sep:
        path = path[0:-1]

    response = dict()

    idx = request.POST['path'].rfind(os.sep)
    if idx != -1: response['up_folder'] = request.POST['path'][0:idx]
    elif request.POST['path'] == '..': response['up_folder'] = '..'
    elif len(request.POST['path']) > 0: response['up_folder'] = '..'
    else: response['up_folder'] = '..'

    if path[-4:] == 'root' or path[-1:] == '.' or path[-2] == '..' or '..' in path:
        response.update(function(path_to_artifacts, path_to_artifacts))
    else:
        response.update(function(path, path_to_artifacts))

    return response


def artifacts(request):
    if not users.logged_in(request): return get_login_page(request)

    response_dict = dict()
    response_dict['vars'] = variables
    response_dict['request'] = request
    response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))

    workspace = request.COOKIES.get('workspace')
    path_to_artifacts = variables.workspaces[workspace].settings.get('path_to_artifacts')

    if request.method == 'GET':
        now = datetime.now() + timedelta(hours=3)

        response_dict['date_now'] = Day(now.day, now.weekday(), now.month)
        get_three_months(response_dict, now)

        return render_to_response('artifacts/artifacts.html', response_dict)

    else:
        todo = request.POST['todo']
        print('POST:', request.POST)

        if todo == 'minus_month' or todo == 'plus_month' or todo == 'minus_year' or todo == 'plus_year':
            set_month(response_dict, request, todo)
            return render_to_response('artifacts/calendar.html', response_dict)
        elif todo == 'get_folders_and_files_by_date':
            response = get_folders_by_date(request)
            response['up_folder'] = '..'
            return JsonResponse(response)
        elif todo == 'get_folder_by_name':
            response = validate_path(path_to_artifacts, request, get_folders_by_name)
            print(response)
            return JsonResponse(response)

        elif todo == 'get_content_of_file':
            response = validate_path(path_to_artifacts, request, get_content_of_file)
            print(response)
            return JsonResponse(response)



