from django.http import JsonResponse

from Luxoft.base.utility.process import Process
from Luxoft.base.utility.messages import Messages
from Luxoft.api.linloctest import run_lint_loc_test

running_processes = dict()

def run_buildcentral(request):
    global running_processes

    response_data = {}
    response_data['status'] = None # started|running|finished
    #response_data['result'] = None # success | fail | running
    response_data['message'] = None
    response_data['name'] = '<span style="color: green">run_buildcentral</span>'


    if 'run_buildcentral' in running_processes:
        latest_messages = running_processes['run_buildcentral'].messages.get_latest_messages()
        output = str()
        for message in latest_messages:
            print('---->   ', message)
            output += str(message) + '<br>'
        response_data['message'] = output


        if running_processes['run_buildcentral'].isAlive():
            response_data['status'] = 'running'
        else:
            del(running_processes['run_buildcentral'])
            response_data['status'] = 'finished'

    else:
        running_processes['run_buildcentral'] = Process(run_lint_loc_test, [Messages()])
        running_processes['run_buildcentral'].start()
        response_data['status'] = 'started'
        response_data['message'] = ''

    return JsonResponse(response_data)