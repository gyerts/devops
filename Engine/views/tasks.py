from django.shortcuts import render_to_response
from Engine.classes.config import *
from django.http import HttpResponse

from Engine.classes.filesystem import *
from Engine.classes.task.task import *
from Engine.classes.login.login import *

import sys


SERVER_PORT = sys.argv[2]
variables.workspaces['DEFAULT'].settings.set('server:port', SERVER_PORT)


def show_all_tasks(request):
    if not users.logged_in(request): return get_login_page(request)

    response_dict = dict()
    response_dict['vars'] = variables
    response_dict['request'] = request
    response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))

    return render_to_response('tasks.html', response_dict)


def change_status_of_task(request):
    if not users.logged_in(request): return get_login_page(request)

    workspace = request.COOKIES.get('workspace')

    if request.method == 'POST':
        status = request.POST['status']
        name = request.POST['key']

        output = str()

        all_tasks = variables.workspaces[workspace].tasks
        for task_name in all_tasks:
            task = all_tasks[task_name]
            if task.name == name:
                if status == 'Enable' and task.status != 'Enabled':
                    task.status = 'Enabled'
                    output = 'task: ' + task.name + ' ' + task.status
                elif status == 'Disable' and task.status != 'Disabled':
                    task.status = 'Disabled'
                    output = 'task: ' + task.name + ' ' + task.status
                else:
                    output = 'some errors'

    save_tasks_into_crontab(all_tasks)
    return HttpResponse(output)


def change_time_pattern(request):
    if not users.logged_in(request): return get_login_page(request)

    workspace = request.COOKIES.get('workspace')
    all_tasks = variables.workspaces[workspace].tasks
    if request.method == 'POST':

        time_pattern = request.POST['time_pattern']
        name = request.POST['key']

        output = str()

        for task_name in all_tasks:
            task = all_tasks[task_name]

            if task.name == name:
                if task.time_pattern == time_pattern:
                    output = 'pattern not changed in ' + task.name
                else:
                    output = 'pattern changed from \'' + task.time_pattern + '\' to \'' + time_pattern + '\''
                    task.time_pattern = time_pattern

    save_tasks_into_crontab(all_tasks)
    return HttpResponse(output)

def get_text_of_task(request):
    if not users.logged_in(request): return get_login_page(request)
    workspace = request.COOKIES.get('workspace')

    all_tasks = all_tasks = variables.workspaces[workspace].tasks
    if request.method == 'POST':
        name = request.POST['key']

        output = str()

        for task_name in all_tasks:
            task = all_tasks[task_name]

            if task.name == name:
                f = open(task.path)
                lines = f.readlines()

                for line in lines:
                    output += line

    return HttpResponse(output)

def edit_python_file(request):
    if not users.logged_in(request): return get_login_page(request)

    workspace = request.COOKIES.get('workspace')

    all_tasks = all_tasks = variables.workspaces[workspace].tasks

    idx = request.path.rfind('/')
    name = request.path[idx + 1:]

    lines = list()

    founded = False
    for task_name in all_tasks:
        task = all_tasks[task_name]

        if task.name == name:
            f = open(task.path)
            lines = f.readlines()
            founded = True

    if not founded:
        return HttpResponse('<h1>Script ' + name + '.py not founded!</h1>')

    response_dict = dict()
    response_dict['vars'] = variables
    response_dict['request'] = request
    response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))
    response_dict['python_file'] = lines
    response_dict['name_of_script'] = name + '.py'
    return render_to_response('edit_python_file.html', response_dict)
