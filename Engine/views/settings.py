from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpRequest
from django.template import RequestContext

from Engine.classes.config import variables
from Engine.classes.login.login import *


#data.append('key', el.attr('key'));       /* always key*/

def settings(request):
    if not users.logged_in(request): return get_login_page(request)

    url = HttpRequest.build_absolute_uri(request)

    print('URL (build_absolute_uri):', url)

    if 'global_settings' in request.get_raw_uri():
        settings = variables.global_settings
        name = 'GLOBAL IWA'
        url_path = 'settings/global_settings'
    else:
        name = request.COOKIES.get('workspace')
        settings = variables.workspaces[name].settings
        url_path = 'settings'


    if request.method == 'GET':
        response_dict = dict()
        response_dict['vars'] = variables
        response_dict['settings'] = settings
        response_dict['request'] = request
        response_dict['name'] = name
        response_dict['url_path'] = url_path
        response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))

        return render_to_response('settings.html', response_dict)

    else:
        print('___NAME___', name)
        print('request.POST: ', request.POST)
        action = request.POST['action']
        key = request.POST['key']
        text = request.POST['text']

        if action == 'change_key':
            settings.settings[text] = settings.settings.pop(key)

        elif action == 'change_value':
            settings.set(key, text)

        elif action == 'delete':
            print(type(settings))
            settings.delete(key)

        elif action == 'add':
            settings.set(key, text)

        return HttpResponse('Successful!')
