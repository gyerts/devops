from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.http import JsonResponse
import json

import os, sys
sys.path.insert(0, os.getcwd())

from Engine.classes.config import variables
from Engine.classes.login.login import *


def main(request):
    if not users.logged_in(request): return get_login_page(request)

    response_dict = dict()
    response_dict['vars'] = variables
    response_dict['request'] = request
    response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))

    return render_to_response('dashboard.html', response_dict)


def run_function(request):
    if not users.logged_in(request): return get_login_page(request)

    params = request.get_raw_uri().split('/?')[1].replace('%7B', '{').replace('%7D', '}').replace('%22', '"').replace('%20', ' ')
    params = json.loads(params)

    ws = params['ws']
    script = params['script']
    args = params['args']



    'http://0.0.0.0:8000/run_function/?{"ws":"BOYTON","script":"find_interface_usage","args":"api.hbsi send_to_everyone"}'

    return HttpResponse('')


def works(request):
    if not users.logged_in(request): return get_login_page(request)

    url = request.get_raw_uri()

    print('unknown url: ' + url)

    script, func, params = url.split('::')

    params = url.split('%20')

    splitted_url = params[0].split('/')
    del(splitted_url[0])
    del(splitted_url[0])
    del(splitted_url[0])

    for ws in variables.workspaces:
        if ws == splitted_url[0]:
            print('------------------ ' + ws + ' ------------------')


    if 0:
        pass
    else:
        response_dict = dict()
        response_dict['vars'] = variables
        response_dict['request'] = request
        response_dict['user'] = users.get_user_by_key(request.COOKIES.get('key'))

        return render_to_response('dashboard.html', response_dict)


def run_process(request):
    response_data = {}

    try:
        # define process
        process = request.POST['process']
        workspace = request.POST['workspace']

        print(process, workspace)

        process = variables.workspaces[workspace].scripts[process]

        if not process.isAlive():
            process.start()

        response_data['name'] = str(process.args[1])
        response_data['status'] = 'started'
        response_data['message'] = '<span class="label label-info">Start process</span><span class="greened">' + process.get_name().replace('_', ' ') + '</span></br></br>'
        response_data['message'] += '<span class="label label-info">Current directory</span> <span class="greened">' + os.getcwd() + '</span></br>'
    except Exception as ex:
        print(ex)

    return JsonResponse(response_data)


def get_output_of_running_process(request, process=None, show_from_message=None):
    response_data = {}

    try:
        # define process
        if(process == None and show_from_message == None):
            process = request.POST['process']
            show_from_message = request.POST['show_from_message']

        # define process
        workspace = request.COOKIES.get('workspace')
        process = variables.workspaces[workspace].scripts[process]

        response_data['name'] = process.get_name()

        latest_messages = process.messages.get_latest_messages(show_from_message)['messages']

        output = str()
        for message in latest_messages:
            if(message != '' or output != '\n'):
                output += str(message) + '<br>'
        response_data['message'] = output
        response_data['len_of_returned_messages'] = len(latest_messages)


        if process.isAlive():
            response_data['status'] = 'running'
        else:
            response_data['status'] = 'finished'
            response_data['message'] += '<span class="label label-info">Process finished</span> <span class="greened">' + process.get_name().replace('_', ' ') + '</span></br>'
    except Exception as ex:
        print(ex)

    return JsonResponse(response_data)


def get_active_process(request):
    workspace = request.POST['workspace']

    try:
        all_workspace_scripts = variables.workspaces[workspace].scripts
        for process in all_workspace_scripts:
            if(all_workspace_scripts[process].isAlive()):
                return JsonResponse({'process':process})
        return JsonResponse({'process':'None'})
    except Exception as ex:
        print(ex)