from django.template.defaulttags import register

@register.filter
def get_item(dictionary, key):
    print("*"*1000)
    print(dictionary)
    print(key)
    return dictionary.get(key)

@register.simple_tag
def replace(string, old, new):
    return string.replace(old, new)

@register.filter
def ostatok(a, b):
    return a%b

@register.filter
def times(number):
    return range(1, number + 1)

@register.filter
def dev_to_seven(number):
    return (number%7)

@register.filter
def sum(a, b):
    return (a+len(b))

@register.filter
def month(a):
    if a == 1:
        return 'Jan'
    if a == 2:
        return 'Feb'
    if a == 3:
        return 'Mar'
    if a == 4:
        return 'Apr'
    if a == 5:
        return 'May'
    if a == 6:
        return 'Jun'
    if a == 7:
        return 'Jul'
    if a == 8:
        return 'Aug'
    if a == 9:
        return 'Sep'
    if a == 10:
        return 'Oct'
    if a == 11:
        return 'Nov'
    if a == 12:
        return 'Dec'

@register.filter
def img(number):
    return 'img/'+str(number)+'.jpg'