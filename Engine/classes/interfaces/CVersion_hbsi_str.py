class HBSI_version_str():
    def get(self, name, root):
        result = None
        for child in root:
            if child.tag == 'Version':
                result = child[0].text + '.' + child[1].text
                break
        return result