from Luxoft.base.interfaces.CVersion_hbsi_str import HBSI_version_str

class HMI_use_API():
    def __init__(self, hmi_apies, apies):
        self.hmi_apies = hmi_apies
        self.apies = apies
        self.table = list()

    def get(self):
        for hmi_api in self.hmi_apies:
            interface = list()
            interface.append(hmi_api)
            interface.append(HBSI_version_str().get(hmi_api, self.hmi_apies[hmi_api]))
            for api in self.apies:
                index = api.rfind('\\')
                name = api[index + 1:]
                if name in hmi_api:
                    interface.append(HBSI_version_str().get(hmi_api, self.apies[api]))
                    break

            self.table.append(interface)
        return self.table