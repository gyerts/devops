from collections import OrderedDict
import os

from Engine.classes.interfaces.CFile_arr import FILE_arr

from Engine.classes.config import *

class Controller():
    def __init__(self):
        print('   __init__')
        self.all_cpm_hbsi_files = None
        self.all_hmi_hbsi_files = None
        self.all_nav_hbsi_files = None
        self.path_to_project_text = None
        self.input = None
        self.all_cpm_diffs = dict()
        self.all_hmi_diffs = dict()
        self.all_nav_diffs = dict()
        self.path_to_project = None


    def set_input(self, inputed_text):
        self.input = inputed_text

    def update(self, workspace):
        self.sync_settings()

        self.path_to_project = variables.workspaces[workspace].settings.get('path_to_project')

        if not os.path.exists(self.path_to_project):
            return False

        self.all_cpm_hbsi_files = FILE_arr()

        self.all_cpm_hbsi_files.add_hbsi_files(os.path.join(self.path_to_project, 'api'))

        self.all_cpm_hbsi_files.add_hbcm_files(os.path.join(self.path_to_project, 'imp'), self.path_to_project)
        self.all_cpm_diffs = self.all_cpm_hbsi_files.get_diffs()

        self.all_nav_hbsi_files = FILE_arr()

        self.all_nav_hbsi_files.add_hbsi_files(os.path.join(self.path_to_project, 'deliveries', 'api', 'nav'))

        self.all_nav_hbsi_files.add_hbcm_files(os.path.join(self.path_to_project, 'imp'), os.path.join(self.path_to_project, 'deliveries'))
        self.all_nav_diffs = self.all_nav_hbsi_files.get_diffs()

        self.all_hmi_hbsi_files = FILE_arr()

        self.all_hmi_hbsi_files.add_hbsi_files(os.path.join(self.path_to_project, 'imp', 'HMI', 'qdsi', 'interfaces', 'api'))
        self.all_hmi_diffs = self.find_all_diffs_in_hmi()


    def render_hbsi_table(self, result):
        print('   render_hbsi_table')

        output = str()
        output += """<table border='1'>
                        <tr>
                            <th class="hbcm">HBCM</th>
                            <th class="hbcmver">HBCM version</th>
                            <th class="hbsiver">HBSI version</th>
                            <th class="hbsi">HBSI</th>
                        </tr>"""

        for hbsi in result:
            output += """<tr>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
            </tr>"""

            quantyty = len(hbsi.list_HBCM)
            if quantyty > 0:
                pass
            else:
                 output += """<tr>
                    <td class="hbcm"><b style="color: orangered">CPM manifests do not use it</b></td>
                    <td class="hbcmver"></td>
                    <td class="hbsiver">""" + hbsi.version + """</td>
                    <td class="hbsi">""" + hbsi.path[len(str(self.path_to_project)) + 1:] + """</td>
                </tr>"""

            forloop = 0
            for hbcm in hbsi.list_HBCM:
                if hbcm != None:
                    output += "<tr><td class='hbcm'>" + hbcm['path'][len(str(self.path_to_project)) + 1:] + "</td>"
                    if hbsi.version == hbcm['version']:
                        output += "<td class='hbcmver'>" + hbcm['version'] + "</td>"
                    else:
                        output += "<td class='hbcmver' style='background-color: orangered'><b>WARNING: " + hbcm['version'] + "</b></td>"
                else:
                    output += "<td colspan='2' class='hbcmver'>Not found on the workspace</td>"

                if forloop == 0:
                    output += "<td rowspan=\"" + str(quantyty) + "\" class='hbsiver'>" + hbsi.version + "</td>"
                    output += "<td rowspan=\"" + str(quantyty) + "\" class='hbsi'>" + hbsi.path[len(str(self.path_to_project)) + 1:] + "</td></tr>"
                forloop += 1

        output += '</table>'

        if len(result) == 0:
            output = None
            #self.browser.updProcessComleteSignal.emit("<span>\"" + str(self.input) + "\" not found in a project root</span>")

        return output

    def render_hbsi_hmi_dif_table(self, result): # [ { 'hbsi_of_workspace': HBSI_file, 'hbsi_of_hmi': HBSI_file }, ... ]
        print('   render_hbsi_hmi_dif_table')
        output = str()
        output += """<table border='1'>
                        <tr>
                            <th>Workspace HBSI path</th>
                            <th>Workspace version</th>
                            <th>HMI version</th>
                            <th>HMI HBSI path</th>
                        </tr>"""

        for pair_hbsi in result:
            output += """<tr>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
                <td class="line"></td>
            </tr>"""

            forloop = 0

            if pair_hbsi['hbsi_of_workspace'] != None:
                output += "<tr><td>" + pair_hbsi['hbsi_of_workspace'].path[len(str(self.path_to_project_text)) + 1:] + "</td>"

                if pair_hbsi['hbsi_of_hmi'].version == pair_hbsi['hbsi_of_workspace'].version:
                    output += "<td>" + pair_hbsi['hbsi_of_workspace'].version + "</td>"
                else:
                    output += "<td style='background-color: orangered'><b>WARNING: " + pair_hbsi['hbsi_of_workspace'].version + "</b></td>"
            else:
                output += "<td colspan='2' style='background-color: yellow'>Not found on the workspace</td>"

            output += "<td>" + pair_hbsi['hbsi_of_hmi'].version + "</td>"
            output += "<td>" + pair_hbsi['hbsi_of_hmi'].path[len(str(self.path_to_project_text)) + 1:] + "</td></tr>"

        output += '</table>'

        return output

    def sync_settings(self):
        self.input = variables.workspaces['COMMON'].settings.get('api_to_check')
        print('   sync_settings')
        if len(self.input) > 5:
            if self.input[-5:] == '.hbsi':
                self.input = self.input[:-len('.hbsi')]

        variables.workspaces['COMMON'].settings.set('api_to_check', self.input)
        print(self.input)

    def find_cpm_in_hmi(self, founded_cpm_hbsi):
        print('   find_cpm_in_hmi')
        founded_hmi_hbsi = None
        if founded_cpm_hbsi is not None:
            for hmi_hbsi in self.all_hmi_hbsi_files.all_HBSI_files:
                if hmi_hbsi.name == founded_cpm_hbsi.name:
                    founded_hmi_hbsi = hmi_hbsi

        return founded_hmi_hbsi

    def find_hmi_in_cpm(self, founded_hmi_hbsi):
        print('   find_hmi_in_cpm')
        founded_cpm_hbsi = None
        if founded_hmi_hbsi is not None:
            for cpm_hbsi in self.all_cpm_hbsi_files.all_HBSI_files:
                if cpm_hbsi.name == founded_hmi_hbsi.name:
                    founded_cpm_hbsi = cpm_hbsi

        return founded_cpm_hbsi

    def find_hmi_in_nav(self, founded_hmi_hbsi):
        print('   find_hmi_in_nav')
        founded_nav_hbsi = None
        if founded_hmi_hbsi is not None:
            for nav_hbsi in self.all_nav_hbsi_files.all_HBSI_files:
                if nav_hbsi.name == founded_hmi_hbsi.name:
                    founded_nav_hbsi = nav_hbsi

        return founded_nav_hbsi

    def find_all_diffs_in_hmi(self):
        print('   find_all_diffs_in_hmi_')
        founded_hbsies = list()

        for hmi_hbsi in self.all_hmi_hbsi_files.all_HBSI_files:
            founded = False
            for cpm_hbsi in self.all_cpm_hbsi_files.all_HBSI_files:
                if cpm_hbsi.name == hmi_hbsi.name:
                    founded = True
                    if cpm_hbsi.version != hmi_hbsi.version:
                        founded_hbsies.append({'hbsi_of_workspace': cpm_hbsi, 'hbsi_of_hmi': hmi_hbsi})
                    break

            for nav_hbsi in self.all_nav_hbsi_files.all_HBSI_files:
                if nav_hbsi.name == hmi_hbsi.name:
                    founded = True
                    if nav_hbsi.version != hmi_hbsi.version:
                        founded_hbsies.append({'hbsi_of_workspace': nav_hbsi, 'hbsi_of_hmi': hmi_hbsi})
                    break

            #if not founded:
                #founded_hbsies.append({'hbsi_of_workspace': None, 'hbsi_of_hmi': hmi_hbsi})

        return founded_hbsies

    def show_diffs(self, workspace):
        self.update(workspace)
        print('-------------show_diffs-------------')

        self.sync_settings()
        ans = {'CPM':None, 'NAVI':None, 'HMI':None}

        if self.all_cpm_diffs is not None:
            if len(self.all_cpm_diffs) > 0:
                ans['CPM'] = self.all_cpm_diffs

        if self.all_nav_diffs is not None:
            if len(self.all_nav_diffs) > 0:
                ans['NAVI'] = self.all_nav_diffs

        # find hmi diffs
        if self.all_hmi_diffs is not None:
            if len(self.all_hmi_diffs) > 0:
                ans['HMI'] = self.all_hmi_diffs

        return ans

    def show_interface(self, workspace):
        self.update(workspace)
        print('-------------show_interface-------------')
        try:
            self.sync_settings()

            html = list()
            founded_workspace_hbsi = None

            html += '<h1>Project path: ' + self.path_to_project + '</h1>'

            for hbsi in self.all_cpm_hbsi_files.all_HBSI_files:
                if hbsi.name.lower() == self.input.lower():
                    html += '<h1>Detected as CPM interface</h1>'
                    html += self.render_hbsi_table([hbsi])
                    founded_workspace_hbsi = hbsi
                    break


            if founded_workspace_hbsi is None:
                for hbsi in self.all_nav_hbsi_files.all_HBSI_files:
                    if hbsi.name.lower() == self.input.lower():
                        html += '<h1>Detected as NAVI interface</h1>'
                        html += self.render_hbsi_table([hbsi])
                        founded_workspace_hbsi = hbsi
                        break

            if founded_workspace_hbsi is None:
                html = '<h1>CPM do not uses this interface: ' + variables.workspaces['COMMON'].settings.get('api_to_check') + '</h1>'


            if founded_workspace_hbsi is not None:
                founded_hmi_hbsi = self.find_cpm_in_hmi(founded_workspace_hbsi)
                if founded_hmi_hbsi is not None:
                    html += '<h1>HMI uses this intrface</h1>'
                    html += self.render_hbsi_hmi_dif_table([{'hbsi_of_workspace': founded_workspace_hbsi, 'hbsi_of_hmi': founded_hmi_hbsi}])
            if html is not None:
                return html
            else:
                return '<span style="color: red">\'' + str(self.input) + '\' not found</span>'

        except Exception as ex:
            if str(ex) == "'NoneType' object has no attribute 'all_HBSI_files'":
                return '<span style="color: red">first you need update file tree</span> press green button'
            else:
                return str(ex)

    def show_all(self, workspace):
        self.update(workspace)
        print('-------------show_all------------')
        try:
            self.sync_settings()

            answer = OrderedDict()

            if self.all_cpm_hbsi_files.all_HBSI_files is not None:
                answer['CPM interfaces'] = self.all_cpm_hbsi_files.all_HBSI_files

            if self.all_nav_hbsi_files.all_HBSI_files is not None:
                answer['NAVI interfaces'] = self.all_nav_hbsi_files.all_HBSI_files

            list_of_hmi_hbsi = list() #[{'hbsi_of_workspace': founded_cpm_hbsi, 'hbsi_of_hmi': founded_hmi_hbsi}]

            for hmi_hbsi in self.all_hmi_hbsi_files.all_HBSI_files:
                founded_workspace_hbsi = self.find_hmi_in_cpm(hmi_hbsi)
                if founded_workspace_hbsi is None:
                    founded_workspace_hbsi = self.find_hmi_in_nav(hmi_hbsi)

                if founded_workspace_hbsi is not None:
                    list_of_hmi_hbsi.append({'hbsi_of_workspace': founded_workspace_hbsi, 'hbsi_of_hmi': hmi_hbsi})
                else:
                    list_of_hmi_hbsi.append({'hbsi_of_workspace': None, 'hbsi_of_hmi': hmi_hbsi})

            answer['HMI interfaces'] = list_of_hmi_hbsi

            return answer

        except Exception as ex:
            if str(ex) == "'NoneType' object has no attribute 'all_HBSI_files'":
                return '<span style="color: red">first you need update file tree</span> press green button'
            else:
                return str(ex)