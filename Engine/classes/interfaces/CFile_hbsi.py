import platform

if platform.system().lower() == 'windows':
    bs = '\\'
else: bs = '/'

class HBSI_file():
    def __init__(self):
        self.name = None
        self.path = None
        self.version = None
    # переменная что говорит о несоответствии интерфейсов, устанавливается методом check_diffs()
        self.errors = None
    # набор HBCM файлов которые используют этот интерфейс
        self.list_HBCM = list() # [ {'HBCM_name': name, 'HBCM_path': path, 'HBCM_using_version': version}, ... ]

    def parse_root(self, path, root): # { path: root }
    # 1) set name
        start = path.rfind(bs)
        finish = path.rfind('.')
        self.name = path[start + 1: finish]
    # 2) set path
        self.path = path
    # 3) set version
        for child in root:
            if child.tag == 'Version':
                self.version = child[0].text + '.' + child[1].text
                break

    def check_diffs(self):
        self.errors = False
        for hbcm in self.list_HBCM:
            if self.version != hbcm['version']:
                self.errors = True
                break

        return self.errors