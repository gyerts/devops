import subprocess, os, sys

def execute(messages, command):
    """executs it in separated thread"""
    from Engine.classes.config import variables

    my_env = os.environ

    my_env['PYTHONPATH'] = ":".join([os.getcwd(), 
      os.path.join(os.getcwd(), "LIBS"),
      os.path.join(os.getcwd(), "WORKSPACES"),
      variables.path_to_project])

    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=my_env)

    for line in iter(process.stdout.readline, ""):
        if line == b'':
            break
        messages.add_message(line.decode().strip())
