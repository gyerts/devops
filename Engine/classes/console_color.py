from datetime import datetime


class ConsoleColor:
    @staticmethod
    def normal(text):
        print("%s %s" % (ConsoleColor.timeNow(), text))

    @staticmethod
    def blue(text):
        ConsoleColor.normal("\033[94m%s\033[0m" % text)

    @staticmethod
    def yellow(text):
        ConsoleColor.normal("\033[93m%s\033[0m" % text)

    @staticmethod
    def red(text):
        ConsoleColor.normal("\033[91m%s\033[0m" % text)

    @staticmethod
    def purple(text):
        ConsoleColor.normal("\033[95m%s\033[0m" % text)

    @staticmethod
    def green(text):
        ConsoleColor.normal("\033[92m%s\033[0m" % text)

    @staticmethod
    def timeNow():
        return datetime.now().strftime("[%H:%M:%S]")