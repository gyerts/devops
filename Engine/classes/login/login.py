from django.http import HttpResponse
from django.shortcuts import render_to_response
import uuid

class User:
    def __init__(self, name, password):
        self.name = name
        self.password = password
        self.key = uuid.uuid4().hex


class Users:
    def __init__(self):
        self.users_keys = dict()
        self.registered_users = dict()

        self.add_user_to_register_from_file('name_of_file_with_users')

    def logged_in(self, request):
        """This function check if person logged in"""
        try:
            return self.key_in_registered_keys(request.COOKIES.get('key'))

        except Exception as ex:
            print(ex)
            return False

    def key_in_registered_keys(self, key):
        if key in self.users_keys.keys(): return True
        else: return False

    def registered(self, request):
        #print('def registered():')

        requested_email = request.POST['email']
        sent_password = request.POST['password']

        if requested_email in self.registered_users:
            user = self.registered_users[requested_email]
            if user.password == sent_password:
                #print('return True')
                return True
        #print('return False')
        return False

    def add_user_to_register_by_request(self, request):
        user = User(request.POST['email'], request.POST['password'])
        self.registered_users[request.POST['email']] = user
        return user

    def add_user_to_register_from_file(self, name_of_file_with_users):
        user = User(name='admin@luxoft.com', password='admin')
        self.registered_users['admin@luxoft.com'] = user
        return user

    def get_user_by_request(self, request):
        requested_email = request.POST['email']
        return self.registered_users[requested_email]

    def get_user_by_key(self, key):
        return self.users_keys[key]

    def log_user_in(self, user):
        self.users_keys[user.key] = user

users = Users()


def register(request):
    if request.method == 'GET':
        response = render_to_response('auth/login_form.html', {} )

    else:
        absolute_uri = request.COOKIES.get('request_from_url')
        #print('absolute_uri', absolute_uri)

        response = render_to_response('auth/redirect.html', {'redirect_to': absolute_uri})

        if users.registered(request):
            user = users.get_user_by_request(request)
            response.set_cookie('key', user.key)
            users.log_user_in(user)

        response.set_cookie('workspace', 'DEFAULT')

    return response


def get_login_page(request):
    response = render_to_response('auth/login_form.html', {} )
    response.set_cookie('absolute_uri', request.build_absolute_uri())
    return response
