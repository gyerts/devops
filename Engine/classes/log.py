class Log:
    def __init__(self, path):
        self.output = list()
        self.path = path
        self.errors = list()

    def add_line(self, line, error=0):
        self.output.append(line)
        file = open(self.path, 'w')
        for line in self.output:
            file.write(line + '\n')

        file.close()
        if error == 1:
            self.errors.append(line)

LOGGER = Log('output_of_web_automatization_tool.txt')