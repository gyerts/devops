class Variables:
    def __init__(self, path_to_workspace):
        self.path_to_workspace = path_to_workspace

        self.settings = None
        self.scripts = None
        self.tasks = None

    def set_settings(self, settings):
        self.settings = settings

    def set_processes(self, scripts):
        self.scripts = scripts

    def set_tasks(self, tasks):
        self.tasks = tasks