import os

from Engine.classes.process import Process
from Engine.classes.execute import execute
from Engine.classes.messages import Messages
from Engine.classes.task.CTask import Task
from Engine.classes.filesystem import *
from Engine.classes.configs.CVariables import Variables
from Engine.classes.configs.CJson import Json_file


class Workspaces:
    def __init__(self, path_to_project):
        self.global_settings = Json_file(os.path.join(path_to_project, 'global_settings.json'))
        self.path_to_workspaces = os.path.join(os.getcwd(), "WORKSPACES")
        self.path_to_project = path_to_project
        self.workspaces = dict()

        self.find_variables()

    def find_variables(self):
        workspaces_folders = os.listdir(self.path_to_workspaces)

        for for_delete in ['__pycache__', '.idea']:
            try:
                workspaces_folders.remove(for_delete)
            except:
                pass

        for workspace in workspaces_folders:
            full_path = os.path.join(self.path_to_workspaces, workspace)
            if not os.path.isfile(full_path):
                settings = Variables(full_path)
                settings.set_processes(self.make_cmd_list_to_run_processes(os.path.join(full_path, 'SCRIPTS')))
                settings.set_settings(Json_file(os.path.join(full_path, 'SETTINGS', 'settings')))

                all_tasks_files = get_all_files(os.path.join(full_path, 'SCRIPTS'), 'py')
                tasks = dict()
                for file_name in all_tasks_files:
                    tasks[file_name.split('.')[0]] = Task(file_name, all_tasks_files[file_name])

                settings.set_tasks(tasks)
                self.workspaces[workspace] = settings

    def make_cmd_list_to_run_processes(self, path):
        detected_processes = dict()

        if os.path.exists(path):
            file_elements = os.listdir(path)

            for file in file_elements:
                full_path = os.path.join(path, file)
                if os.path.isfile(full_path):
                    if file[-3:] == '.py' and '__init__' not in file:

                        python_interpreter = 'python3 -u'
                        if 'python2' in open(full_path).readline():
                            python_interpreter = 'python2 -u'

                        name_of_process = file[:-3]
                        if name_of_process not in detected_processes:
                            detected_processes[name_of_process] = Process(execute, [Messages(), python_interpreter + ' ' + full_path], name_of_process)
        return detected_processes
