import json
import os


class Json_file:
    def __init__(self, path_to_settings_file):
        self.path_to_settings_file = path_to_settings_file

        if os.path.exists(self.path_to_settings_file):
            with open(self.path_to_settings_file) as data_file:
                try:
                    self.settings = json.load(data_file)
                except json.decoder.JSONDecodeError as ex:
                    self.settings = dict()
                    print("WARNING: {f} exists but has no readable format:\nstart >>>{content}\n<<< end".format(
                        f=self.path_to_settings_file,
                        content=data_file.read()
                    ))

        else:
            with open(self.path_to_settings_file, 'w') as data_file:
                data_file.write("{}")
                self.settings = dict()

    def set(self, key, value):
        # load json object to file
        self.settings[key] = value
        self.write()

    def get(self, key):
        if key not in self.settings:
            self.settings[key] = 'You should create assign some proper value to key "' + key + '"'
        return self.settings[key]

    def delete(self, key):
        del(self.settings[key])
        self.write()

    def write(self):
        with open(self.path_to_settings_file, 'w') as f:
            json.dump(self.settings, f, indent=4)
