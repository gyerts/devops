class Task:
    def __init__(self, name, path_to_task):
        self.name = name.split('.')[0]
        self.path = path_to_task
        self.status = None
        self.time_pattern = None
        self.description = None

        self.update()

    def update(self):
        f = open(self.path)
        lines = f.readlines()

        detected = False
        for line in lines:
            if '#IWA::' in line:
                line = line.replace('#IWA::', '')
                args = line.split('||')

                self.status = args[0].strip()
                self.time_pattern = args[1].strip()
                self.description = args[2].strip()
                detected = True

        if not detected:
            self.status = 'Disabled'
            self.time_pattern = '* * * * *'
            self.description = 'this task have no tag/comment \'#IWA::\''