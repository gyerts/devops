from Engine.classes.config import variables
import os


path_to_requester = os.path.join(variables.path_to_project, 'Engine', 'classes', 'run_process_via_web_request.py')


def save_tasks_into_crontab(all_tasks):
    f = open('crontab', 'w')

    for ws_name in variables.workspaces:
        all_tasks = variables.workspaces[ws_name].tasks
        for task_name in all_tasks:
            task = all_tasks[task_name]
            if task.status == 'Enabled':
                f.write(
                    task.time_pattern + ' python3 ' +
                    path_to_requester + ' ' +
                    variables.workspaces['DEFAULT'].settings.get('server:port') + ' ' +
                    task_name + ' ' +
                    ws_name + '\n'
                )

    f.close()

    for ws_name in variables.workspaces:
        all_tasks = variables.workspaces[ws_name].tasks
        for task_name in all_tasks:
            task = all_tasks[task_name]
            f = open(task.path)
            lines = f.readlines()
            f.close()

            os.system('crontab ' + os.path.join(variables.path_to_project, 'crontab'))

            output = str()

            updated_line = '#IWA::' + task.status + '||' + task.time_pattern + '||' + task.description + '\n'
            updated = False

            for line in lines:
                if '#IWA::' in line:
                    if line != updated_line:
                        output += updated_line
                        updated = True
                else:
                    output += line

            if updated:
                f = open(task.path, 'w')
                f.write(output)
                f.close()
