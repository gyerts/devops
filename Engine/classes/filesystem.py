import os, time

def get_all_files(path, extention):
    if os.path.exists(path):
        file_elements = os.listdir(path)
        all_files = dict()
        for file in file_elements:
            full_path = os.path.join(path, file)
            if os.path.isfile(full_path):
                if file[-3:] == ('.' + extention) and '__init__' not in file:
                    all_files[file] = full_path
        return all_files
    else: return {}

class Info:
    def __init__(self, path):
        print('making class Info:', path)
        try:
            (mode, ino, dev, nlink, uid, gid, size,
             self.access_time, self.modified_time, self.created_time) = os.stat(path)
        except Exception as ex:
            print('exception', ex)

class File:
    def __init__(self, path):
        index = path.rfind(os.sep)
        self.name = path[index + 1:]
        self.path = path
        self.info = Info(path)

class Folder:
    def __init__(self, path):
        print('making class Folder with path: ', path)
        index = path.rfind(os.sep)
        self.name = path[index + 1:]
        self.path = path
        self.info = Info(path)

        self.files = list()
        self.folders = list()

    def add_file(self, objFile):
        self.files.append(objFile)

    def add_folder(self, objFolder):
        self.folders.append(objFolder)


def parse_all_files_and_folders(path):
    print('parse_all_files_and_folders', path)
    output_folder = Folder(path)
    print('    success: made objFolder')

    if os.path.exists(path):
        file_elements = os.listdir(path)
        for file in file_elements:
            full_path = os.path.join(path, file)
            if os.path.isfile(full_path):
                output_folder.add_file(File(full_path))
            else:
                output_folder.add_folder(parse_all_files_and_folders(full_path))

    return output_folder
