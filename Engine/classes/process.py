from threading import Thread

import os, sys
sys.path.insert(0, os.getcwd())

from Engine.classes.execute import execute
from Engine.classes.messages import Messages
import time

import os, sys
sys.path.insert(0, os.getcwd())

class Process:
    def __init__(self, target, args, name):
        self.messages = args[0]
        self.target = target
        self.args = args
        self.th = None
        self.name = name

    def isAlive(self):
        if self.th != None:
            return self.th.isAlive()
        return False

    def start(self):
        if self.th == None or not self.isAlive():
            self.th = Thread(target=self.target, args=self.args)
            self.messages.clear()

        self.th.start()

    def start_as_simple_process(self):
        self.target(Messages(), self.args[1])

    def get_name(self):
        return self.name
