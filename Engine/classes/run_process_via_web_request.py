import sys
import requests

host_and_port = sys.argv[1]
process = sys.argv[2]
workspace = sys.argv[3]

r = requests.post(
    'http://{}/run_process'.format(host_and_port),
    data={'process': process, 'workspace': workspace}
)
