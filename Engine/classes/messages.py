import re
ansi_escape = re.compile(r'\x1b[^m]*m')

class Messages:
    def __init__(self):
        self.list_of_messages = list()
        self.viewed = 0
        self.status = 'on'

    def add_message(self, message):
        if self.status == 'on':
            try:
                html = message.replace('\033[0m', '</span>')
                html = html.replace('[1;m\'', '</span>')
                html = html.replace(']', ']</span>')

                html = html.replace('\033[0m', '<span style="color: lightgray">')
                html = html.replace('\033[93m', '<span style="color: yellow">')
                html = html.replace('\033[91m', '<span style="color: red">')
                html = html.replace('[1;41m', '<span style="color: red">')
                html = html.replace('\033[95m', '<span style="color: purple">')
                html = html.replace('\033[92m', '<span style="color: green">')

                html = html.replace('\033[92m', '<span style="color: green">')
                html = html.replace('\033[92m', '<span style="color: green">')

                html = html.replace('[', '<span style="color: orange">[')

                html = html.replace('info    |', '<span style="color: orange">info    |</span>')

                if(' error: ' in html or 'error   |' in html):
                    html = '<span style="color: red">' + html + '</span>'
            except:
                html = message

            print(message)

            self.list_of_messages.append(html)

    def get_latest_messages(self, show_from_message, file=False):
        if file:
            self.download_from_file()

        count_of_messages = len(self.list_of_messages)

        new_list_of_messages = list()

        show_from_message = int(show_from_message)

        if (count_of_messages > show_from_message):
            for i in range(show_from_message, count_of_messages):
                new_list_of_messages.append(self.list_of_messages[i])

        return { 'messages': new_list_of_messages, 'position': count_of_messages }

    def pop_all_messages(self):
        lm = self.get_latest_messages(0)
        output = str()

        for message in lm['messages']:
            output += message + '<br>'

        self.clear()
        return output

    def download_from_file(self):
        f = open('/home/ygyerts/p4sm_Toyota_MEU_CY17_All_WEB_AUTOMATIZATION_YGyerts_ygyerts2-lws_share.log')
        self.list_of_messages = f.readlines()

    def on(self):
        self.status = 'on'

    def off(self):
        self.status = 'off'

    def clear(self):
        self.list_of_messages = list()
        self.viewed = 0